package main

import (
	"bytes"
	"encoding/csv"

	"github.com/pkg/errors"
)

var (
	cSVThresholdsTemplateHeader = []string{"column_name", "min_threshold", "max_threshold"}
)

func MakeCSVThresholdsTemplate(columnsNames []string) (*bytes.Buffer, error) {
	bf := bytes.Buffer{}

	csvWrt := csv.NewWriter(&bf)

	if err := csvWrt.Write(cSVThresholdsTemplateHeader); err != nil {

		return nil, errors.Wrap(err,
			"MakeCSVTriggersTemplate: Error writing CSV header")
	}

	for _, col := range columnsNames {
		if err := csvWrt.Write([]string{col, "", ""}); err != nil {
			return nil, errors.Wrap(err,
				"MakeCSVTriggersTemplate: Error writing CSV row")
		}
	}

	csvWrt.Flush()

	return &bf, nil
}
