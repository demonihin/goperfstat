package main

import (
	"io"

	"gitlab.com/demonihin/goperfstat/web_server/types"

	"golang.org/x/text/encoding/charmap"

	"github.com/pkg/errors"
	"golang.org/x/text/encoding"
)

const (
	UTF8CodepageName = "UTF-8"
)

func copyDecode(output io.Writer, input io.Reader,
	codepageName string) (writtenBytes, readedBytes int64, err error) {

	if codepageName == UTF8CodepageName {
		writtenBytes, err = io.Copy(output, input)
		if err != nil {
			return 0, 0, errors.Wrap(err,
				"copyDecode: Error copying UTF-8 file")
		}

		return writtenBytes, writtenBytes, nil
	}

	return copyDecodeNonUTF8(output, input, codepageName)
}

func copyDecodeNonUTF8(output io.Writer, input io.Reader,
	codepageName string) (writtenBytes, readedBytes int64, err error) {
	var (
		buffer        = make([]byte, 4096)
		writtenAtStep int
		readedAtStep  int
	)

	// create decoder
	decoder, err := newDecoder(codepageName)
	if err != nil {
		return 0, 0, errors.WithStack(err)
	}

	// Copy bytes
	for {

		if readedAtStep, err = input.Read(buffer); err != nil {
			if err == io.EOF {
				return writtenBytes, readedBytes, nil
			}

			return writtenBytes, readedBytes, errors.Wrap(err,
				"copyDecodeNonUTF8: Error reading data block")
		}
		readedBytes += int64(readedAtStep)

		// Decode block
		decodedBlock, err := decoder.Bytes(buffer)
		if err != nil {
			return writtenBytes, readedBytes, errors.Wrap(err,
				"copyDecodeNonUTF8: Error decoding block")
		}

		// Write

		if writtenAtStep, err = output.Write(decodedBlock); err != nil {
			return writtenBytes, readedBytes, errors.Wrap(err,
				"copyDecodeNonUTF8: Error writing block")

		}
		writtenBytes += int64(writtenAtStep)
	}
}

func newDecoder(codepage string) (*encoding.Decoder, error) {

	for _, chm := range charmap.All {

		switch c := chm.(type) {
		case *charmap.Charmap:
			if c.String() == codepage {
				return c.NewDecoder(), nil
			}

			break
		case encoding.Encoding:
			break

		default:
			panic("newDecoder: can not typecast encoding to Charmap or Encoding")
		}

	}

	return nil, errors.WithStack(types.ErrIncorrectCodepageName)
}
