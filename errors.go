package main

import (
	"github.com/pkg/errors"
)

var (
	ErrFileHasNotBeenAnalyzed     = errors.New("File has not been analyzed")
	ErrFileAnalyzedWithErrors     = errors.New("File has analyzing errors")
	ErrTaskHasNotBeenProcessed    = errors.New("Task has not yet been processed")
	ErrTaskHasNotProcessingErrors = errors.New("Task has processing errors")
)
