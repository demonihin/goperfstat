module.exports = {
  devServer: {
    proxy: {
      "/api": {
        target: "http://localhost:4000/",
        ws: true,
        changeOrigin: true
      }
    }
  },
  publicPath: process.env.NODE_ENV === "production" ? "/static/" : "/"
};
