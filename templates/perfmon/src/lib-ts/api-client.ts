// eslint-disable-next-line no-unused-vars
import axios, { AxiosResponse, ResponseType } from "axios";
// eslint-disable-next-line no-unused-vars
import { VueConstructor } from "vue";
import moment from "moment";

enum FileStatus {
  // eslint-disable-next-line no-unused-vars
  analyzed = "analyzed",
  // eslint-disable-next-line no-unused-vars
  waiting = "waiting",
  // eslint-disable-next-line no-unused-vars
  with_errors = "errors",
  // eslint-disable-next-line no-unused-vars
  action_required = "action_required"
}

class NewTaskTimeRange {
  range_start: Date;
  range_end: Date;
  columns_to_analyze: NewTaskColumnToAnalyze[];

  constructor(
    range_start: Date,
    range_end: Date,
    column_names: string[],
    column_thresholds: ColumnThreshold[] | null
  ) {
    this.range_start = range_start;
    this.range_end = range_end;

    // Columns to analyze
    let columns_to_analyze: NewTaskColumnToAnalyze[] = [];
    column_names.forEach(function(item) {
      let minTH = null;
      let maxTH = null;

      // Find column's threshold
      if (column_thresholds != null) {
        for (let i = 0; i < column_thresholds.length; i++) {
          if (column_thresholds[i].column_name === item) {
            minTH = column_thresholds[i].min_threshold;
            maxTH = column_thresholds[i].max_threshold;
            break;
          }
        }
      }

      columns_to_analyze.push(new NewTaskColumnToAnalyze(item, minTH, maxTH));
    });

    this.columns_to_analyze = columns_to_analyze;
  }
}

class NewTaskInfo {
  file_upload_name: string;
  time_ranges_to_analyze: NewTaskTimeRange[];

  constructor(file_upload_name: string, new_time_ranges: NewTaskTimeRange[]) {
    this.file_upload_name = file_upload_name;

    this.time_ranges_to_analyze = new_time_ranges;
  }
}

class NewTaskColumnToAnalyze {
  column_to_analyze_name: string;
  min_threshold_value: number | null;
  max_threshold_value: number | null;

  constructor(
    column_to_analyze_name: string,
    min_threshold_value: number | null,
    max_threshold_value: number | null
  ) {
    this.column_to_analyze_name = column_to_analyze_name;
    this.min_threshold_value = min_threshold_value;
    this.max_threshold_value = max_threshold_value;
  }
}

class ColumnThreshold {
  column_name: string;
  min_threshold: number;
  max_threshold: number;

  constructor(
    column_name: string,
    min_threshold: number,
    max_threshold: number
  ) {
    this.column_name = column_name;
    this.max_threshold = max_threshold;
    this.min_threshold = min_threshold;
  }
}

class TaskInfo {
  task_processed: boolean;
  task_unique_id: string;
  task_name: string;
  file_upload_name: string;
  task_create_timestamp: Date;
  task_processing_info: string;
  task_processing_errors: string[];
  task_processing_spent_time: number;
  task_processing_start_timestamp: Date;
  task_processing_end_timestamp: Date;
  time_ranges_to_analyze: TaskTimeRange[];

  constructor(
    task_processed: boolean,
    file_upload_name: string,
    task_create_timestamp: Date,
    task_processing_info: string,
    task_processing_errors: string[],
    task_processing_spent_time: number,
    task_processing_start_timestamp: Date,
    task_processing_end_timestamp: Date,
    time_ranges_to_analyze: TaskTimeRange[],
    task_unique_id: string,
    task_name: string
  ) {
    this.task_processed = task_processed;
    this.file_upload_name = file_upload_name;
    this.task_create_timestamp = task_create_timestamp;
    this.task_processing_info = task_processing_info;
    this.task_processing_errors = task_processing_errors;
    this.task_processing_spent_time = task_processing_spent_time;
    this.task_processing_start_timestamp = task_processing_start_timestamp;
    this.task_processing_end_timestamp = task_processing_end_timestamp;
    this.time_ranges_to_analyze = time_ranges_to_analyze;
    this.task_unique_id = task_unique_id;
    this.task_name = task_name;
  }

  static fromJSON(jsonObject: any): TaskInfo {
    let time_ranges: TaskTimeRange[] = [];

    for (let i = 0; i < jsonObject.time_ranges_to_analyze.length; i++) {
      time_ranges.push(
        TaskTimeRange.fromJSON(jsonObject.time_ranges_to_analyze[i])
      );
    }

    return new TaskInfo(
      jsonObject.task_processed,
      jsonObject.file_upload_name,
      jsonObject.task_create_timestamp,
      jsonObject.task_processing_info,
      jsonObject.task_processing_errors,
      jsonObject.task_processing_spent_time,
      jsonObject.task_processing_start_timestamp,
      jsonObject.task_processing_end_timestamp,
      time_ranges,
      jsonObject.task_unique_id,
      jsonObject.task_name
    );
  }
}

class FileInfo {
  file_size: number;
  file_original_name: string;
  file_upload_name: string;
  file_analyzing_completed: boolean;
  file_analyzing_result: FileAnalyzingResult;
  file_analyzing_errors: string[];
  file_timestamp_column_name: string;
  file_timestamp_column_format: string;
  file_upload_timestamp: Date;
  tasks: TaskInfo[] | null;

  constructor(
    file_size: number,
    file_original_name: string,
    file_upload_name: string,
    file_analyzing_completed: boolean,
    file_analyzing_result: FileAnalyzingResult,
    file_analyzing_errors: string[],
    file_timestamp_column_name: string,
    file_timestamp_column_format: string,
    file_upload_timestamp: Date,
    tasks: TaskInfo[] | null
  ) {
    this.file_size = file_size;
    this.file_original_name = file_original_name;
    this.file_upload_name = file_upload_name;
    this.file_analyzing_completed = file_analyzing_completed;
    this.file_analyzing_result = file_analyzing_result;
    this.file_analyzing_errors = file_analyzing_errors;
    this.file_timestamp_column_name = file_timestamp_column_name;
    this.file_timestamp_column_format = file_timestamp_column_format;
    this.file_upload_timestamp = file_upload_timestamp;
    this.tasks = tasks;
  }

  static fromJSON(jsonObject: any): FileInfo {
    let tasks: TaskInfo[] = [];
    for (let i = 0; i < jsonObject.tasks.length; i++) {
      tasks.push(TaskInfo.fromJSON(jsonObject.tasks[i]));
    }

    let fileAnRes = FileAnalyzingResult.fromJSON(
      jsonObject.file_analyzing_result
    );

    return new FileInfo(
      jsonObject.file_size,
      jsonObject.file_original_name,
      jsonObject.file_upload_name,
      jsonObject.file_analyzing_completed,
      fileAnRes,
      jsonObject.file_analyzing_errors,
      jsonObject.file_timestamp_column_name,
      jsonObject.file_timestamp_column_format,
      jsonObject.file_upload_timestamp,
      tasks
    );
  }

  getStatus(): FileStatus {
    if (!this.file_analyzing_completed) {
      return FileStatus.waiting;
    }

    if (
      this.file_analyzing_errors != null &&
      this.file_analyzing_errors.length > 0
    ) {
      return FileStatus.with_errors;
    }

    if (
      this.file_timestamp_column_format == null ||
      this.file_timestamp_column_name == null
    ) {
      return FileStatus.action_required;
    }

    return FileStatus.analyzed;
  }
}

class FileAnalyzingResult {
  possible_timestamp_column_infos: TimestampColumnInfo[] | null;
  timestamp_column_info: TimestampColumnInfo | null;
  column_names: string[];
  min_file_timestamp: Date;
  max_file_timestamp: Date;

  constructor(
    possible_timestamp_column_infos: TimestampColumnInfo[] | null,
    timestamp_column_info: TimestampColumnInfo | null,
    column_names: string[],
    min_file_timestamp: Date,
    max_file_timestamp: Date
  ) {
    this.possible_timestamp_column_infos = possible_timestamp_column_infos;
    this.timestamp_column_info = timestamp_column_info;
    this.column_names = column_names;
    this.min_file_timestamp = min_file_timestamp;
    this.max_file_timestamp = max_file_timestamp;
  }

  static fromJSON(jsonObject: any): FileAnalyzingResult {
    let timestamp_col_info =
      jsonObject.timestamp_column_info === null
        ? null
        : TimestampColumnInfo.fromJSON(jsonObject.timestamp_column_info);

    let possible_ts_cols: TimestampColumnInfo[] | null =
      jsonObject.possible_timestamp_column_infos === null ? null : [];

    if (jsonObject.possible_timestamp_column_infos !== null) {
      for (
        let i = 0;
        i < jsonObject.possible_timestamp_column_infos.length;
        i++
      ) {
        // @ts-ignore
        possible_ts_cols.push(
          TimestampColumnInfo.fromJSON(
            jsonObject.possible_timestamp_column_infos[i]
          )
        );
      }
    }

    return new FileAnalyzingResult(
      possible_ts_cols,
      timestamp_col_info,
      jsonObject.column_names,
      moment(jsonObject.min_file_timestamp).toDate(),
      moment(jsonObject.max_file_timestamp).toDate()
    );
  }
}

class TimestampColumnInfo {
  timestamp_column_name: string;
  timestamp_column_format: string;

  constructor(timestamp_column_name: string, timestamp_column_format: string) {
    this.timestamp_column_name = timestamp_column_name;
    this.timestamp_column_format = timestamp_column_format;
  }

  static fromJSON(jsonObject: any): TimestampColumnInfo {
    return new TimestampColumnInfo(
      jsonObject.timestamp_column_name,
      jsonObject.timestamp_column_format
    );
  }
}

class TaskTimeRange {
  range_start: Date;
  range_end: Date;
  columns_to_analyze: TaskColumnToAnalyze[];

  constructor(
    range_start: Date,
    range_end: Date,
    columns_to_analyze: TaskColumnToAnalyze[]
  ) {
    this.range_start = range_start;
    this.range_end = range_end;
    this.columns_to_analyze = columns_to_analyze;
  }

  static fromJSON(jsonObject: any): TaskTimeRange {
    let columns: TaskColumnToAnalyze[] = [];

    for (let i = 0; i < jsonObject.columns_to_analyze.length; i++) {
      columns.push(
        TaskColumnToAnalyze.fromJSON(jsonObject.columns_to_analyze[i])
      );
    }

    return new TaskTimeRange(
      jsonObject.range_start,
      jsonObject.range_end,
      columns
    );
  }
}

class TaskColumnToAnalyze {
  processed_values_count: number;
  column_to_analyze_name: string;
  processing_start_timestamp: Date;
  processing_end_timestamp: Date;
  processing_spent_time: number;
  processing_error: string[];
  min_threshold_value: number;
  max_threshold_value: number;
  processing_result: StatsColumnResult;

  constructor(
    processed_values_count: number,
    column_to_analyze_name: string,
    processing_start_timestamp: Date,
    processing_end_timestamp: Date,
    processing_spent_time: number,
    processing_error: string[],
    min_threshold_value: number,
    max_threshold_value: number,
    processing_result: StatsColumnResult
  ) {
    this.processed_values_count = processed_values_count;
    this.column_to_analyze_name = column_to_analyze_name;
    this.processing_start_timestamp = processing_start_timestamp;
    this.processing_end_timestamp = processing_end_timestamp;
    this.processing_spent_time = processing_spent_time;
    this.processing_error = processing_error;
    this.min_threshold_value = min_threshold_value;
    this.max_threshold_value = max_threshold_value;
    this.processing_result = processing_result;
  }

  static fromJSON(jsonObject: any): TaskColumnToAnalyze {
    let processing_res = StatsColumnResult.fromJSON(
      jsonObject.processing_result
    );

    return new TaskColumnToAnalyze(
      jsonObject.processed_values_count,
      jsonObject.column_to_analyze_name,
      jsonObject.processing_start_timestamp,
      jsonObject.processing_end_timestamp,
      jsonObject.processing_spent_time,
      jsonObject.processing_error,
      jsonObject.min_threshold_value,
      jsonObject.max_threshold_value,
      processing_res
    );
  }
}

class StatsColumnResult {
  mean: number;
  variance: number;
  min: number;
  max: number;
  more_than_max_threshold_fraction: number;
  less_than_min_threshold_fraction: number;
  percentile95: number;
  percentile90: number;
  percentile80: number;

  constructor(
    mean: number,
    variance: number,
    min: number,
    max: number,
    more_than_max_threshold_fraction: number,
    less_than_min_threshold_fraction: number,
    percentile95: number,
    percentile90: number,
    percentile80: number
  ) {
    this.mean = mean;
    this.variance = variance;
    this.min = min;
    this.max = max;
    this.more_than_max_threshold_fraction = more_than_max_threshold_fraction;
    this.less_than_min_threshold_fraction = less_than_min_threshold_fraction;
    this.percentile95 = percentile95;
    this.percentile90 = percentile90;
    this.percentile80 = percentile80;
  }

  static fromJSON(jsonObject: any): StatsColumnResult {
    return new StatsColumnResult(
      jsonObject.mean,
      jsonObject.variance,
      jsonObject.min,
      jsonObject.max,
      jsonObject.more_than_max_threshold_fraction,
      jsonObject.less_than_min_threshold_fraction,
      jsonObject.percentile95,
      jsonObject.percentile90,
      jsonObject.percentile80
    );
  }
}

class ApiClient {
  apiPrefix: string;

  constructor(apiPrefix: string) {
    this.apiPrefix = apiPrefix;
  }

  async login(): Promise<number> {
    let resp = await axios.post(this.apiPrefix + "/auth/login");

    return resp.status;
  }

  async getStatus(): Promise<FileInfo[]> {
    let respData: AxiosResponse | null = null;
    try {
      respData = await axios.get(this.apiPrefix + "/status");
    } catch (e) {
      throw e;
    }

    if (respData && respData.status !== 200) {
      throw `Incorrect server response code ${respData.status}. Status: ${
        respData.statusText
      }`;
    }

    if (!respData) {
      throw "Empty server response";
    }

    let res: FileInfo[] = [];
    for (let i = 0; i < respData.data.length; i++) {
      res.push(FileInfo.fromJSON(respData.data[i]));
    }

    return res;
  }

  async sendTimestampColumnUpdate(
    newTimestampColumnInfo: TimestampColumnInfo,
    fileUploadID: string
  ): Promise<AxiosResponse> {
    return axios.post(
      this.apiPrefix + "/files/" + fileUploadID,
      newTimestampColumnInfo
    );
  }

  async createNewTask(newTask: NewTaskInfo): Promise<number> {
    let response = await axios.post(this.apiPrefix + "/tasks", newTask);

    return response.status;
  }

  async sendFileRename(
    newFileName: string,
    fileUploadID: string
  ): Promise<AxiosResponse> {
    return axios.patch(this.apiPrefix + "/files/" + fileUploadID, [
      {
        key: "file_original_name",
        value: newFileName
      }
    ]);
  }

  async sendFileDelete(fileUploadID: string): Promise<AxiosResponse> {
    return axios.delete(this.apiPrefix + "/files/" + fileUploadID);
  }

  async sendTaskRename(
    newTaskName: string,
    taskUniqueId: string
  ): Promise<AxiosResponse> {
    return axios.patch(this.apiPrefix + "/tasks/" + taskUniqueId, [
      {
        key: "task_name",
        value: newTaskName
      }
    ]);
  }

  async uploadFile(file: File, fileCodepage: string): Promise<AxiosResponse> {
    let formData = new FormData();
    formData.append("files", file);
    formData.append("codepage", fileCodepage);

    return axios.post(this.apiPrefix + "/files", formData);
  }

  async getSupportedCodepages(): Promise<Array<string>> {
    let response = await axios.get(this.apiPrefix + "/info/codepages");

    if (response.status === 200) {
      return response.data;
    }

    throw `Error getting codepages: ${response.status}, ${response.statusText}`;
  }

  getTriggersTemplateLink(fileName: string): string {
    return this.apiPrefix + "/files/" + fileName + "/triggers/template";
  }

  getCSVReportLink(task_unique_id: string): string {
    return this.apiPrefix + "/tasks/" + task_unique_id + "/reports/csv";
  }
}

const ApiPlugin = {
  install: function(VueInst: VueConstructor, options: any) {
    VueInst.prototype.$ApiClient = new ApiClient(options.apiPathPrefix);
  }
};

export {
  FileInfo,
  TaskInfo,
  FileAnalyzingResult,
  StatsColumnResult,
  TaskTimeRange,
  TaskColumnToAnalyze,
  TimestampColumnInfo,
  ApiClient,
  NewTaskTimeRange,
  NewTaskInfo,
  FileStatus,
  ColumnThreshold,
  NewTaskColumnToAnalyze
};

export default ApiPlugin;
