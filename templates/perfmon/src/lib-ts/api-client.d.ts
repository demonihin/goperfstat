import Vue from "vue";
import { ApiClient } from "@/lib-ts/api-client";

declare module "vue/types/vue" {
  interface Vue {
    $ApiClient: ApiClient;
  }
}
