import Vue from "vue";
import ApiPlugin from "../lib-ts/api-client";

Vue.use(ApiPlugin, {
  apiPathPrefix: "/api"
});
