import "material-design-icons-iconfont/dist/material-design-icons.css";
import Vue from "vue";
import Vuetify from "vuetify/lib";
import "vuetify/src/stylus/app.styl";
// @ts-ignore
import { Ripple } from "vuetify/lib/directives";

Vue.use(Vuetify, {
  iconfont: "mdi",
  directives: {
    Ripple
  }
});
