import Vue from "vue";
//@ts-ignore
import DatetimePicker from "vuetify-datetime-picker";
import "vuetify-datetime-picker/src/stylus/main.styl";

Vue.use(DatetimePicker);
