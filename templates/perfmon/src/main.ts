import Vue from "vue";
import "./plugins/vuetify";
import "./plugins/api";
import App from "./App.vue";
import "roboto-fontface/css/roboto/roboto-fontface.css";
import "@mdi/font/css/materialdesignicons.css";

import "./plugins/upload-button";

Vue.config.productionTip = false;

new Vue({
  render: h => h(App)
}).$mount("#app");
