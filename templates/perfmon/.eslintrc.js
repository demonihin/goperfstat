module.exports = {
  root: true,

  env: {
    node: true
  },

  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",

    // enable additional rules
    "indent": ["error", 2],
    "linebreak-style": ["error", "unix"],
    "quotes": ["error", "double"],
    "semi": ["error", "always"],

    // override default options for rules from base configurations
    "no-cond-assign": ["error", "always"],

  },

  parserOptions: {
    parser: '@typescript-eslint/parser'
  },


  'extends': [
    'plugin:vue/essential',
    'plugin:vue/recommended',
    '@vue/prettier',
    '@vue/typescript',
    'eslint:recommended'
  ]
};

