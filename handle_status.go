package main

import (
	"github.com/gin-gonic/gin"
	"github.com/helloeave/json"
	"gitlab.com/demonihin/goperfstat/web_server/db_saver"
)

func createStatusHandler(svr *db_saver.DBSaver) gin.HandlerFunc {
	return func(c *gin.Context) {
		// Get session
		cook, err := c.Cookie(AuthCookiesName)
		if err != nil {
			AbortNoAuthCookies500(c, err)

			return
		}

		// Get info
		files, err := svr.GetFilesListForSessionContext(c, cook)
		if err != nil {
			Abort500Error(c, err, "Error getting files information")

			return
		}

		// c.JSON(200, files)
		js, err := json.MarshalSafeCollections(files)
		if err != nil {
			Abort500Error(c, err, "Error marhaling response")

			return
		}

		if _, err := c.Writer.Write(js); err != nil {
			Abort500Error(c, err, "Error sending response")

			return
		}
	}
}
