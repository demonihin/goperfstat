package main

import (
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/demonihin/goperfstat/web_server/db_saver"
)

func createAuthMiddleware(svr *db_saver.DBSaver, noAuthPaths []string) gin.HandlerFunc {

	return func(c *gin.Context) {

		// Skip authentication for noAuthPaths
		if isInSlice(c.Request.RequestURI, noAuthPaths) {
			return
		}

		// Check Auth
		// Get session
		cook, err := c.Cookie(AuthCookiesName)
		if err != nil {
			Abort401Error(c, err, "Error getting authentication cookies")

			return
		}

		// Check login
		if err := checkLogin(c, svr, cook); err != nil {
			Abort401Error(c, err, "Error getting user session")

			return
		}
	}
}

func isInSlice(path string, lookInPaths []string) bool {

	for i := range lookInPaths {
		if strings.HasPrefix(path, lookInPaths[i]) {
			return true
		}
	}

	return false
}
