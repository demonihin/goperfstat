package main

import (
	"os"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/demonihin/goperfstat/web_server/db_saver"
	"gitlab.com/demonihin/goperfstat/web_server/types"
)

const (
	FileUploadFormFieldName                = "files"
	FileUploadFormFieldCodepageName        = "codepage"
	FileUploadFormFieldDefaultCodepageName = "UTF-8"
)

func createFileUploadHandler(svr *db_saver.DBSaver) gin.HandlerFunc {
	return func(c *gin.Context) {

		// Get session
		cook, err := c.Cookie(AuthCookiesName)
		if err != nil {
			AbortNoAuthCookies500(c, err)

			return
		}

		// Get form
		form, err := c.MultipartForm()
		if err != nil {
			Abort400Error(c, err, "Error getting multipart/form")
			return
		}

		// Get files codepage
		codepage, ok := c.GetPostForm(FileUploadFormFieldCodepageName)
		if !ok {
			codepage = FileUploadFormFieldDefaultCodepageName
		}

		// Process every file
		for _, file := range form.File[FileUploadFormFieldName] {

			// Open file from form
			openedFile, err := file.Open()
			if err != nil {
				Abort500Error(c, err, "Error opening file for writing to server")
				return
			}
			defer func() {
				if ierr := openedFile.Close(); ierr != nil {
					_ = c.Error(ierr)
				}
			}()

			// Open file on server to write to
			uploadFilename := uuid.NewV4().String()
			writerTo, err := os.Create(types.FileUploadDirectory + uploadFilename)
			if err != nil {
				Abort500Error(c, err, "Error generating filename_upload_name")

				return
			}
			defer func() {
				if ierr := writerTo.Close(); ierr != nil {
					_ = c.Error(ierr)
				}
			}()

			// Save the uploaded file
			_, readed, err := copyDecode(writerTo, openedFile, codepage)
			if err != nil {
				if errors.Cause(err) == types.ErrIncorrectCodepageName {
					Abort400Error(c, err, "Incorrect codepage name")

					return
				}

				Abort500Error(c, err, "Error recoding")

				return
			}
			if readed != file.Size {
				Abort500Error(c, types.ErrSavingFile, "")

				return
			}

			// Save info in DB
			if err := svr.SaveUploadedFileInfoContext(c, cook, file.Filename,
				uploadFilename, file.Size); err != nil {

				Abort500Error(c, err, "Error saving file info")

				return
			}
		}

		// Empty response, no error
		Send204JSONResponse(c)
	}
}
