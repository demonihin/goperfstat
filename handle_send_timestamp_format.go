package main

import (
	"context"

	"gitlab.com/demonihin/goperfstat/web_server/db_saver"
	"gitlab.com/demonihin/goperfstat/web_server/types"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
)

func createSendTimestampFormatHandler(svr *db_saver.DBSaver) gin.HandlerFunc {

	return func(c *gin.Context) {

		// Get session
		cook, err := c.Cookie(AuthCookiesName)
		if err != nil {
			AbortNoAuthCookies500(c, err)

			return
		}

		// Get requested filename
		filename := c.Param(FilenameDownloadPathParamName)
		if filename == "" {
			AbortNoFileGiven500(c)

			return
		}

		// Check if the file was uploaded by current user
		fi, err := svr.GetFileInfoForFileContext(c, filename, cook)
		if err != nil {
			Abort404Error(c, err, "File not found")

			return
		}

		// Load TimestampColumnInfo from User's input
		var inputTSColInfo types.TimestampColumnInfo
		if err := c.ShouldBindJSON(&inputTSColInfo); err != nil {
			Abort500Error(c, err, "Error parsing request payload")

			return
		}

		// Set Timestamp column Data and
		// Force file reanalyzing using given Timestamp Column Info
		if err := setTimestampColumnContext(c, svr, fi, &inputTSColInfo); err != nil {
			Abort500Error(c, err, "Error setting file timestamp  column")

			return
		}

		// Response status
		Send200JSONResponse(c, gin.H{"status": "ok"})
	}
}

func setTimestampColumnContext(ctx context.Context, svr *db_saver.DBSaver,
	fi *types.FileInfo, reqTimestampColumnInfo *types.TimestampColumnInfo) error {

	var formatFoundInPossibleFormats bool

	for pCI := range fi.FileAnalyzingResult.PossibleTimestampColumnInfos {

		pTCIP := &fi.FileAnalyzingResult.PossibleTimestampColumnInfos[pCI]

		if reqTimestampColumnInfo.TimestampColumnName == pTCIP.TimestampColumnName &&
			reqTimestampColumnInfo.TimestampColumnFormat == pTCIP.TimestampColumnFormat {

			formatFoundInPossibleFormats = true
			break
		}
	}

	if !formatFoundInPossibleFormats {
		return errors.Wrap(types.ErrCanNotAutoFindTimestapFormat,
			"setTimestampColumnContext: Error: User input format is not in supported formats list")
	}

	// Update DB
	if err := svr.SetFileTimestampColumnInfoContext(ctx,
		fi.FilePKID, reqTimestampColumnInfo, true); err != nil {

		return errors.WithStack(err)
	}

	return nil
}
