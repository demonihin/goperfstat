package db_saver

import (
	"context"
	"database/sql"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
)

func testCreateMockDBTxHelper(t *testing.T, ctx context.Context) (
	*sql.Tx, sqlmock.Sqlmock) {
	t.Helper()

	db, mock := testCreateMockDBHelper(t)
	mock.ExpectBegin()

	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		t.Fatalf("Unexpected fatal error %v", err)
	}
	return tx, mock
}

func testCreateMockDBHelper(t *testing.T) (*sql.DB, sqlmock.Sqlmock) {
	t.Helper()

	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Unexpected error creating SQL mock DB actual = %v, expected %v",
			err, nil)
	}

	return db, mock
}

func testCreateMockXDBHelper(t *testing.T) (*sqlx.DB, sqlmock.Sqlmock) {
	t.Helper()

	db, mock := testCreateMockDBHelper(t)

	dbx := sqlx.NewDb(db, "sqlmock")
	return dbx, mock
}

func testCreateMockDBTxxHelper(t *testing.T, ctx context.Context) (
	*sqlx.Tx, sqlmock.Sqlmock) {
	dbx, mock := testCreateMockXDBHelper(t)
	mock.ExpectBegin()

	txx, err := dbx.BeginTxx(ctx, nil)
	if err != nil {
		t.Fatalf("Unexpected fatal error %v", err)
	}
	return txx, mock
}
