package db_saver

import (
	"testing"
	"time"

	"context"

	"strings"

	"reflect"

	"strconv"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/pkg/errors"
	"gitlab.com/demonihin/goperfstat/web_server/types"
)

type sqlQueryColumnsTestCaseTaskColumnToAnalyze struct {
	args []interface{}
	rows *sqlmock.Rows

	result map[uint64][]types.TaskColumnToAnalyze
}

var (
	errTestSelectFromColumnsToAnalyze = errors.New("SELECT FROM columns_to_analyze failed")
)

func TestGetColumnsForTaskTimeRangesTransactContext(t *testing.T) {

	emptyInputTest := "test empty task IDs input slice"
	if success := t.Run(emptyInputTest, testEmptyInputtaskRangesPKIDs); !success {
		t.Errorf("Failed subtest %s", emptyInputTest)
	}

	nilInputTest := "test nil task IDs input slice"
	if success := t.Run(emptyInputTest, testEmptyInputtaskRangesPKIDs); !success {
		t.Errorf("Failed subtest %s", nilInputTest)
	}

	testSelectError := "test select columns query error"
	if success := t.Run(emptyInputTest, testSelectColumnsQueryError); !success {
		t.Errorf("Failed subtest %s", testSelectError)
	}

	testSelectColumnsOk := "test select columns query ok"
	if success := t.Run(testSelectColumnsOk, testSelectColumnsQueryOk); !success {
		t.Errorf("Failed subtest %s", testSelectColumnsOk)
	}

	testIncorrectColumnSQLDriverValuesScan := "test incorrect driver values returned on Scan"
	if success := t.Run(testIncorrectColumnSQLDriverValuesScan,
		testSelectColumnQueryIncorrectReturnedValuesScan); !success {

		t.Errorf("Failed subtest %s", testIncorrectColumnSQLDriverValuesScan)
	}

	testIncorrectColumnSQLDriverValuesJSONUnmarshall := "test incorrect driver values returned on JSON unmarshall"
	if success := t.Run(testIncorrectColumnSQLDriverValuesJSONUnmarshall,
		testSelectColumnQueryIncorrectReturnedValuesJSONUnmarshall); !success {

		t.Errorf("Failed subtest %s", testIncorrectColumnSQLDriverValuesJSONUnmarshall)
	}

	testIncorrectColumnSQLDriverRowsCloseError := "test driver rows close error"
	if success := t.Run(testIncorrectColumnSQLDriverRowsCloseError,
		testSelectColumnQueryRowsCursorCloseError); !success {

		t.Errorf("Failed subtest %s", testIncorrectColumnSQLDriverRowsCloseError)
	}
}

func testEmptyInputtaskRangesPKIDs(t *testing.T) {
	ctx := context.Background()

	tx, _ := testCreateMockDBTxxHelper(t, ctx)

	_, err := getColumnsForTaskTimeRangesTransactContext(ctx, tx, []interface{}{})
	if cs := errors.Cause(err); cs != types.ErrZeroLengthInputSlice {
		t.Errorf("Unexpected error cause actual = %v, expected = %v",
			cs, types.ErrZeroLengthInputSlice)
	}
}

func testSelectColumnsQueryError(t *testing.T) {

	ctx := context.Background()

	tx, mock := testCreateMockDBTxxHelper(t, ctx)

	mock.ExpectQuery(
		`SELECT\s+.+\s+FROM\s+columns_to_analyze\s+WHERE\s+task_time_range_pk_id\s+`+
			`IN\s+\(.+\);`).WillReturnError(
		errTestSelectFromColumnsToAnalyze).WithArgs(1, 2, 3)

	_, err := getColumnsForTaskTimeRangesTransactContext(ctx, tx,
		[]interface{}{1, 2, 3}) // Non empty []interface{} is important here
	if cs := errors.Cause(err); cs != errTestSelectFromColumnsToAnalyze {
		t.Errorf("Unexpected error cause actual = %v, expected = %v",
			cs, errTestSelectFromColumnsToAnalyze)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("SQL mock expectations were not met actual = %v, expected = %v",
			err, nil)
	}

}

func testSelectColumnsQueryOk(t *testing.T) {

	// Test data
	var (
		processingStartTime = time.Date(2015, time.March, 10, 12, 13, 40, 0, time.UTC)
		processingEndTime   = time.Date(2015, time.March, 10, 12, 13, 43, 0, time.UTC)
		processingDuration  = 3 * time.Second

		minThFraction = 0.3
		maxThFraction = 0.5
		minThVal      = 1.1
		maxThVal      = 3.1

		getColumnsTestCasesOk = []sqlQueryColumnsTestCaseTaskColumnToAnalyze{
			sqlQueryColumnsTestCaseTaskColumnToAnalyze{
				args: []interface{}{1, 2, 3},
				rows: sqlmock.NewRows([]string{
					"column_pk_id",
					"task_time_range_pk_id",
					"processed_values_count",
					"file_column_name",
					"processing_start_timestamp",
					"processing_end_timestamp",
					"processing_spent_time",
					"processing_error",
					"processing_result",
					"min_threshold_value",
					"max_threshold_value",
				}).
					// Minimum
					AddRow(100, 10, nil, "minimum data", nil, nil, nil, nil, nil,
						nil, nil).

					// Processing started
					AddRow(102, 10, nil, "with processing start time",
						processingStartTime,
						nil, nil, nil, nil, nil, nil).

					// Processing finished - get end time and guration
					AddRow(
						103, 10, 1003, "with processing end time and duration",
						processingStartTime,
						processingEndTime,
						processingDuration, nil, nil, nil, nil).

					// Processing ended with error
					AddRow(104, 10, 1013, "with processing times and error",
						processingStartTime, processingEndTime,
						processingDuration, `["error1", "error2", "error3"]`,
						nil, nil, nil).

					// Processing ended without error, contains result
					AddRow(105, 10, 1014, "with processing times and result",
						processingStartTime, processingEndTime,
						processingDuration, nil,
						`{"mean": 1, "variance":10, "min": -1, "max":20, 
					"percentile80": 10.2, "percentile90": 14.3,
					"percentile95":15.1}`, nil, nil).

					// With min and max thresholds
					AddRow(106, 10, 1015, "with processing times, result and thresholds",
						processingStartTime, processingEndTime,
						processingDuration, nil,
						`{"mean": 1, "variance":10, "min": -1, "max":20, 
					"percentile80": 10.2, "percentile90": 14.3, 
					"percentile95":15.1, "more_than_max_threshold_fraction": `+
							strconv.FormatFloat(maxThFraction, 'G', -1, 64)+
							`, "less_than_min_threshold_fraction": `+
							strconv.FormatFloat(minThFraction, 'G', -1, 64)+`}`,
						minThVal, maxThVal).

					// Just some other time ranges' columns to test separating by Time range PKID
					AddRow(200, 20, nil, "f2", nil, nil, nil, nil, nil, nil, nil).
					AddRow(210, 20, nil, "f2", nil, nil, nil, nil, nil, nil, nil).
					AddRow(300, 30, nil, "f3", nil, nil, nil, nil, nil, nil, nil),
				result: map[uint64][]types.TaskColumnToAnalyze{
					10: []types.TaskColumnToAnalyze{
						types.TaskColumnToAnalyze{
							ColumnPKID:        100,
							TaskTimeRangePKID: 10,
							FileColumnName:    "minimum data",
							Result:            &types.StatsColumnResult{},
						},
						types.TaskColumnToAnalyze{
							ColumnPKID:           102,
							TaskTimeRangePKID:    10,
							FileColumnName:       "with processing start time",
							ComputationStartTime: &processingStartTime,
							Result:               &types.StatsColumnResult{},
						},
						types.TaskColumnToAnalyze{
							ColumnPKID:            103,
							TaskTimeRangePKID:     10,
							ProcessedValuesCount:  1003,
							FileColumnName:        "with processing end time and duration",
							ComputationStartTime:  &processingStartTime,
							ComputationFinishTime: &processingEndTime,
							ComputationDuration:   &processingDuration,
							Result:                &types.StatsColumnResult{},
						},
						types.TaskColumnToAnalyze{
							ColumnPKID:            104,
							TaskTimeRangePKID:     10,
							ProcessedValuesCount:  1013,
							FileColumnName:        "with processing times and error",
							ComputationStartTime:  &processingStartTime,
							ComputationFinishTime: &processingEndTime,
							ComputationDuration:   &processingDuration,
							ComputationError:      []string{"error1", "error2", "error3"},
							Result:                &types.StatsColumnResult{},
						},
						types.TaskColumnToAnalyze{
							ColumnPKID:            105,
							TaskTimeRangePKID:     10,
							ProcessedValuesCount:  1014,
							FileColumnName:        "with processing times and result",
							ComputationStartTime:  &processingStartTime,
							ComputationFinishTime: &processingEndTime,
							ComputationDuration:   &processingDuration,
							Result: &types.StatsColumnResult{
								Mean: 1, Variance: 10, Min: -1, Max: 20,
								Percentiles: types.Percentiles{
									Percentile80: 10.2, Percentile90: 14.3,
									Percentile95: 15.1,
								},
							},
						},
						types.TaskColumnToAnalyze{
							ColumnPKID:            106,
							TaskTimeRangePKID:     10,
							ProcessedValuesCount:  1015,
							FileColumnName:        "with processing times, result and thresholds",
							ComputationStartTime:  &processingStartTime,
							ComputationFinishTime: &processingEndTime,
							ComputationDuration:   &processingDuration,
							MinValueThreshold:     &minThVal,
							MaxValueThreshold:     &maxThVal,
							Result: &types.StatsColumnResult{
								Mean: 1, Variance: 10, Min: -1, Max: 20,
								Percentiles: types.Percentiles{
									Percentile80: 10.2, Percentile90: 14.3,
									Percentile95: 15.1,
								},
								LessThanMinThresholdFraction: &minThFraction,
								MoreThanMaxThresholdFraction: &maxThFraction,
							},
						},
					},
					20: []types.TaskColumnToAnalyze{
						types.TaskColumnToAnalyze{
							ColumnPKID:        200,
							TaskTimeRangePKID: 20,
							FileColumnName:    "f2",
							Result:            &types.StatsColumnResult{},
						},
						types.TaskColumnToAnalyze{
							ColumnPKID:        210,
							TaskTimeRangePKID: 20,
							FileColumnName:    "f2",
							Result:            &types.StatsColumnResult{},
						},
					},
					30: []types.TaskColumnToAnalyze{
						types.TaskColumnToAnalyze{
							ColumnPKID:        300,
							TaskTimeRangePKID: 30,
							FileColumnName:    "f3",
							Result:            &types.StatsColumnResult{},
						},
					},
				},
			},
		}
	)

	ctx := context.Background()

	for _, tCase := range getColumnsTestCasesOk {
		tx, mock := testCreateMockDBTxxHelper(t, ctx)

		query := `SELECT\s+column_pk_id,\s*task_time_range_pk_id,\s*processed_values_count,\s*file_column_name,\s*processing_start_timestamp,\s*processing_end_timestamp,\s*processing_spent_time,\s*processing_error,\s*processing_result,\s*min_threshold_value,\s*max_threshold_value\s+FROM\s+columns_to_analyze\s+WHERE\s+task_time_range_pk_id\s+IN\s+\(\s*\?\s*` +
			strings.Repeat(`\s*,\s*\?\s*`, len(tCase.args)-1) + `\);`

		mock.ExpectQuery(query).WillReturnRows(tCase.rows)

		res, err := getColumnsForTaskTimeRangesTransactContext(ctx, tx, tCase.args)
		if err != nil {
			t.Errorf("Unexpected error actual = %v, expected = %v", err, nil)
		}
		if !reflect.DeepEqual(res, tCase.result) {
			t.Errorf("Unexpected not equal result actual = %v, expected = %v",
				res, tCase.result)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("SQL mock expectations were not met actual = %v, expected = %v",
				err, nil)
		}
	}
}

func testSelectColumnQueryIncorrectReturnedValuesScan(t *testing.T) {

	var (
		ctx = context.Background()

		// processingEndTime  = time.Date(2015, time.March, 10, 12, 13, 43, 0, time.UTC)
		// processingDuration = 3 * time.Second

		getColumnsTestCasesOk = []sqlQueryColumnsTestCaseTaskColumnToAnalyze{

			// Incorrect column pk id - string instead of number
			sqlQueryColumnsTestCaseTaskColumnToAnalyze{
				args: []interface{}{10, 2, 3},
				rows: sqlmock.NewRows([]string{
					"column_pk_id",
					"task_time_range_pk_id",
					"processed_values_count",
					"file_column_name",
					"processing_start_timestamp",
					"processing_end_timestamp",
					"processing_spent_time",
					"processing_error",
					"processing_result",
					"min_threshold_value",
					"max_threshold_value",
				}).AddRow("100a", 10, nil, "minimum data", nil, nil, nil, nil, nil,
					nil, nil),
			},
			// // Processing started - incorrect timestamp
			// // ToDo: Fix. Does not work
			// sqlQueryColumnsTestCase{
			// 	args: []interface{}{10, 2, 3},
			// 	rows: sqlmock.NewRows([]string{
			// 		"column_pk_id",
			// 		"task_time_range_pk_id",
			// 		"processed_values_count",
			// 		"file_column_name",
			// 		"processing_start_timestamp",
			// 		"processing_end_timestamp",
			// 		"processing_spent_time",
			// 		"processing_error",
			// 		"processing_result",
			// 		"min_threshold_value",
			// 		"max_threshold_value",
			// 	}).AddRow(
			// 		103, 10, 1003, "with processing end time and duration",
			// 		"2010 12 12 fdsgs dsf",
			// 		processingEndTime, processingDuration, nil, nil, nil, nil),
			// },
		}
	)

	tx, mock := testCreateMockDBTxxHelper(t, ctx)

	for _, tCase := range getColumnsTestCasesOk {
		query := `SELECT\s+column_pk_id,\s*task_time_range_pk_id,\s*processed_values_count,\s*file_column_name,\s*processing_start_timestamp,\s*processing_end_timestamp,\s*processing_spent_time,\s*processing_error,\s*processing_result,\s*min_threshold_value,\s*max_threshold_value\s+FROM\s+columns_to_analyze\s+WHERE\s+task_time_range_pk_id\s+IN\s+\(\s*\?\s*` +
			strings.Repeat(`\s*,\s*\?\s*`, len(tCase.args)-1) + `\);`

		mock.ExpectQuery(query).WillReturnRows(tCase.rows)

		_, err := getColumnsForTaskTimeRangesTransactContext(ctx, tx, tCase.args)

		// Test for SQL scan error
		if cs := errors.Cause(err); cs == nil || !strings.Contains(
			cs.Error(), `sql: Scan error on`) {

			t.Errorf(
				"Unexpected error actual = %v, expected = %v",
				cs, "sql: Scan error on...")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("SQL mock expectations were not met actual = %v, expected = %v",
				err, nil)
		}
	}
}

func testSelectColumnQueryIncorrectReturnedValuesJSONUnmarshall(t *testing.T) {

	var (
		ctx = context.Background()

		processingStartTime = time.Date(2015, time.March, 10, 12, 13, 40, 0, time.UTC)
		processingEndTime   = time.Date(2015, time.March, 10, 12, 13, 43, 0, time.UTC)
		processingDuration  = 3 * time.Second

		minThVal      = 1.0
		maxThVal      = 3.1
		minThFraction = 0.31

		getColumnsTestCasesOk = []sqlQueryColumnsTestCaseTaskColumnToAnalyze{

			// Processing ended with error - wrong error serialization
			sqlQueryColumnsTestCaseTaskColumnToAnalyze{
				args: []interface{}{10, 2, 3},
				rows: sqlmock.NewRows([]string{
					"column_pk_id",
					"task_time_range_pk_id",
					"processed_values_count",
					"file_column_name",
					"processing_start_timestamp",
					"processing_end_timestamp",
					"processing_spent_time",
					"processing_error",
					"processing_result",
					"min_threshold_value",
					"max_threshold_value",
				}).AddRow(104, 10, 1013, "with processing times and error",
					processingStartTime, processingEndTime, processingDuration,
					`["error1", "error2", "error3"`,
					nil, nil, nil),
			},
			// Processing ended without error, contains result which is wrongly
			// serialized
			sqlQueryColumnsTestCaseTaskColumnToAnalyze{
				args: []interface{}{10, 2, 3},
				rows: sqlmock.NewRows([]string{
					"column_pk_id",
					"task_time_range_pk_id",
					"processed_values_count",
					"file_column_name",
					"processing_start_timestamp",
					"processing_end_timestamp",
					"processing_spent_time",
					"processing_error",
					"processing_result",
					"min_threshold_value",
					"max_threshold_value",
				}).
					AddRow(105, 10, 1014, "with processing times and result",
						processingStartTime, processingEndTime,
						processingDuration, nil,
						`{"mean": 1, "variance":10, "min": -1, "max":20,
					"percentile80": 1.0.2, "percentile90": 14.3,
					"percentile95":15.1}`, nil, nil),
			},
			// With min and max thresholds - wrong threshold fraction value
			sqlQueryColumnsTestCaseTaskColumnToAnalyze{
				args: []interface{}{10, 2, 3},
				rows: sqlmock.NewRows([]string{
					"column_pk_id",
					"task_time_range_pk_id",
					"processed_values_count",
					"file_column_name",
					"processing_start_timestamp",
					"processing_end_timestamp",
					"processing_spent_time",
					"processing_error",
					"processing_result",
					"min_threshold_value",
					"max_threshold_value",
				}).AddRow(106, 10, 1015, "with processing times, result and thresholds",
					processingStartTime, processingEndTime,
					processingDuration, nil,
					`{"mean": 1, "variance":10, "min": -1, "max":20,
					"percentile80": 10.2, "percentile90": 14.3,
					"percentile95":15.1, "more_than_max_threshold_fraction": 1.2.2,
					"less_than_min_threshold_fraction": `+
						strconv.FormatFloat(minThFraction, 'G', -1, 64)+`}`,
					minThVal, maxThVal),
			},
		}
	)

	tx, mock := testCreateMockDBTxxHelper(t, ctx)

	for _, tCase := range getColumnsTestCasesOk {
		query := `SELECT\s+column_pk_id,\s*task_time_range_pk_id,\s*processed_values_count,\s*file_column_name,\s*processing_start_timestamp,\s*processing_end_timestamp,\s*processing_spent_time,\s*processing_error,\s*processing_result,\s*min_threshold_value,\s*max_threshold_value\s+FROM\s+columns_to_analyze\s+WHERE\s+task_time_range_pk_id\s+IN\s+\(\s*\?\s*` +
			strings.Repeat(`\s*,\s*\?\s*`, len(tCase.args)-1) + `\);`

		mock.ExpectQuery(query).WillReturnRows(tCase.rows)

		_, err := getColumnsForTaskTimeRangesTransactContext(ctx, tx, tCase.args)

		// Test for SQL scan error
		if cs := errors.Cause(err); cs == nil || !(strings.Contains(
			cs.Error(), "unexpected end of JSON input") || strings.Contains(cs.Error(),
			`invalid character '.' after object key:value pair`)) {

			t.Errorf(
				"Unexpected error actual = %v, expected = %v",
				cs,
				"unexpected end of JSON input... OR invalid character '.' after object key:value pair...")
		}

		t.Log(errors.Cause(err))

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("SQL mock expectations were not met actual = %v, expected = %v",
				err, nil)
		}
	}
}

func testSelectColumnQueryRowsCursorCloseError(t *testing.T) {
	// Test data
	var (
		errRowsCloseError = errors.New("Test Rows close error")

		getColumnsTestCasesOk = []sqlQueryColumnsTestCaseTaskColumnToAnalyze{
			sqlQueryColumnsTestCaseTaskColumnToAnalyze{
				args: []interface{}{1, 2, 3},
				rows: sqlmock.NewRows([]string{
					"column_pk_id",
					"task_time_range_pk_id",
					"processed_values_count",
					"file_column_name",
					"processing_start_timestamp",
					"processing_end_timestamp",
					"processing_spent_time",
					"processing_error",
					"processing_result",
					"min_threshold_value",
					"max_threshold_value",
				}).
					// With Rows Close error which is triggered by Scan error
					AddRow("100 wrong not-number", 10, nil, "minimum data",
						nil, nil, nil, nil, nil,
						nil, nil).CloseError(errRowsCloseError),

				result: map[uint64][]types.TaskColumnToAnalyze{
					10: []types.TaskColumnToAnalyze{
						types.TaskColumnToAnalyze{
							ColumnPKID:        100,
							TaskTimeRangePKID: 10,
							FileColumnName:    "minimum data",
							Result:            &types.StatsColumnResult{},
						},
					},
				},
			},
		}
	)

	ctx := context.Background()

	for _, tCase := range getColumnsTestCasesOk {
		tx, mock := testCreateMockDBTxxHelper(t, ctx)

		query := `SELECT\s+column_pk_id,\s*task_time_range_pk_id,\s*processed_values_count,\s*file_column_name,\s*processing_start_timestamp,\s*processing_end_timestamp,\s*processing_spent_time,\s*processing_error,\s*processing_result,\s*min_threshold_value,\s*max_threshold_value\s+FROM\s+columns_to_analyze\s+WHERE\s+task_time_range_pk_id\s+IN\s+\(\s*\?\s*` +
			strings.Repeat(`\s*,\s*\?\s*`, len(tCase.args)-1) + `\);`

		mock.ExpectQuery(query).WillReturnRows(tCase.rows)

		_, err := getColumnsForTaskTimeRangesTransactContext(ctx, tx, tCase.args)
		if cs := errors.Cause(err); cs != errRowsCloseError {
			t.Errorf("Unexpected error actual = %v, expected = %v",
				cs, errRowsCloseError)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("SQL mock expectations were not met actual = %v, expected = %v",
				err, nil)
		}
	}
}
