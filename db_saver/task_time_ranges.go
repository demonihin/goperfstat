package db_saver

import (
	"context"
	"strings"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"gitlab.com/demonihin/goperfstat/web_server/types"
)

// getRangesForTasksTransactContext - Returns map of []TaskTimeRange for given Task PKIDs
// Key of the map is Task PKID
func getRangesForTasksTransactContext(
	ctx context.Context,
	tx *sqlx.Tx,
	taskPKIDs []interface{}) (
	tasksRanges map[uint64][]types.TaskTimeRange,
	taskRangePKIDs []interface{},
	err error) {

	if len(taskPKIDs) == 0 {
		return nil, nil, errors.Wrap(types.ErrZeroLengthInputSlice,
			"getRangesForTasksTransactContext: ")
	}

	// Build SQL query
	query := `SELECT
		range_pk_id,
		task_pk_id,
		range_start,
		range_end
		FROM task_time_ranges
		WHERE task_pk_id IN (?` + strings.Repeat(", ?", len(taskPKIDs)-1) + ");"

	// Execute SQL query
	rows, err := tx.QueryxContext(ctx, query, taskPKIDs...)
	if err != nil {
		return nil, nil, errors.Wrap(err,
			"getRangesForTasksTransactContext: Error querying task_time_ranges")
	}
	defer func() {
		if ierr := rows.Close(); ierr != nil {
			err = errors.Wrap(ierr,
				"getRangesForTasksTransactContext: Error closing rows cursor")
		}
	}()

	// Get data
	tasksRanges = make(map[uint64][]types.TaskTimeRange)
	taskRangePKIDs = make([]interface{}, 0)
	for rows.Next() {

		var tTR types.TaskTimeRange
		if err = rows.StructScan(&tTR); err != nil {
			return nil, nil, errors.Wrap(err,
				"getRangesForTasksTransactContext: Error StructScanning TaskTimeRange")
		}

		// Add TaskTimeRange to result
		tasksRanges[tTR.TaskPKID] = append(tasksRanges[tTR.TaskPKID], tTR)
		taskRangePKIDs = append(taskRangePKIDs, tTR.RangePKID)
	}
	if err := rows.Err(); err != nil {
		return nil, nil, err
	}

	return tasksRanges, taskRangePKIDs, nil
}
