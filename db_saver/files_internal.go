package db_saver

import (
	"context"
	"database/sql"
	"strings"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"gitlab.com/demonihin/goperfstat/web_server/types"
)

// getTimestampColumnInfoForFileTransactContext - Returns information about
// a Column which contains timestamp info for given file
func getTimestampColumnInfoForFileTransactContext(ctx context.Context,
	tx *sql.Tx, filePKID uint64) (*types.TimestampColumnInfo, error) {

	var (
		tsColName   sql.NullString
		tsColFormat sql.NullString
	)

	// Get data
	if err := tx.QueryRowContext(ctx, `
		SELECT 
		file_timestamp_column_name,
		file_timestamp_column_format
		FROM file_uploads
		WHERE file_pk_id = ?;`, filePKID).Scan(
		&tsColName, &tsColFormat); err != nil {

		return nil, errors.Wrap(err,
			"getTimestampColumnInfoForFileTransactContext: Error Scanning file_timestamp_column_name, file_timestamp_column_format")
	}

	if !tsColName.Valid || !tsColFormat.Valid {
		return nil, errors.Wrap(types.ErrCanNotAutoFindTimestapFormat,
			"getTimestampColumnInfoForFileTransactContext: Error: No Timestamp column data for file")
	}

	res := types.TimestampColumnInfo{
		TimestampColumnName:   tsColName.String,
		TimestampColumnFormat: tsColFormat.String,
	}

	return &res, nil
}

// getFileUploadNamesForTasksTransactContext - Returns names of files in uploads
// for given tasks. Map key is tasks.task_pk_id
func getFileUploadNamesForTasksTransactContext(ctx context.Context,
	tx *sqlx.Tx, taskPKIDs []interface{}) (fileUploads map[uint64]string, err error) {

	// Load data
	rows, err := tx.QueryContext(ctx, `
		SELECT file_upload_name, task_pk_id
		FROM file_uploads
		JOIN tasks ON tasks.file_to_process_pk_id = file_uploads.file_pk_id
		WHERE task_pk_id IN (?`+strings.Repeat(", ?", len(taskPKIDs)-1)+");",
		taskPKIDs...)
	if err != nil {
		return nil, errors.Wrap(err,
			"getFileUploadNamesForTasksTransactContext: Error getting file_uploads info from DB")
	}
	defer func() {
		if ierr := rows.Close(); ierr != nil {
			err = errors.Wrap(ierr,
				"getFileUploadNamesForTasksTransactContext: Error closing rows cursor")
		}
	}()

	// Make result
	fileUploads = make(map[uint64]string)
	for rows.Next() {

		var (
			taskPKID uint64
			fileName string
		)
		if err = rows.Scan(&fileName, &taskPKID); err != nil {
			return nil, errors.Wrap(err,
				"getFileUploadNamesForTasksTransactContext: Error Scanning task_pk_id and file_upload_name")
		}

		fileUploads[taskPKID] = fileName
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}

	return fileUploads, nil
}

func forceFileReanalyzingTrnsactContext(ctx context.Context, tx *sql.Tx,
	filePKID uint64) (err error) {

	sqlRes, err := tx.ExecContext(ctx, `
		UPDATE file_uploads
		SET
		file_has_been_analyzed = false
		WHERE file_pk_id = ?;`,
		filePKID)
	if err != nil {
		return errors.Wrap(err,
			"forceFileReanalyzingTrnsactContext: Error updating file_uploads with Timestamp column info")
	}

	rc, err := sqlRes.RowsAffected()
	if err != nil {
		return errors.Wrap(err,
			"forceFileReanalyzingTrnsactContext: Error getting affected rows count")
	}

	if rc != 1 {
		return errors.Wrap(types.ErrNotOneRowUpdated,
			"forceFileReanalyzingTrnsactContext: Error: updated rows count is not equal to 1")
	}

	return nil
}

// parseSQLFileInfoRows - Loads FileInfo from database's rows
func parseSQLFileInfoRows(rows *sqlx.Rows, sessionID string) ([]types.FileInfo, error) {

	resFileInfo := make([]types.FileInfo, 0)
	for rows.Next() {

		var fi types.DBintFileInfo

		if err := rows.StructScan(&fi); err != nil {
			return nil, errors.Wrap(err,
				"parseSQLFileInfoRows: Error StructScanning intFileInfo")
		}

		fi.FileOwnerSessionID = sessionID

		cRes, err := fi.ToFileInfo()
		if err != nil {
			return nil, errors.WithStack(err)
		}
		resFileInfo = append(resFileInfo, *cRes)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}

	return resFileInfo, nil
}
