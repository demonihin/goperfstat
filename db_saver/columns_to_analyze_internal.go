package db_saver

import (
	"context"
	"strings"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"gitlab.com/demonihin/goperfstat/web_server/types"
)

// getColumnsForTaskTimeRangesTransactContext - Returns []TaskColumnToAnalyze for
// given TaskTimeRange PKIDs. Key of returning map is TaskTimeRange PKID
func getColumnsForTaskTimeRangesTransactContext(
	ctx context.Context,
	tx *sqlx.Tx,
	tasksRangesPKIDs []interface{}) (
	tasksColumns map[uint64][]types.TaskColumnToAnalyze, err error) {

	if len(tasksRangesPKIDs) == 0 {
		return nil, errors.WithStack(types.ErrZeroLengthInputSlice)
	}

	// Build SQL query to get Columns
	query := `SELECT
		column_pk_id,
		task_time_range_pk_id,
		processed_values_count,
		file_column_name,
		processing_start_timestamp,
		processing_end_timestamp,
		processing_spent_time,
		processing_error,
		processing_result,
		min_threshold_value,
		max_threshold_value
		FROM  columns_to_analyze
		WHERE task_time_range_pk_id IN (? ` + strings.Repeat(
		", ?", len(tasksRangesPKIDs)-1) + ");"

	// Execute SQL query
	rows, err := tx.QueryxContext(ctx, query, tasksRangesPKIDs...)
	if err != nil {
		return nil, errors.Wrap(err,
			"getColumnsForTasksTransactContext: Error getting []TaskColumnToAnalyze")
	}
	defer func() {
		if ierr := rows.Close(); ierr != nil {
			err = ierr
		}
	}()

	// Get rows
	tasksColumns = make(map[uint64][]types.TaskColumnToAnalyze)
	for rows.Next() {

		var tc types.DBintTaskColumnToAnalyze
		if err = rows.StructScan(&tc); err != nil {
			return nil, errors.Wrap(err,
				"getColumnsForTasksTransactContext: Error StructScanning TaskColumnToAnalyze")
		}

		// Convert
		tcR, err := tc.ToTaskColumnToAnalyze()
		if err != nil {
			return nil, errors.WithStack(err)
		}

		// Save result
		tasksColumns[tc.TaskTimeRangePKID] = append(
			tasksColumns[tc.TaskTimeRangePKID], *tcR)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}

	return tasksColumns, nil
}
