package db_saver

import (
	"github.com/jmoiron/sqlx"

	"github.com/pkg/errors"
)

type DBSaver struct {
	db *sqlx.DB
}

// InitDBTables - Initializes (creates) database structure
func InitDBTables(dbCon *sqlx.DB) (sv *DBSaver, err error) {

	// Create tables
	transact, err := dbCon.Begin()
	if err != nil {
		return nil, errors.Wrap(err,
			"InitDBTables: Error opening transaction")
	}
	defer func() {
		if err == nil {
			err = errors.Wrap(transact.Commit(),
				"InitDBTables: Error committing transaction")
		} else {
			if ierr := transact.Rollback(); ierr != nil {
				err = errors.Wrap(ierr,
					"InitDBTables: Error rolling back transaction")
			}
		}
	}()

	_, err = transact.Exec(`
	CREATE TABLE IF NOT EXISTS "sessions" (
		"session_pk_id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
		"session_id"	TEXT NOT NULL UNIQUE,
		"start_timestamp"	DATETIME NOT NULL,
		"end_timestamp"	DATETIME,
		"last_action_timestamp" DATETIME NOT NULL
	);`)
	if err != nil {
		return nil, errors.Wrap(err,
			"InitDBTables: Error creating table sessions")
	}

	_, err = transact.Exec(`
	CREATE TABLE IF NOT EXISTS "file_uploads" (
		"file_pk_id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
		"file_orig_name"	TEXT NOT NULL,
		"file_upload_name"	TEXT NOT NULL,
		"file_has_been_analyzed" INTEGER NOT NULL,
		"file_analyzing_result" TEXT,
		"file_analyzing_error" TEXT,
		"file_timestamp_column_name" TEXT,
		"file_timestamp_column_format" TEXT,
		"file_upload_timestamp" DATETIME NOT NULL,
		"file_size" INTEGER NOT NULL,
		"session_pk_id"	INTEGER NOT NULL,
		FOREIGN KEY(session_pk_id) REFERENCES sessions(session_pk_id) 
		ON DELETE CASCADE ON UPDATE CASCADE
	);`)
	if err != nil {
		return nil, errors.Wrap(err,
			"InitDBTables: Error creating table file_uploads")
	}

	_, err = transact.Exec(`
	CREATE TABLE IF NOT EXISTS "tasks" (
		"task_pk_id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
		"task_create_timestamp" DATETIME NOT NULL,
		"task_processed"	INTEGER NOT NULL,
		"task_processing_info"	TEXT,
		"task_processing_error"	TEXT,
		"task_processing_spent_time" 		INTEGER,
		"task_processing_start_timestamp" 	DATETIME,
		"task_processing_end_timestamp" 	DATETIME,
		"file_to_process_pk_id" INTEGER NOT NULL,
		"task_uuid" TEXT NOT NULL,
		"task_name" TEXT,
		FOREIGN KEY (file_to_process_pk_id) REFERENCES file_uploads(file_pk_id)
		ON DELETE CASCADE ON UPDATE CASCADE
	);`)
	if err != nil {
		return nil, errors.Wrap(err,
			"InitDBTables: Error creating table tasks")
	}

	_, err = transact.Exec(`
	CREATE TABLE IF NOT EXISTS "task_time_ranges" (
		"range_pk_id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
		"task_pk_id"	INTEGER NOT NULL,
		"range_start"	DATETIME NOT NULL,
		"range_end"		DATETIME NOT NULL,
		FOREIGN KEY(task_pk_id) REFERENCES tasks(task_pk_id)
		ON DELETE CASCADE ON UPDATE CASCADE
	);`)
	if err != nil {
		return nil, errors.Wrap(err,
			"InitDBTables: Error creating table task_time_ranges")
	}

	_, err = transact.Exec(`
	CREATE TABLE IF NOT EXISTS "columns_to_analyze" (
		"column_pk_id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
		"processed_values_count" INTEGER,
		"processing_start_timestamp" DATETIME,
		"processing_end_timestamp" DATETIME,
		"processing_spent_time" INTEGER,
		"processing_error" TEXT,
		"processing_result" TEXT,
		"min_threshold_value" REAL,
		"max_threshold_value" REAL,
		"task_time_range_pk_id" INTEGER NOT NULL,
		"file_column_name" TEXT NOT NULL,
		FOREIGN KEY (task_time_range_pk_id) REFERENCES task_time_ranges(range_pk_id)
		ON DELETE CASCADE ON UPDATE CASCADE
	);`)
	if err != nil {
		return nil, errors.Wrap(err,
			"InitDBTables: Error creating table columns_to_analyze")
	}

	return &DBSaver{
		db: dbCon,
	}, nil
}
