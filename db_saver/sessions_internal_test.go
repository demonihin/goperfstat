package db_saver

import (
	"context"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/pkg/errors"
)

func TestGetSessionPKIDTransactContext(t *testing.T) {
	getSessionsOk := "test getSessionPKIDTransactContext Ok"
	if !t.Run(getSessionsOk, testGetSessionPKIDTransactContextOk) {
		t.Errorf("Failed subtest %s", getSessionsOk)
	}

	getSessionQueryError := "test getSessionPKIDTransactContext query error"
	if !t.Run(getSessionQueryError, testGetSessionPKIDTransactContextQueryError) {
		t.Errorf("Failed subtest %s", getSessionQueryError)
	}
}

func testGetSessionPKIDTransactContextOk(t *testing.T) {
	// Prepare data
	var (
		sessionKeyID       = "session key 1"
		sessionPKID  int64 = 102
	)

	ctx := context.Background()
	tx, mock := testCreateMockDBTxHelper(t, ctx)

	mock.ExpectQuery("SELECT session_pk_id FROM sessions WHERE end_timestamp IS NULL AND session_id = ?").
		WithArgs(sessionKeyID).WillReturnRows(sqlmock.NewRows(
		[]string{"session_pk_id"}).AddRow(sessionPKID))

	gotPKID, err := getSessionPKIDTransactContext(ctx, tx, sessionKeyID)
	if err != nil {
		t.Fatalf("Unexpected error: actual = %v, expected = %v",
			err, nil)
	}

	if gotPKID != sessionPKID {
		t.Fatalf("Got session PK ID value %d is not equal to expected %d",
			gotPKID, sessionPKID)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("Not all SQL mock expectations were met: %v", err)
	}
}

func testGetSessionPKIDTransactContextQueryError(t *testing.T) {
	// Prepare data
	sqlQueryError := errors.New("test query error")
	sessionTestKey := "test session key"

	ctx := context.Background()
	tx, mock := testCreateMockDBTxHelper(t, ctx)

	mock.ExpectQuery("SELECT session_pk_id FROM sessions WHERE end_timestamp IS NULL AND session_id = ?").
		WithArgs(sessionTestKey).WillReturnError(sqlQueryError)

	_, err := getSessionPKIDTransactContext(ctx, tx, sessionTestKey)
	if err != nil {
		if cs := errors.Cause(err); cs != sqlQueryError {
			t.Fatalf("Unexpected error: actual = %v, expected = %v",
				cs, sqlQueryError)
		}
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("Not all SQL mock expectations were met: %v", err)
	}
}
