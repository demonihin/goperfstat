package db_saver

import (
	"testing"
	"time"

	"context"

	"reflect"

	"database/sql/driver"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/pkg/errors"
	"gitlab.com/demonihin/goperfstat/web_server/types"
)

var (
	errTestGetTimestampColumnInfoForFileTransactContextSelect = errors.New("select error")
	errTestGetFileUploadNamesForTasksTransactContextSelect    = errors.New("select error")
)

// Tests for GetTimestampColumnInfoForFileTransactContext
type sqlQueryColumnsTestCaseTimestampColumnInfo struct {
	args uint64
	rows *sqlmock.Rows

	result *types.TimestampColumnInfo
}

func TestGetTimestampColumnInfoForFileTransactContext(t *testing.T) {
	testSelectError := "test select columns query error"
	if success := t.Run(testSelectError,
		testGetTimestampColumnInfoForFileTransactContextSelectError); !success {
		t.Errorf("Failed subtest %s", testSelectError)
	}

	testSelectColumnsOk := "test select columns query ok"
	if !t.Run(testSelectColumnsOk,
		testGetTimestampColumnInfoForFileTransactContextOk) {
		t.Errorf("Failed subtest %s", testSelectColumnsOk)
	}

	testIncorrectColumnSQLDriverValuesScan := "test incorrect driver values returned on Scan"
	if success := t.Run(testIncorrectColumnSQLDriverValuesScan,
		testGetTimestampColumnInfoForFileTransactContextIncorrectValue); !success {

		t.Errorf("Failed subtest %s", testIncorrectColumnSQLDriverValuesScan)
	}
}

func testGetTimestampColumnInfoForFileTransactContextIncorrectValue(t *testing.T) {
	testTable := []sqlQueryColumnsTestCaseTimestampColumnInfo{
		// TimestampColumnFormat is NULL
		sqlQueryColumnsTestCaseTimestampColumnInfo{
			args: 1,
			rows: sqlmock.NewRows(
				[]string{"file_timestamp_column_name", "file_timestamp_column_format"}).
				AddRow("col name 1", nil),
		},
		// TimestampColumnName is NULL
		sqlQueryColumnsTestCaseTimestampColumnInfo{
			args: 1,
			rows: sqlmock.NewRows(
				[]string{"file_timestamp_column_name", "file_timestamp_column_format"}).
				AddRow(nil, "Jan 2, 2006 at 3:04pm (MST)"),
		},
	}

	for _, tc := range testTable {
		ctx := context.Background()
		tx, mock := testCreateMockDBTxHelper(t, ctx)

		mock.ExpectQuery(
			`SELECT\s+file_timestamp_column_name,\s*file_timestamp_column_format\s+FROM\s+file_uploads\s+WHERE file_pk_id\s*=`).
			WithArgs(tc.args).WillReturnRows(tc.rows)

		_, err := getTimestampColumnInfoForFileTransactContext(
			ctx, tx, tc.args)
		if err != nil {
			if cs := errors.Cause(err); cs != types.ErrCanNotAutoFindTimestapFormat {
				t.Errorf("Unexpected error cause, actual = %v, expected = %v", cs,
					errTestGetTimestampColumnInfoForFileTransactContextSelect)
			}
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("SQL mock expectations were not met actual = %v, expected = %v",
				err, nil)
		}
	}
}

func testGetTimestampColumnInfoForFileTransactContextSelectError(t *testing.T) {
	testTable := []sqlQueryColumnsTestCaseTimestampColumnInfo{
		sqlQueryColumnsTestCaseTimestampColumnInfo{
			args: 1,
			rows: sqlmock.NewRows(
				[]string{"file_timestamp_column_name", "file_timestamp_column_format"}).
				AddRow("col name 1", "Jan 2, 2006 at 3:04pm (MST)"),
		},
	}

	for _, tc := range testTable {
		ctx := context.Background()
		tx, mock := testCreateMockDBTxHelper(t, ctx)

		mock.ExpectQuery(
			`SELECT\s+file_timestamp_column_name,\s*file_timestamp_column_format\s+FROM\s+file_uploads\s+WHERE file_pk_id\s*=`).
			WithArgs(tc.args).WillReturnRows(tc.rows).WillReturnError(
			errTestGetTimestampColumnInfoForFileTransactContextSelect)

		_, err := getTimestampColumnInfoForFileTransactContext(
			ctx, tx, tc.args)
		if err != nil {
			if cs := errors.Cause(err); cs != errTestGetTimestampColumnInfoForFileTransactContextSelect {
				t.Errorf("Unexpected error cause, actual = %v, expected = %v", cs,
					errTestGetTimestampColumnInfoForFileTransactContextSelect)
			}
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("SQL mock expectations were not met actual = %v, expected = %v",
				err, nil)
		}
	}
}

func testGetTimestampColumnInfoForFileTransactContextOk(t *testing.T) {

	testTable := []sqlQueryColumnsTestCaseTimestampColumnInfo{
		sqlQueryColumnsTestCaseTimestampColumnInfo{
			args: 1,
			rows: sqlmock.NewRows(
				[]string{"file_timestamp_column_name", "file_timestamp_column_format"}).
				AddRow("col name 1", "Jan 2, 2006 at 3:04pm (MST)"),
			result: &types.TimestampColumnInfo{
				TimestampColumnName:   "col name 1",
				TimestampColumnFormat: "Jan 2, 2006 at 3:04pm (MST)",
			},
		},
	}

	for _, tc := range testTable {
		ctx := context.Background()
		tx, mock := testCreateMockDBTxHelper(t, ctx)

		mock.ExpectQuery(
			`SELECT\s+file_timestamp_column_name,\s*file_timestamp_column_format\s+FROM\s+file_uploads\s+WHERE file_pk_id\s*=`).
			WithArgs(tc.args).WillReturnRows(tc.rows)

		tsCol, err := getTimestampColumnInfoForFileTransactContext(
			ctx, tx, tc.args)
		if err != nil {
			t.Errorf("Unexpected error actual = %v, expected = %v", err, nil)
		}
		if !reflect.DeepEqual(tsCol, tc.result) {
			t.Errorf("Unexpected not equal result actual = %v, expected = %v",
				tsCol, tc.result)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("SQL mock expectations were not met actual = %v, expected = %v",
				err, nil)
		}
	}
}

// Tests for getFileUploadNamesForTasksTransactContext
type sqlQueryColumnsTestCaseFileUploadNamesForTasks struct {
	args []driver.Value
	rows *sqlmock.Rows

	result map[uint64]string
}

func TestGetFileUploadNamesForTasksTransactContext(t *testing.T) {
	testSelectError := "test select columns query error"
	if success := t.Run(testSelectError,
		testGetFileUploadNamesForTasksTransactContextQueryError); !success {
		t.Errorf("Failed subtest %s", testSelectError)
	}

	testSelectColumnsOk := "test select columns query ok"
	if !t.Run(testSelectColumnsOk,
		testGetFileUploadNamesForTasksTransactContextOk) {
		t.Errorf("Failed subtest %s", testSelectColumnsOk)
	}

	testIncorrectColumnSQLDriverValuesScan := "test incorrect driver values returned on Scan"
	if success := t.Run(testIncorrectColumnSQLDriverValuesScan,
		testGetFileUploadNamesForTasksTransactContextIncorrectValue); !success {

		t.Errorf("Failed subtest %s", testIncorrectColumnSQLDriverValuesScan)
	}

	testRowsCloseError := "test rows close returns error"
	if success := t.Run(testRowsCloseError,
		testGetFileUploadNamesForTasksTransactContextRowsCloseError); !success {

		t.Errorf("Failed subtest %s", testRowsCloseError)
	}
}

func testGetFileUploadNamesForTasksTransactContextOk(t *testing.T) {
	testTable := []sqlQueryColumnsTestCaseFileUploadNamesForTasks{
		sqlQueryColumnsTestCaseFileUploadNamesForTasks{
			args: []driver.Value{1, 2, 3},

			rows: sqlmock.NewRows([]string{"file_upload_name", "task_pk_id"}).
				AddRow("d029ca33-8e08-4891-aab3-d43d24568444", 1).
				AddRow("dc4a7ed2-d807-489b-b113-c26c5ec30bb5", 2),
			result: map[uint64]string{
				1: "d029ca33-8e08-4891-aab3-d43d24568444",
				2: "dc4a7ed2-d807-489b-b113-c26c5ec30bb5",
			},
		},
	}

	for _, tc := range testTable {
		ctx := context.Background()
		tx, mock := testCreateMockDBTxxHelper(t, ctx)

		mock.ExpectQuery(`SELECT\s+file_upload_name,\s*task_pk_id\s+FROM\s+file_uploads\s+JOIN tasks ON tasks.file_to_process_pk_id\s*=\s*file_uploads.file_pk_id\s+WHERE\s+task_pk_id\s+IN`).
			WithArgs(tc.args...).WillReturnRows(tc.rows)

		// Make []interface{} from args
		iArgs := make([]interface{}, 0, len(tc.args))
		for i := range tc.args {
			iArgs = append(iArgs, interface{}(tc.args[i]))
		}

		res, err := getFileUploadNamesForTasksTransactContext(ctx, tx, iArgs)
		if err != nil {
			t.Errorf("Unexpected error actual = %v, expected = %v", err, nil)
		}

		if !reflect.DeepEqual(res, tc.result) {
			t.Errorf("Unexpected not equal result actual = %v, expected = %v",
				res, tc.result)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("SQL mock expectations were not met actual = %v, expected = %v",
				err, nil)
		}
	}
}

func testGetFileUploadNamesForTasksTransactContextQueryError(t *testing.T) {

	testTable := []sqlQueryColumnsTestCaseFileUploadNamesForTasks{
		sqlQueryColumnsTestCaseFileUploadNamesForTasks{
			args: []driver.Value{1, 2, 3},

			rows: sqlmock.NewRows([]string{"file_upload_name", "task_pk_id"}).
				AddRow("d029ca33-8e08-4891-aab3-d43d24568444", 1).
				AddRow("dc4a7ed2-d807-489b-b113-c26c5ec30bb5", 2),
			result: map[uint64]string{
				1: "d029ca33-8e08-4891-aab3-d43d24568444",
				2: "dc4a7ed2-d807-489b-b113-c26c5ec30bb5",
			},
		},
	}

	for _, tc := range testTable {

		argsI := make([]interface{}, len(tc.args))
		for i := range tc.args {
			argsI[i] = tc.args[i]
		}

		ctx := context.Background()
		tx, mock := testCreateMockDBTxxHelper(t, ctx)

		mock.ExpectQuery(`SELECT\s+file_upload_name,\s*task_pk_id\s+FROM\s+file_uploads\s+JOIN tasks ON tasks.file_to_process_pk_id\s*=\s*file_uploads.file_pk_id\s+WHERE\s+task_pk_id\s+IN`).
			WithArgs(tc.args...).WillReturnError(
			errTestGetFileUploadNamesForTasksTransactContextSelect)

		_, err := getFileUploadNamesForTasksTransactContext(ctx, tx, argsI)
		if err != nil {
			if cs := errors.Cause(err); cs != errTestGetFileUploadNamesForTasksTransactContextSelect {
				t.Errorf("Unexpected error cause, actual=%v, expected = %v", cs,
					errTestGetFileUploadNamesForTasksTransactContextSelect)
			}
		} else {
			t.Errorf("Test did not return %v error",
				errTestGetFileUploadNamesForTasksTransactContextSelect)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("SQL mock expectations were not met actual = %v, expected = %v",
				err, nil)
		}

	}
}

func testGetFileUploadNamesForTasksTransactContextIncorrectValue(t *testing.T) {
	testTable := []sqlQueryColumnsTestCaseFileUploadNamesForTasks{
		sqlQueryColumnsTestCaseFileUploadNamesForTasks{
			args: []driver.Value{1, 2, 3},

			rows: sqlmock.NewRows([]string{"file_upload_name", "task_pk_id"}).
				AddRow("d029ca33-8e08-4891-aab3-d43d24568444", nil). // must not be nil
				AddRow("dc4a7ed2-d807-489b-b113-c26c5ec30bb5", 2),
			result: map[uint64]string{
				1: "d029ca33-8e08-4891-aab3-d43d24568444",
				2: "dc4a7ed2-d807-489b-b113-c26c5ec30bb5",
			},
		},
	}

	for _, tc := range testTable {
		ctx := context.Background()
		tx, mock := testCreateMockDBTxxHelper(t, ctx)

		mock.ExpectQuery(`SELECT\s+file_upload_name,\s*task_pk_id\s+FROM\s+file_uploads\s+JOIN tasks ON tasks.file_to_process_pk_id\s*=\s*file_uploads.file_pk_id\s+WHERE\s+task_pk_id\s+IN`).
			WithArgs(tc.args...).WillReturnRows(tc.rows)

		// Make []interface{} from args
		iArgs := make([]interface{}, 0, len(tc.args))
		for i := range tc.args {
			iArgs = append(iArgs, interface{}(tc.args[i]))
		}

		_, err := getFileUploadNamesForTasksTransactContext(ctx, tx, iArgs)
		if err == nil {
			t.Fatalf("Unexpected error == nil. Error must be sql Scan error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("SQL mock expectations were not met actual = %v, expected = %v",
				err, nil)
		}
	}
}

func testGetFileUploadNamesForTasksTransactContextRowsCloseError(t *testing.T) {
	rowsCloseErr := errors.New("rows close error")

	testTable := []sqlQueryColumnsTestCaseFileUploadNamesForTasks{
		sqlQueryColumnsTestCaseFileUploadNamesForTasks{
			args: []driver.Value{1, 2, 3},

			rows: sqlmock.NewRows([]string{"file_upload_name", "task_pk_id"}).
				AddRow("d029ca33-8e08-4891-aab3-d43d24568444", nil). // to make explicit deferred rows.Close
				AddRow("dc4a7ed2-d807-489b-b113-c26c5ec30bb5", 2).
				CloseError(rowsCloseErr),
			result: map[uint64]string{
				1: "d029ca33-8e08-4891-aab3-d43d24568444",
				2: "dc4a7ed2-d807-489b-b113-c26c5ec30bb5",
			},
		},
	}

	for _, tc := range testTable {
		ctx := context.Background()
		tx, mock := testCreateMockDBTxxHelper(t, ctx)

		mock.ExpectQuery(`SELECT\s+file_upload_name,\s*task_pk_id\s+FROM\s+file_uploads\s+JOIN tasks ON tasks.file_to_process_pk_id\s*=\s*file_uploads.file_pk_id\s+WHERE\s+task_pk_id\s+IN`).
			WithArgs(tc.args...).WillReturnRows(tc.rows)

		// Make []interface{} from args
		iArgs := make([]interface{}, 0, len(tc.args))
		for i := range tc.args {
			iArgs = append(iArgs, interface{}(tc.args[i]))
		}

		_, err := getFileUploadNamesForTasksTransactContext(ctx, tx, iArgs)
		if err != nil {
			if cs := errors.Cause(err); cs != rowsCloseErr {
				t.Errorf("Unexpected error cause: actual = %v, expected = %v",
					cs, rowsCloseErr)
			}
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("SQL mock expectations were not met actual = %v, expected = %v",
				err, nil)
		}
	}
}

func TestForceFileReanalyzingTrnsactContext(t *testing.T) {
	testUpdateFileOk := "test forceFileReanalyzingTrnsactContext ok"
	if !t.Run(testUpdateFileOk,
		testForceFileReanalyzingTrnsactContextUpdateOk) {
		t.Errorf("Failed subtest %s", testUpdateFileOk)
	}

	testUpdateFileSQLError := "test forceFileReanalyzingTrnsactContext SQL Error"
	if !t.Run(testUpdateFileSQLError,
		testForceFileReanalyzingTrnsactContextUpdateSQLError) {
		t.Errorf("Failed subtest %s", testUpdateFileSQLError)
	}

	testUpdateFileIncorrectAffectedRCount := "test forceFileReanalyzingTrnsactContext Incorrect affected rows count"
	if !t.Run(testUpdateFileIncorrectAffectedRCount,
		testForceFileReanalyzingTrnsactContextUpdateIncorrectAffectedRowsCount) {
		t.Errorf("Failed subtest %s", testUpdateFileIncorrectAffectedRCount)
	}

	testUpdateFileIncorrectSQLResult := "test forceFileReanalyzingTrnsactContext Incorrect SQL result"
	if !t.Run(testUpdateFileIncorrectSQLResult,
		testForceFileReanalyzingTrnsactContextUpdateNoSqlResult) {
		t.Errorf("Failed subtest %s", testUpdateFileIncorrectSQLResult)
	}
}

func testForceFileReanalyzingTrnsactContextUpdateOk(t *testing.T) {
	ctx := context.Background()
	tx, mock := testCreateMockDBTxHelper(t, ctx)

	mock.ExpectExec(`\s*UPDATE\s+file_uploads\s+SET\s+file_has_been_analyzed\s*=\s*false\s+WHERE\s+file_pk_id\s*=`).
		WithArgs(1).WillReturnResult(sqlmock.NewResult(0, 1))

	if err := forceFileReanalyzingTrnsactContext(ctx, tx, 1); err != nil {
		t.Errorf("Unexpected error, actual = %v, expected = %v", err, nil)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("SQL mock expectations were not met actual = %v, expected = %v",
			err, nil)
	}
}

func testForceFileReanalyzingTrnsactContextUpdateSQLError(t *testing.T) {
	sqlErr := errors.New("test update error")

	ctx := context.Background()
	tx, mock := testCreateMockDBTxHelper(t, ctx)

	mock.ExpectExec(`\s*UPDATE\s+file_uploads\s+SET\s+file_has_been_analyzed\s*=\s*false\s+WHERE\s+file_pk_id\s*=`).
		WithArgs(1).WillReturnError(sqlErr)

	if err := forceFileReanalyzingTrnsactContext(ctx, tx, 1); err != nil {
		if cs := errors.Cause(err); cs != sqlErr {
			t.Errorf("Unexpected error, actual = %v, expected = %v", cs, sqlErr)
		}
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("SQL mock expectations were not met actual = %v, expected = %v",
			err, nil)
	}
}

func testForceFileReanalyzingTrnsactContextUpdateNoSqlResult(t *testing.T) {
	sqlResErr := errors.New("error getting SQL result")

	ctx := context.Background()
	tx, mock := testCreateMockDBTxHelper(t, ctx)

	mock.ExpectExec(`\s*UPDATE\s+file_uploads\s+SET\s+file_has_been_analyzed\s*=\s*false\s+WHERE\s+file_pk_id\s*=`).
		WithArgs(1).WillReturnResult(sqlmock.NewErrorResult(sqlResErr))

	if err := forceFileReanalyzingTrnsactContext(ctx, tx, 1); err != nil {
		if cs := errors.Cause(err); cs != sqlResErr {
			t.Errorf("Unexpected error, actual = %v, expected = %v", cs, sqlResErr)
		}
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("SQL mock expectations were not met actual = %v, expected = %v",
			err, nil)
	}
}

func testForceFileReanalyzingTrnsactContextUpdateIncorrectAffectedRowsCount(t *testing.T) {
	ctx := context.Background()
	tx, mock := testCreateMockDBTxHelper(t, ctx)

	mock.ExpectExec(`\s*UPDATE\s+file_uploads\s+SET\s+file_has_been_analyzed\s*=\s*false\s+WHERE\s+file_pk_id\s*=`).
		WithArgs(1).WillReturnResult(sqlmock.NewResult(0, 2)) // Incorrect affected rows count

	if err := forceFileReanalyzingTrnsactContext(ctx, tx, 1); err != nil {
		if cs := errors.Cause(err); cs != types.ErrNotOneRowUpdated {
			t.Errorf("Unexpected error, actual = %v, expected = %v", cs, types.ErrNotOneRowUpdated)
		}
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("SQL mock expectations were not met actual = %v, expected = %v",
			err, nil)
	}
}

func TestParseSQLFileIntoRows(t *testing.T) {
	testParseRowsOk := "test parse rows - Ok"
	if success := t.Run(testParseRowsOk,
		testParseSQLFileIntoRowsOk); !success {
		t.Errorf("Failed subtest %s", testParseRowsOk)
	}

	testParseRowsScanError := "test parse rows - Scan Error"
	if success := t.Run(testParseRowsScanError,
		testParseSQLFileIntoRowsScanError); !success {
		t.Errorf("Failed subtest %s", testParseRowsScanError)
	}
}

func testParseSQLFileIntoRowsOk(t *testing.T) {
	// Prepare data
	timestampColNames := []string{
		"timestamp column name 1",
		"timestamp column name 2",
	}
	timestampColFormat := "%y-%M-%M %H-%m-%S"

	sqlRows := sqlmock.NewRows(
		[]string{
			"file_pk_id",
			"session_pk_id",
			"file_size",
			"file_orig_name",
			"file_upload_name",
			"file_has_been_analyzed",
			"file_analyzing_result",
			"file_analyzing_error",
			"file_timestamp_column_name",
			"file_timestamp_column_format",
			"file_upload_timestamp"}).
		AddRow(1, 1, 100, "file orig name 1", "UUID1", true, "{}",
			nil, timestampColNames[0], timestampColFormat, time.Unix(1564755209, 0)).
		AddRow(2, 1, 101, "file orig name 2", "UUID2", true, "{}",
			nil, timestampColNames[1], timestampColFormat, time.Unix(1564755229, 0)).
		AddRow(3, 1, 102, "file orig name 3", "UUID3", false, nil,
			nil, nil, nil, time.Unix(1564755209, 0))
	sessionID := "session id"

	scannedFiles := []types.FileInfo{
		types.FileInfo{
			FilePKID:                  1,
			FileCreatorSessionPKID:    1,
			FileSize:                  100,
			FileOwnerSessionID:        sessionID,
			FileOriginalName:          "file orig name 1",
			FileUploadName:            "UUID1",
			FileHasBeenAnalyzed:       true,
			FileAnalyzingResult:       types.FileAnalyzeResult{},
			FileAnalyzingErrors:       nil,
			FileTimestampColumnName:   &timestampColNames[0],
			FileTimestampColumnFormat: &timestampColFormat,
			FileUploadTimestamp:       time.Unix(1564755209, 0),
		},
		types.FileInfo{
			FilePKID:                  2,
			FileCreatorSessionPKID:    1,
			FileSize:                  101,
			FileOwnerSessionID:        sessionID,
			FileOriginalName:          "file orig name 2",
			FileUploadName:            "UUID2",
			FileHasBeenAnalyzed:       true,
			FileAnalyzingResult:       types.FileAnalyzeResult{},
			FileAnalyzingErrors:       nil,
			FileTimestampColumnName:   &timestampColNames[1],
			FileTimestampColumnFormat: &timestampColFormat,
			FileUploadTimestamp:       time.Unix(1564755229, 0),
		},
		types.FileInfo{
			FilePKID:                  3,
			FileCreatorSessionPKID:    1,
			FileSize:                  102,
			FileOwnerSessionID:        sessionID,
			FileOriginalName:          "file orig name 3",
			FileUploadName:            "UUID3",
			FileHasBeenAnalyzed:       false,
			FileAnalyzingResult:       types.FileAnalyzeResult{},
			FileAnalyzingErrors:       nil,
			FileTimestampColumnName:   nil,
			FileTimestampColumnFormat: nil,
			FileUploadTimestamp:       time.Unix(1564755209, 0),
		},
	}

	ctx := context.Background()
	tx, mock := testCreateMockDBTxxHelper(t, ctx)
	mock.ExpectQuery("").WillReturnRows(sqlRows)

	rows, err := tx.QueryxContext(ctx, "")
	if err != nil {
		t.Fatalf("Unexpected error during executing test tx.QueryxContext: %v", err)
	}

	files, err := parseSQLFileInfoRows(rows, sessionID)
	if err != nil {
		t.Fatalf("Unexpected error, actual = %v, expected = %v", err, nil)
	}

	if len(files) != len(scannedFiles) {
		t.Fatalf("Scanned files count is not equal to expected, expected = %d, actual = %d",
			len(scannedFiles), len(files))
	}

	for i := range files {
		isEq := reflect.DeepEqual(files[i], scannedFiles[i])
		if !isEq {
			t.Errorf("Rows scan results are not equal: expected: %+v, actual: %+v",
				scannedFiles[i], files[i])
		}
	}
}

func testParseSQLFileIntoRowsScanError(t *testing.T) {
	// Prepare data
	timestampColNames := []string{
		"timestamp column name 1"}
	timestampColFormat := "%y-%M-%M %H-%m-%S"

	sqlRows := sqlmock.NewRows(
		[]string{
			"file_pk_id",
			"session_pk_id",
			"file_size",
			"file_orig_name",
			"file_upload_name",
			"file_has_been_analyzed",
			"file_analyzing_result",
			"file_analyzing_error",
			"file_timestamp_column_name",
			"file_timestamp_column_format",
			"file_upload_timestamp"}).AddRow(
		1, 1, nil, "file orig name 1", "UUID1", true, "{}", // third argument must not be nil
		nil, timestampColNames[0], timestampColFormat, time.Unix(1564755209, 0))
	sessionID := "session id"
	expectedErrorText := `sql: Scan error on column index 2, name "file_size": converting driver.Value type <nil> ("<nil>") to a uint64: invalid syntax`

	ctx := context.Background()
	tx, mock := testCreateMockDBTxxHelper(t, ctx)
	mock.ExpectQuery("").WillReturnRows(sqlRows)

	rows, err := tx.QueryxContext(ctx, "")
	if err != nil {
		t.Fatalf("Unexpected error during executing test tx.QueryxContext: %v", err)
	}

	_, err = parseSQLFileInfoRows(rows, sessionID)
	if err != nil {
		if cs := errors.Cause(err); cs.Error() != expectedErrorText {
			t.Fatalf("Unexpected error text, actual = %v, expected = %v", cs.Error(), expectedErrorText)
		}
	}

}
