module gitlab.com/demonihin/goperfstat/db_saver

go 1.12

require (
	github.com/DATA-DOG/go-sqlmock v1.3.3
	github.com/gin-gonic/gin v1.4.0 // indirect
	github.com/helloeave/json v1.11.1 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/mattn/go-sqlite3 v1.10.0
	github.com/pkg/errors v0.8.1
	github.com/satori/go.uuid v1.2.0
	gitlab.com/demonihin/goperfstat/web_server/types v0.0.0-00010101000000-000000000000
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)

replace gitlab.com/demonihin/goperfstat/web_server/types => ../types/
