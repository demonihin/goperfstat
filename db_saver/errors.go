package db_saver

import (
	"errors"
)

var (
	ErrEmptySliceAsResult = errors.New("Empty slice in result")
)
