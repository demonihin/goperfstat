package db_saver

import (
	"context"
	"testing"

	"github.com/jmoiron/sqlx"
)

func TestDBSaver_GetSessionPKIDContext(t *testing.T) {
	type fields struct {
		db *sqlx.DB
	}
	type args struct {
		ctx       context.Context
		sessionID string
	}
	tests := []struct {
		name            string
		fields          fields
		args            args
		wantSessionPKID int64
		wantErr         bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			sv := &DBSaver{
				db: tt.fields.db,
			}
			gotSessionPKID, err := sv.GetSessionPKIDContext(tt.args.ctx, tt.args.sessionID)
			if (err != nil) != tt.wantErr {
				t.Errorf("DBSaver.GetSessionPKIDContext() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotSessionPKID != tt.wantSessionPKID {
				t.Errorf("DBSaver.GetSessionPKIDContext() = %v, want %v", gotSessionPKID, tt.wantSessionPKID)
			}
		})
	}
}
