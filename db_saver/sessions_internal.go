package db_saver

import (
	"context"
	"database/sql"

	"github.com/pkg/errors"
)

// getSessionPKIDContext - Returns session.session_pk_id by its cookie session_id
func getSessionPKIDTransactContext(ctx context.Context,
	tx *sql.Tx, sessionID string) (int64, error) {

	var pkid int64
	err := tx.QueryRowContext(ctx,
		"SELECT session_pk_id FROM sessions WHERE end_timestamp IS NULL AND session_id = ?",
		sessionID).Scan(&pkid)
	if err != nil {
		return 0, errors.Wrap(err,
			"getSessionPKIDTransactContext: Error Scanning sessionPKID")
	}

	return pkid, nil
}
