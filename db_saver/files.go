package db_saver

import (
	"context"
	"database/sql"
	"encoding/json"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/demonihin/goperfstat/web_server/types"
)

// SaveUploadedFileInfoContext - Saves a new file info in DB
func (sv *DBSaver) SaveUploadedFileInfoContext(ctx context.Context,
	sessionID string, fileOriginalName string, fileUploadName string,
	fileSize int64) (err error) {

	// Transaction
	tx, err := sv.db.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		return errors.Wrap(err,
			"SaveUploadedFileInfoContext: Error opening transaction")
	}
	defer func() {
		if err == nil {
			err = errors.Wrap(tx.Commit(),
				"SaveUploadedFileInfoContext: Error commiting transaction")
		} else {
			if ierr := tx.Rollback(); ierr != nil {
				err = errors.Wrap(ierr,
					"SaveUploadedFileInfoContext: Error rolling back transaction")
			}
		}
	}()

	// Get sessionPKID
	sessionPKID, err := getSessionPKIDTransactContext(ctx, tx, sessionID)
	if err != nil {
		return errors.WithStack(err)
	}

	// Insert file record
	_, err = tx.ExecContext(ctx,
		`INSERT INTO file_uploads (session_pk_id, file_upload_timestamp,
			file_orig_name, file_upload_name, file_size, file_has_been_analyzed)
			VALUES (?, ?, ?, ?, ?, false);`,
		sessionPKID, time.Now(), fileOriginalName, fileUploadName, fileSize)
	if err != nil {
		return errors.Wrap(err,
			"SaveUploadedFileInfoContext: Error adding record to file_uploads")
	}

	return nil

}

// SaveFileAnalyzeResultContext - Saves results of analyzing for given file
func (sv *DBSaver) SaveFileAnalyzeResultContext(ctx context.Context,
	fAT *types.FileAnalyzeTask) (err error) {

	var (
		anRes                     sql.NullString
		errorText                 sql.NullString
		fileTimestampColumnName   sql.NullString
		fileTimestampColumnFormat sql.NullString
	)

	// Store result as json
	if fAT.Errors == nil {

		// Save analyzing result as JSON
		jsRes, err := json.Marshal(fAT.Result)
		if err != nil {
			return errors.Wrap(err,
				"SaveFileAnalyzeResultContext: Error Marshalling types.FileAnalyzeTask to JSON")
		}

		anRes.String = string(jsRes)
		anRes.Valid = true

		if fAT.Result.TimestampColumn != nil {
			fileTimestampColumnFormat.String, fileTimestampColumnFormat.Valid = fAT.Result.TimestampColumn.TimestampColumnFormat, true
			fileTimestampColumnName.String, fileTimestampColumnName.Valid = fAT.Result.TimestampColumn.TimestampColumnName, true
		}

	} else {

		errorText.Valid = true
		errorBytes, err := json.Marshal(fAT.Errors)
		if err != nil {
			return errors.Wrap(err,
				"SaveFileAnalyzeResultContext: Error marshalling types.FileAnalyzeTask.Errors to JSON")
		}
		errorText.String = string(errorBytes)
	}

	// Transaction
	tx, err := sv.db.BeginTx(ctx, nil)
	if err != nil {
		return errors.Wrap(err,
			"SaveFileAnalyzeResultContext: Error opening transaction")
	}
	defer func() {
		if err == nil {
			if ierr := tx.Commit(); ierr != nil {
				err = errors.Wrap(ierr,
					"SaveFileAnalyzeResultContext: Error commiting transaction")
			}
		} else {
			if ierr := tx.Rollback(); ierr != nil {
				err = errors.Wrap(ierr,
					"SaveFileAnalyzeResultContext: Error rolling back transaction")
			}
		}
	}()

	sqlRes, err := tx.ExecContext(ctx, `
		UPDATE file_uploads
		SET
		file_has_been_analyzed = true,
		file_analyzing_error = ?,
		file_analyzing_result = ?,
		file_timestamp_column_format = ?,
		file_timestamp_column_name = ?
		WHERE file_pk_id = ?;`,
		errorText, anRes,
		fileTimestampColumnFormat,
		fileTimestampColumnName,
		fAT.FileInformation.FilePKID)
	if err != nil {
		return errors.Wrap(err,
			"SaveFileAnalyzeResultContext: Error updating file_uploads with results")
	}

	ra, err := sqlRes.RowsAffected()
	if err != nil {
		return errors.Wrap(err,
			"SaveFileAnalyzeResultContext: Error getting RowsAffected")
	}

	if ra != 1 {
		return errors.Wrap(types.ErrNotOneRowUpdated,
			"SaveFileAnalyzeResultContext: Error: updated rows count is not equal to 1")
	}

	return nil
}

// SetFileTimestampColumnInfoContext - Updates TimestampColumnInfo for file given by its file_pk_id
func (sv *DBSaver) SetFileTimestampColumnInfoContext(ctx context.Context,
	filePKID uint64, ti *types.TimestampColumnInfo, forceFileReanalyze bool) (err error) {

	// Transaction
	tx, err := sv.db.BeginTx(ctx, nil)
	if err != nil {
		return errors.Wrap(err,
			"SetFileTimestampColumnInfoContext: Error opening transaction")
	}
	defer func() {
		if err == nil {
			if ierr := tx.Commit(); ierr != nil {
				err = errors.Wrap(ierr,
					"SetFileTimestampColumnInfoContext: Error committing transaction")
			}
		} else {
			if ierr := tx.Rollback(); ierr != nil {
				err = errors.Wrap(ierr,
					"SetFileTimestampColumnInfoContext: Error rolling back transaction")
			}
		}
	}()

	sqlRes, err := tx.ExecContext(ctx, `
		UPDATE file_uploads
		SET
		file_timestamp_column_format = ?,
		file_timestamp_column_name = ?
		WHERE file_pk_id = ?;`,
		ti.TimestampColumnFormat,
		ti.TimestampColumnName,
		filePKID)
	if err != nil {
		return errors.Wrap(err,
			"SetFileTimestampColumnInfoContext: Error updating file_uploads with Timestamp column info")
	}

	rc, err := sqlRes.RowsAffected()
	if err != nil {
		return errors.Wrap(err,
			"SetFileTimestampColumnInfoContext: Error getting affected rows count")
	}

	if rc != 1 {
		return errors.Wrap(types.ErrNotOneRowUpdated,
			"SetFileTimestampColumnInfoContext: Error: updated rows count is not equal to 1")
	}

	if forceFileReanalyze {
		if err = forceFileReanalyzingTrnsactContext(ctx, tx, filePKID); err != nil {
			return errors.WithStack(err)
		}
	}

	return nil
}

// GetFilesListForSessionContext - Returns  file info for given session
func (sv *DBSaver) GetFilesListForSessionContext(
	ctx context.Context, sessionID string) (fileInfo []types.FileInfo, err error) {

	tx, err := sv.db.BeginTxx(ctx, &sql.TxOptions{})
	if err != nil {
		return nil, errors.WithMessage(err, "Can not open transaction")
	}
	defer func() {
		if ierr := tx.Rollback(); ierr != nil {
			err = errors.WithMessage(ierr, "Can not rollback transaction")
		}
	}()

	// Find sessionPKID
	sessionPKID, err := getSessionPKIDTransactContext(ctx, tx.Tx, sessionID)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	// Get files
	rows, err := tx.QueryxContext(ctx,
		`SELECT 
		file_pk_id,
		file_orig_name,
		file_upload_name,
		file_has_been_analyzed,
		file_analyzing_result,
		file_analyzing_error,
		file_timestamp_column_name,
		file_timestamp_column_format,
		file_upload_timestamp,
		file_size,
		session_pk_id
		FROM file_uploads
		WHERE session_pk_id = ?;`,
		sessionPKID)
	if err != nil {
		return nil, errors.Wrap(err,
			"GetFilesListForSessionContext: Error getting data from file_uploads")
	}
	defer func() {
		if ierr := rows.Close(); ierr != nil {
			err = errors.WithStack(ierr)
		}
	}()

	resFileInfo, err := parseSQLFileInfoRows(rows, sessionID)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	// Load Tasks info for files
	if err = populateTaskInfoTransactContext(ctx, tx, resFileInfo); err != nil {
		return nil, errors.WithStack(err)
	}

	return resFileInfo, nil
}

// GetFilesForAnalyzingContext - Returns files which have not been analyzed yet
func (sv *DBSaver) GetFilesForAnalyzingContext(
	ctx context.Context, maxFiles int) (fi []types.FileInfo, err error) {

	transact, err := sv.db.BeginTxx(ctx, &sql.TxOptions{})
	if err != nil {
		return nil, errors.Wrap(err,
			"GetFilesForAnalyzingContext: Error opening transaction")
	}
	defer func() {
		if ierr := transact.Rollback(); ierr != nil {
			err = errors.Wrap(ierr,
				"GetFilesForAnalyzingContext: Error rolling back transaction")
		}
	}()

	rows, err := transact.QueryxContext(ctx, `
		SELECT 
		file_pk_id,
		file_orig_name,
		file_upload_name,
		file_has_been_analyzed,
		file_analyzing_result,
		file_analyzing_error,
		file_timestamp_column_name,
		file_timestamp_column_format,
		file_upload_timestamp,
		file_size,
		session_pk_id
		FROM file_uploads
		WHERE file_has_been_analyzed = false 
		ORDER BY file_upload_timestamp
		LIMIT ?;`, maxFiles)
	if err != nil {
		return nil, errors.Wrap(err,
			"GetFilesForAnalyzingContext: Error getting date from file_uploads")
	}

	fi, err = parseSQLFileInfoRows(rows, "")
	return fi, errors.WithStack(err)
}

// GetFileInfoForFileContext - Returns FileInfo for requested file
func (sv *DBSaver) GetFileInfoForFileContext(ctx context.Context,
	file_upload_name string, sessionID string) (fi *types.FileInfo, err error) {

	transact, err := sv.db.BeginTxx(ctx, &sql.TxOptions{})
	if err != nil {
		return nil, errors.Wrap(err,
			"GetFileInfoForFileContext: Error opening transaction")
	}
	defer func() {
		if ierr := transact.Rollback(); ierr != nil {
			err = errors.Wrap(ierr,
				"GetFileInfoForFileContext: Error rolling back transaction")
		}
	}()

	// Get FileInfo
	var finfo types.DBintFileInfo
	if err = transact.QueryRowxContext(ctx, `
		SELECT 
		file_pk_id,
		file_orig_name,
		file_upload_name,
		file_has_been_analyzed,
		file_analyzing_result,
		file_analyzing_error,
		file_timestamp_column_name,
		file_upload_timestamp,
		file_size,
		sessions.session_pk_id AS session_pk_id
		FROM file_uploads
		JOIN sessions ON sessions.session_pk_id = file_uploads.session_pk_id
		WHERE file_upload_name = ? AND session_id = ?`,
		file_upload_name, sessionID).StructScan(&finfo); err != nil {

		return nil, errors.Wrap(err,
			"GetFileInfoForFileContext: Error getting data from file_uploads")
	}
	finfo.FileOwnerSessionID = sessionID
	fi, err = finfo.ToFileInfo()
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return fi, nil
}

func (sv *DBSaver) GetTimestampColumnInfoForFileContext(ctx context.Context,
	filePKID uint64) (tColInfo *types.TimestampColumnInfo, err error) {

	tx, err := sv.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, errors.Wrap(err,
			"GetTimestampColumnInfoForFileContext: Error opening transaction")
	}
	defer func() {
		if ierr := tx.Rollback(); ierr != nil {
			err = errors.Wrap(ierr,
				"GetTimestampColumnInfoForFileContext: Error rolling back transaction")
		}
	}()

	tColInfo, err = getTimestampColumnInfoForFileTransactContext(ctx, tx, filePKID)
	return tColInfo, errors.WithStack(err)
}

// UpdateFileNameContext - Performs FileInfo File original name update
func (sv *DBSaver) UpdateFileNameContext(ctx context.Context,
	filePKID uint64, newFileOriginalName string) (err error) {

	tx, err := sv.db.BeginTx(ctx, nil)
	if err != nil {
		return errors.WithMessage(err,
			"UpdateFileNameContext: Error opening transaction")
	}
	defer func() {
		if err == nil {
			if ierr := tx.Commit(); ierr != nil {
				err = errors.WithMessagef(ierr,
					"UpdateFileNameContext: Error commiting transaction")
			}
		} else {
			if ierr := tx.Rollback(); ierr != nil {
				err = errors.WithMessage(ierr,
					"UpdateFileNameContext: Error rolling back transaction")
			}
		}
	}()

	// Perform update
	sqlRes, err := tx.ExecContext(ctx,
		`UPDATE file_uploads
		SET file_orig_name = ?
		WHERE file_pk_id = ?;`, newFileOriginalName, filePKID)
	if err != nil {
		return errors.WithMessage(err,
			"UpdateFileNameContext: Error updating file_orig_name")
	}

	rowsCount, err := sqlRes.RowsAffected()
	if err != nil {
		return errors.WithMessage(err,
			"UpdateFileNameContext: Error getting affected rows count")
	}

	if rowsCount != 1 {
		return errors.WithStack(types.ErrNotOneRowUpdated)
	}

	return nil
}

// DeleteFileInfoForFileContext - Deletes FileInfo from database and all its tasks
func (sv *DBSaver) DeleteFileInfoForFileContext(ctx context.Context,
	filePKID uint64) (err error) {

	tx, err := sv.db.BeginTx(ctx, nil)
	if err != nil {
		return errors.WithMessage(err,
			"DeleteFileInfoForFileContext: Error opening transaction")
	}
	defer func() {
		if err == nil {
			if ierr := tx.Commit(); ierr != nil {
				err = errors.WithMessagef(ierr,
					"DeleteFileInfoForFileContext: Error commiting transaction")
			}
		} else {
			if ierr := tx.Rollback(); ierr != nil {
				err = errors.WithMessage(ierr,
					"DeleteFileInfoForFileContext: Error rolling back transaction")
			}
		}
	}()

	// Delete
	sqlRes, err := tx.ExecContext(ctx,
		`DELETE FROM file_uploads
		WHERE file_pk_id = ?`, filePKID)
	if err != nil {
		return errors.WithMessage(err,
			"DeleteFileInfoForFileContext: Error updating file_orig_name")
	}

	rowsCount, err := sqlRes.RowsAffected()
	if err != nil {
		return errors.WithMessage(err,
			"DeleteFileInfoForFileContext: Error getting affected rows count")
	}

	if rowsCount != 1 {
		return errors.WithStack(types.ErrNotOneRowUpdated)
	}

	return nil
}
