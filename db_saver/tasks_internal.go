package db_saver

import (
	"context"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"gitlab.com/demonihin/goperfstat/web_server/types"
)

// populateTaskInfoTransactContext - loads subordinate slices for given files
func populateTaskInfoTransactContext(ctx context.Context,
	tx *sqlx.Tx, filesInfo []types.FileInfo) (err error) {

	for i, v := range filesInfo {

		// Get data
		locTasks, err := getTasksForFileTransactContext(ctx,
			tx, filesInfo[i].FilePKID)
		if err != nil {
			return errors.WithStack(err)
		}

		// Fill with file_upload_name
		for i := range locTasks {
			locTasks[i].FileUploadName = v.FileUploadName
		}

		filesInfo[i].Tasks = locTasks
	}

	return nil
}

// getTasksForFileTransactContext - Loads full TaskInfo for gived file_uploads.file_pk_id
func getTasksForFileTransactContext(ctx context.Context,
	tx *sqlx.Tx, filePKID uint64) ([]types.TaskInfo, error) {

	// Get Tasks
	rows, err := tx.QueryxContext(ctx, `
		SELECT
		tasks.task_pk_id AS task_pk_id,
		task_create_timestamp,
		task_processed,
		task_processing_info,
		task_processing_error,
		task_processing_spent_time,
		task_processing_start_timestamp,
		task_processing_end_timestamp,
		file_to_process_pk_id,
		task_uuid,
		task_name
		FROM tasks
		WHERE file_to_process_pk_id = ?;`, filePKID)
	if err != nil {
		return nil, errors.Wrap(err,
			"getTasksForFileTransactContext: Error getting tasks")
	}
	defer func() {
		if ierr := rows.Close(); ierr != nil {
			err = ierr
		}
	}()

	taskIDS := make([]interface{}, 0)
	tasks := make([]types.DBintTaskInfo, 0)
	for rows.Next() {

		var tempIntTaskInfo types.DBintTaskInfo

		if err := rows.StructScan(&tempIntTaskInfo); err != nil {
			return nil, errors.Wrap(err,
				"getTasksForFileTransactContext: Error StructScanning intTaskInfo")
		}

		tasks = append(tasks, tempIntTaskInfo)
		taskIDS = append(taskIDS, tempIntTaskInfo.TaskPKID)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}

	// Check case when Tasks list is empty
	if len(tasks) == 0 {
		return make([]types.TaskInfo, 0), nil
	}

	res, err := fillTasksWithRangesAndColumnsTransactContext(ctx, tx, taskIDS, tasks)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return res, nil
}

func fillTasksWithRangesAndColumnsTransactContext(ctx context.Context, tx *sqlx.Tx,
	taskIDS []interface{}, tasks []types.DBintTaskInfo) ([]types.TaskInfo, error) {

	// Get Tasks TimeRanges
	tasksRanges, taskRangePKIDs, err := getRangesForTasksTransactContext(ctx, tx, taskIDS)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	// Get TasksTimeRange Columns to Analyze
	tasksTimeRangeColumns, err := getColumnsForTaskTimeRangesTransactContext(
		ctx, tx, taskRangePKIDs)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	// Make result slice
	res := make([]types.TaskInfo, 0, len(tasks))
	for _, task := range tasks {

		outTask, err := task.ToTaskInfo()
		if err != nil {
			return nil, errors.WithStack(err)
		}

		// TimeRanges
		tr, ok := tasksRanges[task.TaskPKID]
		if ok {
			outTask.TimeRangesToAnalyze = tr
		}

		// Columns for the TimeRange
		for i := range tr {
			tr[i].ColumnsToAnalyze = tasksTimeRangeColumns[tr[i].RangePKID]
		}

		res = append(res, *outTask)
	}

	return res, nil
}

func getTaskInfoTransactContext(ctx context.Context, tx *sqlx.Tx,
	task_uuid string, sessionID string, full_view bool) (ti *types.TaskInfo, err error) {

	// Get data
	row := tx.QueryRowxContext(ctx,
		`SELECT
		tasks.task_pk_id AS task_pk_id,
		task_create_timestamp,
		task_processed,
		task_processing_info,
		task_processing_error,
		task_processing_spent_time,
		task_processing_start_timestamp,
		task_processing_end_timestamp,
		file_to_process_pk_id,
		task_uuid,
		task_name
		FROM tasks
		JOIN file_uploads ON file_uploads.file_pk_id = tasks.file_to_process_pk_id
		JOIN sessions ON sessions.session_pk_id = file_uploads.session_pk_id
		WHERE task_uuid = ? AND session_id = ?`, task_uuid, sessionID)
	var intT types.DBintTaskInfo
	if err := row.StructScan(&intT); err != nil {
		return nil, errors.WithMessage(err,
			"getTaskInfoTransactContext: Error getting Task data")
	}

	if full_view {
		res, err := fillTasksWithRangesAndColumnsTransactContext(ctx, tx,
			[]interface{}{intT.TaskPKID}, []types.DBintTaskInfo{intT})
		if err != nil {
			return nil, errors.WithStack(err)
		}

		if len(res) == 0 {
			return nil, errors.WithStack(ErrEmptySliceAsResult)
		}

		ti = &res[0]

	} else {
		ti, err = intT.ToTaskInfo()
		if err != nil {
			return nil, errors.WithStack(err)
		}
	}
	return ti, nil
}
