package db_saver

import (
	"context"
	"database/sql"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/demonihin/goperfstat/web_server/types"
)

// OpenSessionContext - Creates a new session in DB
func (sv *DBSaver) OpenSessionContext(ctx context.Context,
	sessionID string) error {

	curtime := time.Now()
	_, err := sv.db.ExecContext(ctx,
		`INSERT INTO sessions (session_id, start_timestamp, last_action_timestamp)
		 VALUES (?, ?, ?);`,
		sessionID, curtime, curtime)
	if err != nil {
		return errors.Wrap(err,
			"OpenSessionContext: Error opening session")
	}

	return nil
}

// CloseSessionContext - Closes an existing session
func (sv *DBSaver) CloseSessionContext(ctx context.Context,
	sessionID string) (err error) {

	// Transaction
	tx, err := sv.db.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		return errors.Wrap(err,
			"CloseSessionContext: Error opening transaction")
	}
	defer func() {
		if err == nil {
			err = errors.Wrap(tx.Commit(),
				"CloseSessionContext: Error commiting trnsaction")
		} else {
			if ierr := tx.Rollback(); ierr != nil {
				err = errors.Wrap(ierr,
					"CloseSessionContext: Error rolling back transaction")
			}
		}
	}()

	sqlRes, err := tx.ExecContext(ctx,
		"UPDATE sessions SET end_timestamp = ? WHERE session_id = ?;",
		time.Now(), sessionID)
	if err != nil {
		return errors.Wrap(err,
			"CloseSessionContext: Error updating session with end_timestamp")
	}

	rc, err := sqlRes.RowsAffected()
	if err != nil {
		return errors.Wrap(err,
			"CloseSessionContext: Error getting SQL result")
	}

	if rc != 1 {
		err = types.ErrNotOneRowUpdated
		return errors.WithStack(err)
	}

	return nil
}

// GetSessionPKIDContext - Returns session's PKID by its session_id
func (sv *DBSaver) GetSessionPKIDContext(ctx context.Context,
	sessionID string) (sessionPKID int64, err error) {

	// Transaction
	tx, err := sv.db.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		return 0, errors.Wrap(err,
			"GetSessionPKIDContext: Error opening transaction")
	}
	defer func() {
		if err == nil {
			err = tx.Commit()
		} else {
			if ierr := tx.Rollback(); ierr != nil {
				err = errors.Wrap(ierr,
					"GetSessionPKIDContext: Error rolling back transaction")
			}
		}
	}()

	sessionPKID, err = getSessionPKIDTransactContext(ctx, tx, sessionID)
	return sessionPKID, errors.WithStack(err)
}

// UpdateSessionLastActionTimestampContext - Updates last action timestamp in DB
func (sv *DBSaver) UpdateSessionLastActionTimestampContext(ctx context.Context,
	sessionID string) (err error) {

	// Transaction
	tx, err := sv.db.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		return err
	}
	defer func() {
		if err == nil {
			err = errors.Wrap(tx.Commit(),
				"UpdateSessionLastActionTimestampContext: Error commiting transaction")
		} else {
			if ierr := tx.Rollback(); ierr != nil {
				err = errors.Wrap(ierr,
					"UpdateSessionLastActionTimestampContext: Error rolling back transaction")
			}
		}
	}()

	sqlRes, err := tx.ExecContext(ctx,
		`UPDATE sessions SET last_action_timestamp = ? 
		WHERE end_timestamp IS NULL AND session_id = ?;`,
		time.Now(), sessionID)
	if err != nil {
		return errors.Wrapf(err,
			"UpdateSessionLastActionTimestampContext: Error updating session %s with end timestamp",
			sessionID)
	}

	rc, err := sqlRes.RowsAffected()
	if err != nil {
		return errors.Wrap(err,
			"UpdateSessionLastActionTimestampContext: Error getting SQL query result")
	}

	if rc != 1 {
		err = types.ErrNotOneRowUpdated
		return errors.Wrap(types.ErrNotOneRowUpdated,
			"UpdateSessionLastActionTimestampContext: Error: updated rows count is not equal to 1")
	}

	return nil
}
