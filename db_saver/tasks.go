package db_saver

import (
	"context"
	"encoding/json"
	"strings"
	"time"

	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/demonihin/goperfstat/web_server/types"
)

// SaveTaskComputaionResultContext - Saves Statistics computation result
func (sv *DBSaver) SaveTaskComputaionResultContext(ctx context.Context,
	ti *types.TaskInfo) (err error) {

	// Serialize processing errors
	procErrs, err := json.Marshal(ti.TaskProcessingErrors)
	if err != nil {
		return errors.Wrap(err,
			"SaveTaskComputaionResultContext: Error marshalling Task Processing Errors")
	}

	tx, err := sv.db.BeginTx(ctx, nil)
	if err != nil {
		return errors.Wrap(err,
			"SaveTaskComputaionResultContext: Error opening transaction")
	}
	defer func() {
		if err == nil {
			err = errors.Wrap(tx.Commit(),
				"SaveTaskComputaionResultContext: Error committing transaction")
		} else {
			if ierr := tx.Rollback(); ierr != nil {
				err = errors.Wrap(ierr,
					"SaveTaskComputaionResultContext: Error rolling back transaction")
			}
		}
	}()

	sqlRes, err := tx.ExecContext(ctx, `
		UPDATE tasks
		SET
		task_processed = true,
		task_processing_info = ?,
		task_processing_error = ?,
		task_processing_spent_time = ?,
		task_processing_start_timestamp = ?,
		task_processing_end_timestamp = ?
		WHERE task_pk_id = ?;`,
		ti.TaskProcessingInfo,
		string(procErrs),
		ti.TaskProcessingSpentTime,
		ti.TaskProcessingStartTime,
		ti.TaskProcessingEndTime,
		ti.TaskPKID)
	if err != nil {
		return errors.Wrap(err,
			"SaveTaskComputaionResultContext: Error saving Task Computation Result")
	}

	rc, err := sqlRes.RowsAffected()
	if err != nil {
		return errors.Wrap(err,
			"SaveTaskComputaionResultContext: Error getting affected rows count")
	}

	if rc != 1 {
		return errors.Wrap(types.ErrNotOneRowUpdated,
			"SaveTaskComputaionResultContext: Updated rows count is not equal to 1")
	}

	// Save info about processed columns
	for tri := range ti.TimeRangesToAnalyze {

		for tci := range ti.TimeRangesToAnalyze[tri].ColumnsToAnalyze {

			tcp := &ti.TimeRangesToAnalyze[tri].ColumnsToAnalyze[tci]

			serErr, err := json.Marshal(tcp.ComputationError)
			if err != nil {
				return errors.Wrapf(err,
					"SaveTaskComputaionResultContext: Error marshalling Column's errors. %v",
					tcp.ComputationError)
			}

			serProcRes, err := json.Marshal(tcp.Result)
			if err != nil {
				return errors.Wrapf(err,
					"SaveTaskComputaionResultContext: Error marshalling Column's statistics result. %v",
					tcp.Result)
			}

			sqlRes, err := tx.ExecContext(ctx, `
			UPDATE columns_to_analyze
			SET
			processed_values_count = ?,
			processing_start_timestamp = ?,
			processing_end_timestamp = ?,
			processing_spent_time = ?,
			processing_error = ?,
			processing_result = ?
			WHERE column_pk_id = ?;`,
				tcp.ProcessedValuesCount,
				tcp.ComputationStartTime,
				tcp.ComputationFinishTime,
				tcp.ComputationDuration,
				string(serErr),
				string(serProcRes),
				tcp.ColumnPKID)
			if err != nil {
				return errors.Wrap(err,
					"SaveTaskComputaionResultContext: Error updating columns_to_analyze")
			}

			colC, err := sqlRes.RowsAffected()
			if err != nil {
				return errors.Wrap(err,
					"SaveTaskComputaionResultContext: Error getting updated rows count")
			}

			if colC != 1 {
				return errors.Wrap(types.ErrNotOneRowUpdated,
					"SaveTaskComputaionResultContext: Updated columns_to_analyze_rows_count is not 1")
			}

		}
	}

	return nil
}

// GetTaskInfoForTaskUUIDContext - Returns TaskInfo for requested Task
func (sv *DBSaver) GetTaskInfoForTaskUUIDContext(ctx context.Context,
	task_uuid string, sessionID string) (ti *types.TaskInfo, err error) {

	tx, err := sv.db.BeginTxx(ctx, nil)
	if err != nil {
		return nil, errors.WithMessage(err,
			"GetTaskInfoForTaskUUIDContext: Error opening transaction")
	}
	defer func() {
		if ierr := tx.Rollback(); ierr != nil {
			err = errors.WithMessage(
				ierr,
				"GetTaskInfoForTaskUUIDContext: Error rolling back transaction")
		}
	}()

	ti, err = getTaskInfoTransactContext(ctx, tx, task_uuid, sessionID, false)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return ti, nil
}

// GetFullTaskInfoContext - Returns TaskInfo populated with subordinate values
func (sv *DBSaver) GetFullTaskInfoContext(ctx context.Context, task_uuid string,
	sessionID string) (ti *types.TaskInfo, err error) {

	tx, err := sv.db.BeginTxx(ctx, nil)
	if err != nil {
		return nil, errors.WithMessage(err,
			"GetTaskInfoForTaskUUIDContext: Error opening transaction")
	}
	defer func() {
		if ierr := tx.Rollback(); ierr != nil {
			err = errors.WithMessage(
				ierr,
				"GetTaskInfoForTaskUUIDContext: Error rolling back transaction")
		}
	}()

	ti, err = getTaskInfoTransactContext(ctx, tx, task_uuid, sessionID, true)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return ti, nil
}

// CreateTaskForFileContext - Creates a new Task for given file
// ToDo - exclude fileInfo agrument ?
func (sv *DBSaver) CreateTaskForFileContext(ctx context.Context, task *types.TaskInfo,
	fileInfo *types.FileInfo) (err error) {

	transact, err := sv.db.BeginTx(ctx, nil)
	if err != nil {
		return errors.Wrap(err,
			"CreateTaskForFileContext: Error opening transaction")
	}
	defer func() {
		if err == nil {
			err = errors.Wrap(transact.Commit(),
				"CreateTaskForFileContext: Error committing transaction")
		} else {
			if ierr := transact.Rollback(); ierr != nil {
				err = errors.Wrap(ierr,
					"CreateTaskForFileContext: Error rolling back transaction")
			}
		}
	}()

	// Create a new Task
	sqlRes, err := transact.ExecContext(ctx, `
		INSERT INTO tasks
		(task_create_timestamp,
		task_processed,
		file_to_process_pk_id,
		task_uuid,
		task_name)
		VALUES(?, ?, ?, ?, ?);`, time.Now(), false, fileInfo.FilePKID,
		uuid.NewV4().String(), task.TaskName)
	if err != nil {
		return errors.Wrap(err,
			"CreateTaskForFileContext: Error inserting to tasks")
	}

	// TaskPKID
	newTaskPKID, err := sqlRes.LastInsertId()
	if err != nil {
		return errors.Wrap(err,
			"CreateTaskForFileContext: Error getting LastInsertId")
	}

	// Populate Task Time Ranges
	for tTimeRangeIND, _ := range task.TimeRangesToAnalyze {

		tTimeRange := &task.TimeRangesToAnalyze[tTimeRangeIND]

		sqlRes, err = transact.ExecContext(ctx, `
			INSERT INTO task_time_ranges
			(task_pk_id,
			range_start,
			range_end)
			VALUES (?, ?, ?);`, newTaskPKID, tTimeRange.RangeStartTime,
			tTimeRange.RangeEndTime)
		if err != nil {

			return errors.Wrap(err,
				"CreateTaskForFileContext: Error inserting into task_time_ranges")
		}

		var timeRangeID int64
		timeRangeID, err = sqlRes.LastInsertId()
		if err != nil {
			return errors.Wrap(err,
				"CreateTaskForFileContext: Error getting inserted task_time_range ID")
		}

		// Do not insert ColumnsToAnalyze if empty input
		// Raise error
		if len(tTimeRange.ColumnsToAnalyze) == 0 {
			return errors.Wrapf(types.ErrZeroLengthInputSlice,
				"CreateTaskForFileContext: ColumnsToAnalyze count for TaskTimeRange %v is empty",
				tTimeRange)
		}

		// Insert ColumnsToAnalyze
		query :=
			`INSERT INTO columns_to_analyze
			(task_time_range_pk_id, file_column_name,
				min_threshold_value, max_threshold_value)
			VALUES (?, ?, ?, ?)` + strings.Repeat(
				", (?, ?, ?, ?)", len(tTimeRange.ColumnsToAnalyze)-1)

		// Make args
		args := make([]interface{}, 0, 2*len(tTimeRange.ColumnsToAnalyze))
		for _, col := range tTimeRange.ColumnsToAnalyze {
			args = append(args, timeRangeID, col.FileColumnName,
				col.MinValueThreshold, col.MaxValueThreshold)
		}

		sqlRes, err = transact.ExecContext(ctx, query, args...)
		if err != nil {
			return errors.Wrap(err,
				"CreateTaskForFileContext: Error getting SQL result")
		}

		var ra int64
		ra, err = sqlRes.RowsAffected()
		if err != nil {
			return errors.Wrap(err,
				"CreateTaskForFileContext: Error getting inserted columns_to_analyze count")
		}

		if ra != int64(len(tTimeRange.ColumnsToAnalyze)) {
			return errors.Wrap(types.ErrIncorrectInsertedRowsCount,
				"CreateTaskForFileContext: ")
		}
	}

	return nil
}

// GetTasksForStatsComputationContext - Returns full []types.TaskInfo to compute statistics
func (sv *DBSaver) GetTasksForStatsComputationContext(
	ctx context.Context, maxTasks int) (outTasks []types.TaskInfo, err error) {

	// Transaction
	tx, err := sv.db.BeginTxx(ctx, nil)
	if err != nil {
		return nil, errors.Wrap(err,
			"GetTasksForStatsComputationContext: Error opening transaction")
	}
	defer func() {
		if ierr := tx.Rollback(); ierr != nil {
			err = errors.Wrap(ierr,
				"GetTasksForStatsComputationContext: Error rolling back transaction")
		}
	}()

	// Get Tasks
	rows, err := tx.QueryxContext(ctx, `
		SELECT
		tasks.task_pk_id AS task_pk_id,
		task_create_timestamp,
		task_processed,
		task_processing_info,
		task_processing_error,
		task_processing_spent_time,
		task_processing_start_timestamp,
		task_processing_end_timestamp,
		file_to_process_pk_id,
		task_uuid,
		task_name
		FROM tasks
		WHERE task_processed = false
		ORDER BY task_create_timestamp DESC
		LIMIT ?;`, maxTasks)
	if err != nil {
		return nil, errors.Wrap(err,
			"GetTasksForStatsComputationContext: Error getting data from tasks")
	}
	defer func() {
		if ierr := rows.Close(); ierr != nil {
			err = errors.Wrap(ierr,
				"GetTasksForStatsComputationContext: Error closing rows cursor")
		}
	}()

	taskIDS := make([]interface{}, 0)
	tasks := make([]types.DBintTaskInfo, 0)
	for rows.Next() {

		var tempIntTaskInfo types.DBintTaskInfo

		if err := rows.StructScan(&tempIntTaskInfo); err != nil {
			return nil, errors.Wrap(err,
				"GetTasksForStatsComputationContext: Error performing StructScan to intTaskInfo")
		}

		tasks = append(tasks, tempIntTaskInfo)
		taskIDS = append(taskIDS, tempIntTaskInfo.TaskPKID)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}

	// Check case when Tasks list is empty
	if len(tasks) == 0 {
		return make([]types.TaskInfo, 0), nil
	}

	// Fill Tasks with subordinate data
	outTasks, err = fillTasksWithRangesAndColumnsTransactContext(ctx,
		tx, taskIDS, tasks)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	// Fill FileUploadName
	tasksFiles, err := getFileUploadNamesForTasksTransactContext(ctx, tx, taskIDS)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	for i := range outTasks {
		// Set Upload File Name to populate the field in output
		tPKID := outTasks[i].TaskPKID
		fUpName, ok := tasksFiles[tPKID]
		if !ok {
			return nil, errors.Wrapf(types.ErrIncorrectDatabaseStructure,
				"GetTasksForStatsComputationContext: Error no record for task %d in taskFiles map",
				tPKID)
		}
		outTasks[i].FileUploadName = fUpName
	}

	return outTasks, nil
}

// UpdateTaskNameContext - Updates Task name
func (sv *DBSaver) UpdateTaskNameContext(ctx context.Context, taskPKID uint64,
	newTaskName string) (err error) {

	tx, err := sv.db.BeginTx(ctx, nil)
	if err != nil {
		return errors.WithMessage(err,
			"UpdateTaskNameContext: Error opening transaction")
	}
	defer func() {
		if err == nil {
			if ierr := tx.Commit(); ierr != nil {
				err = errors.WithMessagef(ierr,
					"UpdateTaskNameContext: Error commiting transaction")
			}
		} else {
			if ierr := tx.Rollback(); ierr != nil {
				err = errors.WithMessage(ierr,
					"UpdateTaskNameContext: Error rolling back transaction")
			}
		}
	}()

	// Perform update
	sqlRes, err := tx.ExecContext(ctx,
		`UPDATE tasks
		SET task_name = ?
		WHERE task_pk_id = ?;`, newTaskName, taskPKID)
	if err != nil {
		return errors.WithMessage(err,
			"UpdateTaskNameContext: Error updating file_orig_name")
	}

	rowsCount, err := sqlRes.RowsAffected()
	if err != nil {
		return errors.WithMessage(err,
			"UpdateTaskNameContext: Error getting affected rows count")
	}

	if rowsCount != 1 {
		return errors.WithStack(types.ErrNotOneRowUpdated)
	}

	return nil
}
