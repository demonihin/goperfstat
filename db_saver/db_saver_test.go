package db_saver

import (
	"testing"

	"os"

	"strings"

	"fmt"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
	"github.com/pkg/errors"
)

var (
	errTestBeginTransaction    = errors.New("Begin transaction Failed")
	errTestCommitTransaction   = errors.New("Commit failed")
	errTestRollbackTransaction = errors.New("Rollback failed")
	errTestCreateTable         = errors.New("CREATE table error")

	requiredTablesStructuresTests = map[string][]interface{}{
		"sessions": {
			"session_pk_id", "session_id", "start_timestamp",
			"end_timestmap", "last_action_timestamp"},

		"file_uploads": {
			"file_pk_id", "file_orig_name", "file_upload_name",
			"file_has_been_analyzed", "file_analyzing_result",
			"file_analyzing_error", "file_timestamp_column_name",
			"file_timestamp_column_format", "file_upload_timestamp",
			"file_size", "session_pk_id"},

		"tasks": {
			"task_pk_id", "task_create_timestamp", "task_processed",
			"task_processing_info", "task_processing_error",
			"task_processing_spent_time", "task_processing_start_timestamp",
			"task_processing_end_timestamp", "file_to_process_pk_id",
			"task_uuid", "task_name",
		},

		"task_time_ranges": {
			"range_pk_id", "task_pk_id", "range_start", "range_end",
		},

		"columns_to_analyze": {
			"column_pk_id", "processed_values_count", "processing_start_timestamp",
			"processing_end_timestamp", "processing_spent_time", "processing_error",
			"processing_result", "min_threshold_value", "max_threshold_value",
			"task_time_range_pk_id", "file_column_name",
		},
	}

	requiredTablesCreateTests = []string{
		"sessions", "file_uploads", "tasks", "task_time_ranges", "columns_to_analyze",
	}
)

func TestMain(t *testing.M) {

	os.Exit(t.Run())
}

func TestInitDBTables(t *testing.T) {

	sqliteTestName := "test SQLite3 tables"
	if success := t.Run(sqliteTestName, testInitDBTablesSQLite); !success {
		t.Errorf("Failed subtest %s", sqliteTestName)
	}

	mockTransactionBeginFailTestName := "test mock transaction begin fail"
	if success := t.Run(mockTransactionBeginFailTestName,
		testInitDBTablesMockErrorTransactionOpen); !success {
		t.Errorf("Failed subtest %s", mockTransactionBeginFailTestName)
	}

	mockTestInitDBTablesTablesCreateError := "test mock tables create fail"
	if success := t.Run(mockTestInitDBTablesTablesCreateError,
		testInitDBTablesTablesCreateError); !success {
		t.Errorf("Failed subtest %s", mockTransactionBeginFailTestName)
	}

	mockTestTransactionRollbackError := "test mock transaction rollback error"
	if success := t.Run(mockTestTransactionRollbackError,
		testInitDBTablesMockErrorTransactionRollback); !success {
		t.Errorf("Failed subtest %s", mockTestTransactionRollbackError)
	}

	mockTestTransactionCommitError := "test mock transaction commit error"
	if success := t.Run(mockTestTransactionCommitError,
		testInitDBTablesErrorTransactionCommin); !success {
		t.Errorf("Failed subtest %s", mockTestTransactionCommitError)
	}

}

func testInitDBTablesTablesCreateError(t *testing.T) {

	// Test table creating Errors
	for i, tableName := range requiredTablesCreateTests {
		dbx, mock := testCreateMockXDBHelper(t)

		mock.ExpectBegin()

		// Success table creations
		// Start j from 1 because we test first pass over
		// table as fail
		for j := 0; j < i; j++ {
			mock.ExpectExec(
				fmt.Sprintf(`CREATE TABLE IF NOT EXISTS\s+"%s"`,
					requiredTablesCreateTests[j])).WillReturnResult(
				sqlmock.NewResult(0, 0))
		}

		// Last is failure
		mock.ExpectExec(fmt.Sprintf(`CREATE TABLE IF NOT EXISTS "%s"`,
			tableName)).WillReturnError(errors.Wrapf(errTestCreateTable,
			"%s", tableName))

		// Transaction must be rolled back
		mock.ExpectRollback()

		//Execute
		_, err := InitDBTables(dbx)
		if cs := errors.Cause(err); cs != errTestCreateTable {
			t.Errorf("Unexpected error cause actual = %v, expected %v",
				cs, errTestCreateTable)
		}

		if expMet := mock.ExpectationsWereMet(); expMet != nil {
			t.Errorf("SQL mock expectations were not met actual = %v, expected = %v",
				err, nil)
		}
	}
}

func testInitDBTablesSQLite(t *testing.T) {
	// Create DB Connection
	dbConTest, err := sqlx.Connect("sqlite3", ":memory:")
	if err != nil {
		t.Fatalf("Error creating in-memory database actual = %v, expected = %v",
			err, nil)
	}
	defer dbConTest.Close()

	// Init DB in memory
	svc, err := InitDBTables(dbConTest)
	if err != nil {
		t.Errorf("Error creating tables actual = %v, expected = %v", err, nil)
	}

	// Test that SELECTs work correctly
	for tableName, columns := range requiredTablesStructuresTests {
		// Select
		query := ("SELECT ?" + strings.Repeat(", ?", len(columns)-1) +
			" FROM " + tableName + ";")
		_, err := svc.db.Exec(query, columns...)

		if err != nil {
			t.Errorf(
				"Error making query %s from table %s with arguments %v actual =  %v, expected = %v",
				query, tableName, columns, err, nil)
		}

	}
}

func testInitDBTablesMockErrorTransactionOpen(t *testing.T) {
	// Test db errors with mock db
	dbx, mock := testCreateMockXDBHelper(t)

	// Test transaction open error
	mock.ExpectBegin().WillReturnError(errTestBeginTransaction)
	_, err := InitDBTables(dbx)
	if cs := errors.Cause(err); cs != errTestBeginTransaction {
		t.Errorf(
			"Unexpected error cause beginning transaction actual = %v, expected = %v",
			cs, errTestBeginTransaction)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Errorf("SQL mock expectations were not met actual = %v, expected = %v",
			err, nil)
	}
}

func testInitDBTablesMockErrorTransactionRollback(t *testing.T) {

	dbx, mock := testCreateMockXDBHelper(t)

	mock.ExpectBegin()
	mock.ExpectExec("CREATE TABLE IF NOT EXISTS.+").WillReturnError(
		errTestCreateTable)
	mock.ExpectRollback().WillReturnError(errTestRollbackTransaction)

	_, err := InitDBTables(dbx)
	if cs := errors.Cause(err); cs != errTestRollbackTransaction {

		t.Errorf("Unexpected error cause on transaction rollback actual = %v, expected = %v",
			cs, errTestRollbackTransaction)
	}

}

func testInitDBTablesErrorTransactionCommin(t *testing.T) {
	dbx, mock := testCreateMockXDBHelper(t)

	mock.ExpectBegin()
	// 5 tables
	mock.ExpectExec("CREATE TABLE IF NOT EXISTS.+").WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec("CREATE TABLE IF NOT EXISTS.+").WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec("CREATE TABLE IF NOT EXISTS.+").WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec("CREATE TABLE IF NOT EXISTS.+").WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec("CREATE TABLE IF NOT EXISTS.+").WillReturnResult(sqlmock.NewResult(0, 0))

	mock.ExpectCommit().WillReturnError(errTestCommitTransaction)

	_, err := InitDBTables(dbx)
	if cs := errors.Cause(err); cs != errTestCommitTransaction {

		t.Errorf("Unexpected error cause on transaction commit actual = %v, expected = %v",
			cs, errTestCommitTransaction)
	}
}
