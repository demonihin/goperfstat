package main

import (
	"fmt"

	"io"

	"github.com/gin-gonic/gin"
)

func sendFile(c *gin.Context, fileName string, fileLen int64, file io.Reader) {
	c.DataFromReader(200, fileLen,
		"application/octet-stream", file,
		map[string]string{
			"Content-Disposition": fmt.Sprintf(
				"attachment; filename=\"Triggers_Template_for_%s.csv\"",
				fileName),
			"Content-Description":       "File Transfer",
			"Content-Transfer-Encoding": "binary"})
}
