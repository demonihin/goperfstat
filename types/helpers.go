package types

import (
	"github.com/Mottl/ctimefmt"
)

// FindPossibleTimestampColumnFormats - scans given values for possible TimestampColumnInfos.
func FindPossibleTimestampColumnFormats(columnNames []string, values []string) ([]TimestampColumnInfo, error) {
	var tsColInfoPossibleVariants = make(map[TimestampColumnInfo]bool, 1)

	// Lookup over values
	for colInd, col := range values {

		// Try to parse column as Timestamp
		for _, dFV := range PossibleDateFormats {
			for _, tFV := range PossibleTimeFormats {

				formatToCheck := dFV + " " + tFV
				tsMapKey := TimestampColumnInfo{
					TimestampColumnName:   columnNames[colInd],
					TimestampColumnFormat: formatToCheck,
				}

				// Check if not set as not possible
				allowed, alreadySet := tsColInfoPossibleVariants[tsMapKey]
				if alreadySet {
					if !allowed {
						continue
					}

					// Check if existing format is valid for current timestamp
					// Parse
					_, err := ctimefmt.Parse(formatToCheck, col)
					if err != nil {
						// On first fail set Timestamp format as forbidden to use
						tsColInfoPossibleVariants[tsMapKey] = false
					}

				} else {

					// Parse
					_, err := ctimefmt.Parse(formatToCheck, col)
					if err != nil {
						// On first fail set Timestamp format as forbidden to use
						tsColInfoPossibleVariants[tsMapKey] = false
					} else {
						// Set as successful
						tsColInfoPossibleVariants[tsMapKey] = true
					}
				}
			}
		}
	}

	// Make result
	res := make([]TimestampColumnInfo, 0, len(tsColInfoPossibleVariants))
	for k, v := range tsColInfoPossibleVariants {
		if v {
			res = append(res, k)
		}
	}

	return res, nil
}
