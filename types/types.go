package types

import (
	"database/sql"
	"encoding/json"

	"time"

	"github.com/Mottl/ctimefmt"
	"github.com/pkg/errors"
)

const (
	MaxRowsReadToFindTimestampColumn     = 10
	FileAnalyzeLoopWatchIntervalSec      = 1
	FileStatsComputeLoopWatchIntervalSec = 1
	FileAnalyzeBatchSize                 = 50
	FileStatsComputeBatchSize            = 50
	FileUploadDirectory                  = "upload/"
	// FileBackwardsReadingBlockSizeBytes   = 8192

	TaskPatchTaskNameKey = "task_name"
)

var (
	ErrCopyInputValues                = errors.New("Can not copy input values")
	ErrUnsortedInput                  = errors.New("Input is not sorted")
	ErrIncorrectThresholdValue        = errors.New("Threshold value is incorrect")
	ErrNotOneRowUpdated               = errors.New("Updated rows count is not equal to 1")
	ErrIncorrectInsertedRowsCount     = errors.New("Inserted rows count is not equal to expected")
	ErrZeroLengthInputSlice           = errors.New("Empty slice given as argument")
	ErrCanNotAutoFindTimestapFormat   = errors.New("Can not find possible Timestamp format and Timestamp column name")
	ErrIncorrectDatabaseStructure     = errors.New("Incorrect database's relation structure")
	ErrColumnNotFoundInFile           = errors.New("Can not find column by its header in file headers")
	ErrZeroLengthInputMap             = errors.New("Empty map given as argument")
	ErrCanNotFindTaskTimeRange        = errors.New("Can not find TaskTimeRange")
	ErrCanNotFindTimestampColumnIndex = errors.New("Can not find Timestamp column index in CSV header")
	ErrInvalidPatchOperationKey       = errors.New("Incorrect key for patch operation")
	ErrIncorrectCodepageName          = errors.New("given codepage does not exist in possible codepages list")
	ErrFilenameNotGiven               = errors.New("No filename exists in request")
	ErrNoTaskUUIDGiven                = errors.New("No task uuid given for update")
	ErrSavingFile                     = errors.New("File saved size is not equal to uploaded file size")

	PossibleDateFormats = map[string]string{
		"PossibleDateFormatdMyyyy":    `%d/%m/%Y`,
		"PossibleDateFormatMdyyyy":    "%m/%d/%Y",
		"PossibleDateFormatMdyy":      "%m/%d/%y",
		"PossibleDateFormatyyMd":      "%y/%m/%d",
		"PossibleDateFormatyyyy_M_d":  "%Y/%m/%d",
		"PossibleDateFormatdd_MMM_yy": "%d/%b/%y",
	}

	PossibleTimeFormats = map[string]string{
		"PossibleTimeFormathh__mm__ss___fff":       "%H:%M:%S.%L",
		"PossibleTimeFormathh__mm__ss___fff_12hsm": "%l:%M:%S.%L %P",
		"PossibleTimeFormathh__mm__ss___fff_12hLG": "%l:%M:%S.%L %p",
	}

	utcTimeLocation, _   = time.LoadLocation("UTC")
	MaxTimetampInReport  = time.Date(3000, 0, 0, 0, 0, 0, 0, utcTimeLocation)
	MinTimestampInReport = time.Time{}
)

// FileAnalyzeTask - One task to perform uploaded File analyzing
type FileAnalyzeTask struct {
	FileInformation   *FileInfo
	StartTime         *time.Time
	FinishTime        *time.Time
	AnalyzingDuration *time.Duration
	Errors            []string
	Result            *FileAnalyzeResult
}

// FileAnalyzeResult - Result of analyzing of one file
type FileAnalyzeResult struct {
	PossibleTimestampColumnInfos []TimestampColumnInfo `json:"possible_timestamp_column_infos"`
	TimestampColumn              *TimestampColumnInfo  `json:"timestamp_column_info"`
	ColumnNames                  []string              `json:"column_names"`
	MinFileTimestamp             *time.Time            `json:"min_file_timestamp"`
	MaxFileTimestamp             *time.Time            `json:"max_file_timestamp"`
}

// TimestampColumnInfo - Contains information about a column which contains Timestamp
type TimestampColumnInfo struct {
	TimestampColumnName   string `json:"timestamp_column_name"`
	TimestampColumnFormat string `json:"timestamp_column_format"`
}

// ParseTime - parses given string as time.Time using TimestampColumnInfo format string.
func (tci *TimestampColumnInfo) ParseTime(tm string) (time.Time, error) {
	return ctimefmt.Parse(tci.TimestampColumnFormat, tm)
}

// StatsColumnResult - Result of statistics computation
type StatsColumnResult struct {
	Mean                         float64  `json:"mean"`
	Variance                     float64  `json:"variance"`
	Min                          float64  `json:"min"`
	Max                          float64  `json:"max"`
	MoreThanMaxThresholdFraction *float64 `json:"more_than_max_threshold_fraction"`
	LessThanMinThresholdFraction *float64 `json:"less_than_min_threshold_fraction"`

	Percentiles
}

// Percentiles - Percentiles for values
type Percentiles struct {
	Percentile95 float64 `json:"percentile95"`
	Percentile90 float64 `json:"percentile90"`
	Percentile80 float64 `json:"percentile80"`
}

type NullTime struct {
	time.Time
	Valid bool // Valid is true if Time is not NULL
}

// Scan implements the Scanner interface.
func (nt *NullTime) Scan(value interface{}) error {
	nt.Time, nt.Valid = value.(time.Time)
	return nil
}

// Value implements the driver Valuer interface.
func (nt NullTime) Value() (*time.Time, error) {
	if !nt.Valid {
		return nil, nil
	}
	return &nt.Time, nil
}

type DBintFileInfo struct {
	FilePKID                  uint64         `db:"file_pk_id"`
	FileCreatorSessionPKID    uint64         `db:"session_pk_id"`
	FileSize                  uint64         `db:"file_size"`
	FileOwnerSessionID        string         `db:"-"`
	FileOriginalName          string         `db:"file_orig_name"`
	FileUploadName            string         `db:"file_upload_name"`
	FileHasBeenAnalyzed       bool           `db:"file_has_been_analyzed"`
	FileAnalyzingResult       sql.NullString `db:"file_analyzing_result"`
	FileAnalyzingError        sql.NullString `db:"file_analyzing_error"`
	FileTimestampColumnName   sql.NullString `db:"file_timestamp_column_name"`
	FileTimestampColumnFormat sql.NullString `db:"file_timestamp_column_format"`
	FileUploadTimestamp       time.Time      `db:"file_upload_timestamp"`
	Tasks                     []TaskInfo     `db:"-"`
}

// ToFileInfo - Performs converting intFileInfo to FileInfo
func (ifinfo *DBintFileInfo) ToFileInfo() (*FileInfo, error) {

	tmpRFI := FileInfo{
		FilePKID:               ifinfo.FilePKID,
		FileCreatorSessionPKID: ifinfo.FileCreatorSessionPKID,
		FileSize:               ifinfo.FileSize,
		FileOwnerSessionID:     ifinfo.FileOwnerSessionID,
		FileOriginalName:       ifinfo.FileOriginalName,
		FileUploadName:         ifinfo.FileUploadName,
		FileHasBeenAnalyzed:    ifinfo.FileHasBeenAnalyzed,
		FileUploadTimestamp:    ifinfo.FileUploadTimestamp,
	}

	// File Analyzing Result
	if ifinfo.FileAnalyzingResult.Valid {
		ts := ifinfo.FileAnalyzingResult.String

		if err := json.Unmarshal([]byte(ts),
			&tmpRFI.FileAnalyzingResult); err != nil {

			return nil, errors.Wrap(err,
				"convertIntFileInfoToFileInfo: Error loading JSON FileAnalyzingResult")
		}
	}

	// File Analyzing Errors
	if ifinfo.FileAnalyzingError.Valid {
		ts := ifinfo.FileAnalyzingError.String
		var tmpE []string
		if err := json.Unmarshal([]byte(ts), &tmpE); err != nil {
			return nil, errors.Wrap(err,
				"convertIntFileInfoToFileInfo: Error loading JSON FileAnalyzingError")
		}
		tmpRFI.FileAnalyzingErrors = tmpE
	}

	// File Timestamp Column Name
	if ifinfo.FileTimestampColumnName.Valid {
		ts := ifinfo.FileTimestampColumnName.String
		tmpRFI.FileTimestampColumnName = &ts
	}

	// File Timestamp Column Format
	if ifinfo.FileTimestampColumnFormat.Valid {
		ts := ifinfo.FileTimestampColumnFormat.String
		tmpRFI.FileTimestampColumnFormat = &ts
	}

	return &tmpRFI, nil
}

const (
	FileOriginalNameKey = "file_original_name"
)

type FileInfo struct {
	FilePKID                  uint64            `json:"-"`
	FileCreatorSessionPKID    uint64            `json:"-"`
	FileSize                  uint64            `json:"file_size"`
	FileOwnerSessionID        string            `json:"-"`
	FileOriginalName          string            `json:"file_original_name"`
	FileUploadName            string            `json:"file_upload_name"`
	FileHasBeenAnalyzed       bool              `json:"file_analyzing_completed"`
	FileAnalyzingResult       FileAnalyzeResult `json:"file_analyzing_result"`
	FileAnalyzingErrors       []string          `json:"file_analyzing_errors"`
	FileTimestampColumnName   *string           `json:"file_timestamp_column_name"`
	FileTimestampColumnFormat *string           `json:"file_timestamp_column_format"`
	FileUploadTimestamp       time.Time         `json:"file_upload_timestamp"`
	Tasks                     []TaskInfo        `json:"tasks"`
}

type TaskInfo struct {
	TaskPKID                uint64          `json:"-"`
	FilePKID                uint64          `json:"-"`
	TaskProcessed           bool            `json:"task_processed"`
	FileUploadName          string          `json:"file_upload_name"`
	TaskUUID                string          `json:"task_unique_id"`
	TaskCreateTimestamp     time.Time       `json:"task_create_timestamp"`
	TaskProcessingInfo      *string         `json:"task_processing_info"`
	TaskName                *string         `json:"task_name"`
	TaskProcessingErrors    []string        `json:"task_processing_errors"`
	TaskProcessingSpentTime *time.Duration  `json:"task_processing_spent_time"`
	TaskProcessingStartTime *time.Time      `json:"task_processing_start_timestamp"`
	TaskProcessingEndTime   *time.Time      `json:"task_processing_end_timestamp"`
	TimeRangesToAnalyze     []TaskTimeRange `json:"time_ranges_to_analyze"`
}

type DBintTaskInfo struct {
	TaskPKID                uint64          `db:"task_pk_id"`
	FilePKID                uint64          `db:"file_to_process_pk_id"`
	TaskProcessed           bool            `db:"task_processed"`
	TaskUUID                string          `db:"task_uuid"`
	TaskName                sql.NullString  `db:"task_name"`
	TaskReportFileName      sql.NullString  `db:"task_report_file_name"`
	TaskProcessingInfo      sql.NullString  `db:"task_processing_info"`
	TaskProcessingError     sql.NullString  `db:"task_processing_error"`
	TaskProcessingSpentTime sql.NullInt64   `db:"task_processing_spent_time"`
	TaskCreateTimestamp     time.Time       `db:"task_create_timestamp"`
	TaskProcessingStartTime NullTime        `db:"task_processing_start_timestamp"`
	TaskProcessingEndTime   NullTime        `db:"task_processing_end_timestamp"`
	TimeRangesToAnalyze     []TaskTimeRange `db:"-"`
}

// convertIntTaskInfoToTaskInfo - Performs converting intTaskInfo to TaskInfo
func (itaskinfo *DBintTaskInfo) ToTaskInfo() (*TaskInfo, error) {

	res := TaskInfo{
		TaskPKID:            itaskinfo.TaskPKID,
		FilePKID:            itaskinfo.FilePKID,
		TaskProcessed:       itaskinfo.TaskProcessed,
		TaskCreateTimestamp: itaskinfo.TaskCreateTimestamp,
		TimeRangesToAnalyze: itaskinfo.TimeRangesToAnalyze,
		TaskUUID:            itaskinfo.TaskUUID,
	}

	if itaskinfo.TaskProcessingSpentTime.Valid {
		dur := time.Duration(itaskinfo.TaskProcessingSpentTime.Int64)
		res.TaskProcessingSpentTime = &dur
	}

	if itaskinfo.TaskProcessingStartTime.Valid {
		tT := itaskinfo.TaskProcessingStartTime.Time
		res.TaskProcessingStartTime = &tT
	}

	if itaskinfo.TaskProcessingEndTime.Valid {
		tT := itaskinfo.TaskProcessingEndTime.Time
		res.TaskProcessingEndTime = &tT
	}

	if itaskinfo.TaskName.Valid {
		tS := itaskinfo.TaskName.String
		res.TaskName = &tS
	}

	if itaskinfo.TaskProcessingInfo.Valid {
		ts := itaskinfo.TaskProcessingInfo.String
		res.TaskProcessingInfo = &ts
	}

	if itaskinfo.TaskProcessingError.Valid {
		var errs []string
		if err := json.Unmarshal([]byte(itaskinfo.TaskProcessingError.String), &errs); err != nil {
			return nil, errors.Wrap(err,
				"convertIntTaskInfoToTaskInfo: Error unmarshalling errors")
		}
		res.TaskProcessingErrors = errs
	}

	return &res, nil
}

type TaskColumnToAnalyze struct {
	ColumnPKID            uint64             `json:"-"`
	TaskTimeRangePKID     uint64             `json:"-"`
	ProcessedValuesCount  uint64             `json:"processed_values_count"`
	FileColumnName        string             `json:"column_to_analyze_name"`
	ComputationStartTime  *time.Time         `json:"processing_start_timestamp"`
	ComputationFinishTime *time.Time         `json:"processing_end_timestamp"`
	ComputationDuration   *time.Duration     `json:"processing_spent_time"`
	ComputationError      []string           `json:"processing_error"`
	MinValueThreshold     *float64           `json:"min_threshold_value"`
	MaxValueThreshold     *float64           `json:"max_threshold_value"`
	Values                []float64          `json:"-"`
	Result                *StatsColumnResult `json:"processing_result"`
}

type DBintTaskColumnToAnalyze struct {
	ColumnPKID            uint64          `db:"column_pk_id"`
	TaskTimeRangePKID     uint64          `db:"task_time_range_pk_id"`
	ProcessedValuesCount  sql.NullInt64   `db:"processed_values_count"`
	FileColumnName        string          `db:"file_column_name"`
	ComputationStartTime  NullTime        `db:"processing_start_timestamp"`
	ComputationFinishTime NullTime        `db:"processing_end_timestamp"`
	ComputationDuration   sql.NullInt64   `db:"processing_spent_time"`
	ComputationError      sql.NullString  `db:"processing_error"`
	ProcessingResult      sql.NullString  `db:"processing_result"`
	MinValueThreshold     sql.NullFloat64 `db:"min_threshold_value"`
	MaxValueThreshold     sql.NullFloat64 `db:"max_threshold_value"`
}

func (itcta *DBintTaskColumnToAnalyze) ToTaskColumnToAnalyze() (*TaskColumnToAnalyze, error) {
	var (
		errs                    []string
		processedValues         uint64
		compStTime, compFinTime *time.Time
		compDur                 *time.Duration
		minVTh, maxVTh          *float64
		procRes                 StatsColumnResult
	)

	// Errors
	if itcta.ComputationError.Valid {
		if err := json.Unmarshal([]byte(itcta.ComputationError.String), &errs); err != nil {
			return nil, errors.Wrap(err,
				"TaskColumnToAnalyze: Error unmarshalling Column's errors field")
		}
	}

	// Processing result
	if itcta.ProcessingResult.Valid {
		if err := json.Unmarshal([]byte(itcta.ProcessingResult.String), &procRes); err != nil {
			return nil, errors.Wrap(err,
				"TaskColumnToAnalyze: Error unmarshalling Column's processing result field")
		}
	}

	// processed values count
	if itcta.ProcessedValuesCount.Valid {
		processedValues = uint64(itcta.ProcessedValuesCount.Int64)
	}

	// Computation Start Timestamp
	if itcta.ComputationStartTime.Valid {
		tST := itcta.ComputationStartTime.Time
		compStTime = &tST
	}

	// Computation Finish Time
	if itcta.ComputationFinishTime.Valid {
		tCFT := itcta.ComputationFinishTime.Time
		compFinTime = &tCFT
	}

	// Computation duration
	if itcta.ComputationDuration.Valid {
		val := time.Duration(itcta.ComputationDuration.Int64)
		compDur = &val
	}

	// Min Threshold
	if itcta.MinValueThreshold.Valid {
		tMVT := itcta.MinValueThreshold.Float64
		minVTh = &tMVT
	}

	// Max Threshold
	if itcta.MaxValueThreshold.Valid {
		tMVT := itcta.MaxValueThreshold.Float64
		maxVTh = &tMVT
	}

	return &TaskColumnToAnalyze{
		ColumnPKID:            itcta.ColumnPKID,
		TaskTimeRangePKID:     itcta.TaskTimeRangePKID,
		ProcessedValuesCount:  processedValues,
		FileColumnName:        itcta.FileColumnName,
		ComputationStartTime:  compStTime,
		ComputationFinishTime: compFinTime,
		ComputationDuration:   compDur,
		ComputationError:      errs,
		MinValueThreshold:     minVTh,
		MaxValueThreshold:     maxVTh,
		Result:                &procRes,
	}, nil
}

type TaskTimeRange struct {
	RangePKID        uint64                `db:"range_pk_id" json:"-"`
	TaskPKID         uint64                `db:"task_pk_id" json:"-"`
	RangeStartTime   time.Time             `db:"range_start" json:"range_start"`
	RangeEndTime     time.Time             `db:"range_end" json:"range_end"`
	ColumnsToAnalyze []TaskColumnToAnalyze `db:"-" json:"columns_to_analyze"`
}

type FilePatchPayload struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

type TaskPatchPayload struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

type ColumnValueThreshold struct {
	ColumnName        string   `json:"column_name"`
	MaxValueThreshold *float64 `json:"max_threshold"`
	MinValueThreshold *float64 `json:"min_threshold"`
}
