module gitlab.com/demonihin/goperfstat/web_server/types

go 1.12

require (
	github.com/Mottl/ctimefmt v0.0.0-20190803144728-fd2ac23a585a
	github.com/pkg/errors v0.8.1
)
