# Go Performance Statistics Analyser

The application is a simple statistics analyser with web interface.
The application makes computing statistics for big Windows CSV Performance monitor
log files easier. It can analyse quite big files (I think about tens gigabytes)
which are difficult to analyse using instruments like LibreOffice Calc or Microsoft Excel.

After building the application you get just a one binary file and a folder with
static frontend files. The application uses SQLite3 database as internal state storage
which adds to its portability.

## Installation

Install Go compiler v1.12 or newer.
Install NodeJS v11.14 or newer.
Run: `make all`.
In directory `dist/` you will get your build.

I am almost sure that the application could be built using Go and NodeJS of earlier
but I have not tested it.

## Running

Run Go application from `dist/server` directory and open http://127.0.0.1:4000/static in your browser.
