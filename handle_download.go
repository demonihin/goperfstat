package main

import (
	"gitlab.com/demonihin/goperfstat/web_server/db_saver"
	"gitlab.com/demonihin/goperfstat/web_server/types"

	"github.com/gin-gonic/gin"
)

func createFileDownloadHandler(svr *db_saver.DBSaver) gin.HandlerFunc {
	return func(c *gin.Context) {

		// Get session
		cook, err := c.Cookie(AuthCookiesName)
		if err != nil {
			AbortNoAuthCookies500(c, err)

			return
		}

		// Get requested filename
		filename := c.Param(FilenameDownloadPathParamName)
		if filename == "" {
			AbortNoFileGiven500(c)

			return
		}

		// Check if the file was uploaded by current user
		fi, err := svr.GetFileInfoForFileContext(c, filename, cook)
		if err != nil {
			Abort404Error(c, err, "Error file not found")

			return
		}

		// Open and send file
		c.FileAttachment(types.FileUploadDirectory+fi.FileUploadName,
			fi.FileOriginalName)
	}
}
