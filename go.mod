module gitlab.com/demonihin/goperfstat/web_server

go 1.12

require (
	github.com/gin-gonic/gin v1.4.0
	github.com/helloeave/json v1.11.1
	github.com/jmoiron/sqlx v1.2.0
	github.com/mattn/go-sqlite3 v1.10.0
	github.com/pkg/errors v0.8.1
	github.com/satori/go.uuid v1.2.0
	gitlab.com/demonihin/goperfstat/web_server/db_saver v0.0.0-00010101000000-000000000000
	gitlab.com/demonihin/goperfstat/web_server/perfmon_stat v0.0.0-00010101000000-000000000000
	gitlab.com/demonihin/goperfstat/web_server/types v0.0.0-00010101000000-000000000000
	golang.org/x/text v0.3.2
	gonum.org/v1/gonum v0.0.0-20190602094245-c4a599f7b712 // indirect
	google.golang.org/appengine v1.6.2 // indirect
)

replace gitlab.com/demonihin/goperfstat/web_server/types => ./types

replace gitlab.com/demonihin/goperfstat/web_server/db_saver => ./db_saver

replace gitlab.com/demonihin/goperfstat/web_server/perfmon_stat => ./perfmon_stat
