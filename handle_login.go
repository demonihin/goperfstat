package main

import (
	"context"
	"crypto/sha256"

	"gitlab.com/demonihin/goperfstat/web_server/db_saver"

	"crypto/rand"
	"math/big"

	"math"

	"github.com/gin-gonic/gin"
)

const (
	AuthCookiesName        = "session_id"
	AuthCookiesLifetimeSec = 604800 // 1 week
	AuthCookieSecureOnly   = false
)

func createLoginHandler(svr *db_saver.DBSaver, cookieDomain string) gin.HandlerFunc {

	return func(c *gin.Context) {

		// Check if cookies exist
		exstingCook, err := c.Cookie(AuthCookiesName)
		if err != nil {

			sessionID, err := generateCookiesOpenNewSession(c, svr)
			if err != nil {
				Abort500Error(c, err, "Error generating session id")

				return
			}

			c.Header("Content-type", "application/json")
			c.SetCookie(AuthCookiesName, sessionID,
				AuthCookiesLifetimeSec, "/", cookieDomain, AuthCookieSecureOnly, false)

			return
		}

		// Check existing cookies
		if err := checkLogin(c, svr, exstingCook); err != nil {
			// Existing cookies are not for active session

			sessionID, err := generateCookiesOpenNewSession(c, svr)
			if err != nil {
				Abort500Error(c, err, "Error generating session id")

				return
			}

			c.Header("Content-type", "application/json")
			c.SetCookie(AuthCookiesName, sessionID,
				AuthCookiesLifetimeSec, "/", cookieDomain, AuthCookieSecureOnly, false)

			return
		}

		// Update existing cookies lifetime
		if err := svr.UpdateSessionLastActionTimestampContext(c,
			exstingCook); err != nil {
			Abort500Error(c, err, "Error updating session timestamp")

			return
		}

		c.Header("Content-type", "application/json")
		c.SetCookie(AuthCookiesName, exstingCook,
			AuthCookiesLifetimeSec, "/", cookieDomain, AuthCookieSecureOnly, false)
	}
}

func checkLogin(ctx context.Context, svr *db_saver.DBSaver, sessionID string) (err error) {

	// Check session
	_, err = svr.GetSessionPKIDContext(ctx, sessionID)
	if err != nil {
		return err
	}

	// Update session status
	if err := svr.UpdateSessionLastActionTimestampContext(ctx, sessionID); err != nil {
		return err
	}

	return nil
}

func generateCookiesOpenNewSession(c *gin.Context, svr *db_saver.DBSaver) (newCookiesValue string, err error) {
	// New cookies
	randata, err := rand.Int(rand.Reader, big.NewInt(math.MaxInt64))
	if err != nil {

		return "", err
	}

	// hash
	hashVal := sha256.Sum256(randata.Bytes())
	sessionID := string(hashVal[:])

	if err = svr.OpenSessionContext(c, sessionID); err != nil {

		return "", err
	}

	return sessionID, nil
}
