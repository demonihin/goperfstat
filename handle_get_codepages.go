package main

import (
	"github.com/gin-gonic/gin"
	"golang.org/x/text/encoding"
	"golang.org/x/text/encoding/charmap"
)

func handleCodepagesHandler(c *gin.Context) {

	result := make([]string, 0, len(charmap.All))

	// Add UTF-8
	result = append(result, UTF8CodepageName)

	for _, chm := range charmap.All {

		switch tchm := chm.(type) {
		case *charmap.Charmap:

			result = append(result, tchm.String())

			break
		case encoding.Encoding:
			break

		default:
			panic("newDecoder: can not typecast encoding to Charmap or Encoding")
		}

	}

	Send200JSONResponse(c, result)
}
