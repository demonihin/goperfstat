package main

import (
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/demonihin/goperfstat/web_server/db_saver"
	"gitlab.com/demonihin/goperfstat/web_server/types"
)

func createDeleteFileHandler(svr *db_saver.DBSaver) gin.HandlerFunc {

	return func(c *gin.Context) {

		// Get session
		cook, err := c.Cookie(AuthCookiesName)
		if err != nil {
			AbortNoAuthCookies500(c, err)

			return
		}

		// Get requested filename
		filename := c.Param(FilenameDownloadPathParamName)
		if filename == "" {
			AbortNoFileGiven500(c)

			return
		}

		// Check if the file was uploaded by current user
		fi, err := svr.GetFileInfoForFileContext(c, filename, cook)
		if err != nil {
			Abort404Error(c, err, "Error file not found")

			return
		}

		// Delete file from uploads directory
		if err := os.Remove(types.FileUploadDirectory + fi.FileUploadName); err != nil {
			Abort500Error(c, err, "Error file was not deleted")

			return
		}

		// Delete file from database
		if err := svr.DeleteFileInfoForFileContext(c, fi.FilePKID); err != nil {
			Abort500Error(c, err, "Error file was not deleted")

			return
		}

		sendResponse(c, 200, gin.H{"status": "ok"})
	}
}
