package main

import (
	"log"
	"os"

	"gitlab.com/demonihin/goperfstat/web_server/perfmon_stat"

	"context"

	"github.com/jmoiron/sqlx"
	"gitlab.com/demonihin/goperfstat/web_server/db_saver"

	_ "github.com/mattn/go-sqlite3"
)

func main() {

	// Get start arguments
	var cookieDomain string
	if len(os.Args) == 2 {
		cookieDomain = os.Args[1]
	} else {
		cookieDomain = "localhost"
	}
	log.Printf("Set cookie domain to %s", cookieDomain)

	// Init database connection
	dbCon, err := sqlx.Connect("sqlite3", "sqlite.sqlite3")
	if err != nil {
		log.Fatalln(err)
	}

	saver, err := db_saver.InitDBTables(dbCon)
	if err != nil {
		log.Fatalln(err)
	}

	// Global context
	gctx, _ := context.WithCancel(context.Background())

	// Global error channel
	errCh := make(chan error, 100)

	// Global Logger
	perfmon_stat.InitErrorsLoggerContext(gctx, errCh)

	// Init files analyzing
	perfmon_stat.InitNewFilesWatcherLoopContext(gctx, saver, errCh, 2)

	// Init stats computation
	perfmon_stat.InitTasksProcessLoopContext(gctx, saver, errCh, 2)

	engine := CreateRouter(saver, cookieDomain)
	log.Fatalln(engine.Run(":4000"))
}
