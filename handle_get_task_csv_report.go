package main

import (
	"fmt"
	"strconv"

	"bytes"

	"encoding/csv"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"gitlab.com/demonihin/goperfstat/web_server/db_saver"
	"gitlab.com/demonihin/goperfstat/web_server/types"
)

func createCreateTaskCSVReportHandler(svr *db_saver.DBSaver) gin.HandlerFunc {
	return func(c *gin.Context) {
		// Get session
		cook, err := c.Cookie(AuthCookiesName)
		if err != nil {
			AbortNoAuthCookies500(c, err)

			return
		}

		// Get requested task UUID
		task_uuid := c.Param(TaskInfoUUIDPathParamName)
		if task_uuid == "" {
			AbortNoTaskUUIDGiven500(c)

			return
		}

		// Check if the task was uploaded by current user
		ti, err := svr.GetFullTaskInfoContext(c, task_uuid, cook)
		if err != nil {
			Abort404Error(c, err, "No task found")

			return
		}

		// Check task status
		if !ti.TaskProcessed {
			Abort400Error(c, ErrTaskHasNotBeenProcessed, fmt.Sprintf(
				"Task name: \"%s\", task UUID \"%s\"",
				*ti.TaskName, ti.TaskUUID))

			return
		}

		// Check task errors
		if len(ti.TaskProcessingErrors) != 0 {
			Abort400Error(c, ErrTaskHasNotProcessingErrors, fmt.Sprintf(
				"Task name: \"%s\", task UUID \"%s\", errors: %v",
				*ti.TaskName, ti.TaskUUID, ti.TaskProcessingErrors))

			return
		}

		// Make CSV report
		fileRes, err := makeCSVReport(ti)
		if err != nil {
			Abort500Error(c, err, "Error generating report")

			return
		}

		sendFile(
			c, fmt.Sprintf("Report_for_%s__%s__%s.csv",
				ti.FileUploadName, *ti.TaskName, ti.TaskUUID),
			int64(fileRes.Len()), fileRes)
	}
}

func makeCSVReport(ti *types.TaskInfo) (report *bytes.Buffer, err error) {
	var outBuf bytes.Buffer

	// CSV
	csvW := csv.NewWriter(&outBuf)

	// Headers
	if err = csvW.Write([]string{
		"Counter name",
		"Time Range Start",
		"Time Range End",
		"Processed values count",
		"Minimum value",
		"Maximum value",
		"Mean",
		"Variance",
		"Percentile 80",
		"Percentile 90",
		"Percentile 95",
		"Max threshold",
		"More than max threshold, %",
		"Min threshold",
		"Less than min threshold, %"}); err != nil {

		return nil, errors.Wrap(err,
			"makeCSVReport: Error writing CSV header")
	}

	// Write values
	for _, tr := range ti.TimeRangesToAnalyze {

		for j := range tr.ColumnsToAnalyze {

			pcol := &tr.ColumnsToAnalyze[j]

			// Thresholds
			var (
				minTH, maxTH, lMinTh, mMaxTh string
			)
			if pcol.MinValueThreshold != nil {
				minTH = strconv.FormatFloat(*pcol.MinValueThreshold, 'G', -1, 64)
				lMinTh = strconv.FormatFloat(*pcol.Result.LessThanMinThresholdFraction, 'G', -1, 64)
			}
			if pcol.MaxValueThreshold != nil {
				maxTH = strconv.FormatFloat(*pcol.MaxValueThreshold, 'G', -1, 64)
				mMaxTh = strconv.FormatFloat(*pcol.Result.MoreThanMaxThresholdFraction, 'G', -1, 64)
			}

			if err = csvW.Write([]string{
				tr.ColumnsToAnalyze[j].FileColumnName,
				tr.RangeStartTime.Format("2006-01-02 15:04:05"),
				tr.RangeEndTime.Format("2006-01-02 15:04:05"),
				strconv.FormatUint(tr.ColumnsToAnalyze[j].ProcessedValuesCount, 10),
				strconv.FormatFloat(tr.ColumnsToAnalyze[j].Result.Min, 'G', -1, 64),
				strconv.FormatFloat(tr.ColumnsToAnalyze[j].Result.Max, 'G', -1, 64),
				strconv.FormatFloat(tr.ColumnsToAnalyze[j].Result.Mean, 'G', -1, 64),
				strconv.FormatFloat(tr.ColumnsToAnalyze[j].Result.Variance, 'G', -1, 64),
				strconv.FormatFloat(tr.ColumnsToAnalyze[j].Result.Percentile80, 'G', -1, 64),
				strconv.FormatFloat(tr.ColumnsToAnalyze[j].Result.Percentile90, 'G', -1, 64),
				strconv.FormatFloat(tr.ColumnsToAnalyze[j].Result.Percentile95, 'G', -1, 64),
				maxTH, mMaxTh,
				minTH, lMinTh,
			}); err != nil {

				return nil, errors.Wrapf(err,
					"makeCSVReport: Error writing CSV record: %v", pcol)
			}
		}
	}

	csvW.Flush()
	if err = csvW.Error(); err != nil {
		return nil, errors.Wrap(err, "makeCSVReport: Error flushing CSV")
	}

	return &outBuf, nil
}
