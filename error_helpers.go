package main

import (
	"github.com/gin-gonic/gin"
	"github.com/helloeave/json"
	"gitlab.com/demonihin/goperfstat/web_server/types"
)

const (
	ContentTypeJSON      = "application/json"
	ContentTypeJSONError = "application/json+error"
)

func Send200JSONResponse(c *gin.Context, jsonObj interface{}) {
	sendResponse(c, 200, jsonObj)
}

func Send204JSONResponse(c *gin.Context) {
	sendResponse(c, 204, nil)
}

func Abort400Error(c *gin.Context, err error, msg string) {
	abortWithErrorCode(c, 400, err, msg)
}

func Abort401Error(c *gin.Context, err error, msg string) {
	abortWithErrorCode(c, 401, err, msg)
}

func Abort404Error(c *gin.Context, err error, msg string) {
	abortWithErrorCode(c, 404, err, msg)
}

func Abort500Error(c *gin.Context, err error, msg string) {
	abortWithErrorCode(c, 500, err, msg)
}

func abortWithErrorCode(c *gin.Context, responseCode int, err error, msg string) {

	if err == nil {
		c.Header("Content-Type", ContentTypeJSON)
	} else {
		_ = c.Error(err)

		c.Header("Content-Type", ContentTypeJSONError)

		c.AbortWithStatus(responseCode)
	}

	jsb, ierr := json.Marshal(gin.H{
		"status_code": responseCode,
		"message":     msg,
		"error":       err,
	})
	if ierr != nil {
		_ = c.Error(ierr)
		return
	}

	if _, ierr := c.Writer.Write(jsb); ierr != nil {
		_ = c.Error(ierr)
		return
	}
}

func sendResponse(c *gin.Context, responseCode int, jsonObj interface{}) {
	c.Header("Content-type", ContentTypeJSON)

	jsb, ierr := json.Marshal(jsonObj)
	if ierr != nil {
		_ = c.Error(ierr)
		return
	}

	if _, ierr := c.Writer.Write(jsb); ierr != nil {
		_ = c.Error(ierr)
		return
	}
}

// Per topic errors

func AbortNoAuthCookies500(c *gin.Context, err error) {
	Abort500Error(c, err, "Error getting authentication cookies")
}

func AbortNoFileGiven500(c *gin.Context) {
	Abort500Error(c, types.ErrFilenameNotGiven, "No filename given")
}

func AbortNoTaskUUIDGiven500(c *gin.Context) {
	Abort500Error(c, types.ErrNoTaskUUIDGiven, "Error getting task UUID")
}
