package main

import (
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"gitlab.com/demonihin/goperfstat/web_server/db_saver"
)

func createTriggersDownloadHandler(svr *db_saver.DBSaver) gin.HandlerFunc {

	return func(c *gin.Context) {
		// Get session
		cook, err := c.Cookie(AuthCookiesName)
		if err != nil {
			AbortNoAuthCookies500(c, err)

			return
		}

		// Get requested filename
		filename := c.Param(FilenameDownloadPathParamName)
		if filename == "" {
			AbortNoFileGiven500(c)

			return
		}

		// Check if the file was uploaded by current user
		fi, err := svr.GetFileInfoForFileContext(c, filename, cook)
		if err != nil {
			Abort404Error(c, err, "Error file not found")

			return
		}

		// File not analyzed check
		if !fi.FileHasBeenAnalyzed {
			Abort400Error(c, errors.New("File has not yet been analyzed"),
				"Not analyzed")

			return
		}

		// Check for errors
		if len(fi.FileAnalyzingErrors) != 0 {
			Abort400Error(c,
				errors.New("File has analyzing errors. Can not build triggers"),
				"File has errors")

			return
		}

		// Make response file
		fileBytes, err := MakeCSVThresholdsTemplate(fi.FileAnalyzingResult.ColumnNames)
		if err != nil {
			Abort500Error(c, err, "Error building Triggers Template")

			return
		}

		// Send
		sendFile(c, fi.FileOriginalName, int64(fileBytes.Len()), fileBytes)
	}
}
