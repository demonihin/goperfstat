
clean:
	rm -r dist/

build-back:
	mkdir -p dist dist/upload
	go build -tags sqlite_foreign_keys -o dist/server

build-front:
	mkdir -p dist
	cd ./templates/perfmon/;npm i --save; ./node_modules/@vue/cli-service/bin/vue-cli-service.js build --dest ../../dist/frontend/


all: build-back build-front

docker-images:
	docker build --tag demonihin/perfstat:production --cache-from demonihin/perfstat:production ./