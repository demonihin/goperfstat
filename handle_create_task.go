package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/demonihin/goperfstat/web_server/db_saver"
	"gitlab.com/demonihin/goperfstat/web_server/types"
)

func createCreateTaskHandler(svr *db_saver.DBSaver) gin.HandlerFunc {
	return func(c *gin.Context) {

		// Get session
		cook, err := c.Cookie(AuthCookiesName)
		if err != nil {
			AbortNoAuthCookies500(c, err)

			return
		}

		// Load task
		var InTask types.TaskInfo
		if err := c.ShouldBindJSON(&InTask); err != nil {
			Abort500Error(c, err, "Error reading client request payload")

			return
		}

		// Find File for the Task
		fileInfo, err := svr.GetFileInfoForFileContext(c, InTask.FileUploadName,
			cook)
		if err != nil {
			Abort400Error(c, err,
				"Error getting file information to create a new task for")

			return
		}

		// Check file has been analyzed
		if !fileInfo.FileHasBeenAnalyzed {
			Abort400Error(c, ErrFileHasNotBeenAnalyzed, fileInfo.FileUploadName)

			return
		}

		// Check file for analyzing errors
		if len(fileInfo.FileAnalyzingErrors) != 0 {
			Abort400Error(c, ErrFileAnalyzedWithErrors, fileInfo.FileUploadName)

			return
		}

		// Create a new Task
		if err := svr.CreateTaskForFileContext(c, &InTask, fileInfo); err != nil {
			Abort500Error(c, err, "Error creating a new task")

			return
		}

	}
}
