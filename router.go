package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/demonihin/goperfstat/web_server/db_saver"
)

const (
	FilenameDownloadPathParamName = "filename"
	TaskInfoUUIDPathParamName     = "task_uuid"
)

func CreateRouter(svr *db_saver.DBSaver, cookieDomain string) *gin.Engine {

	eng := gin.Default()

	// Add Auth middleware
	eng.Use(createAuthMiddleware(svr, []string{"/api/auth/login", "/static/"}))

	// Create Upload handler
	eng.POST("/api/files", createFileUploadHandler(svr))

	// Create file download handler
	eng.GET("/api/files/:"+FilenameDownloadPathParamName, createFileDownloadHandler(svr))

	// Create Send Timestamp Column Format handler
	eng.POST("/api/files/:"+FilenameDownloadPathParamName, createSendTimestampFormatHandler(svr))

	// Create FileInfo update handler
	eng.PATCH("/api/files/:"+FilenameDownloadPathParamName, createPatchFileHandler(svr))

	// Create FileInfo delete handler
	eng.DELETE("/api/files/:"+FilenameDownloadPathParamName, createDeleteFileHandler(svr))

	// Create File Triggers download handler
	eng.GET("/api/files/:"+FilenameDownloadPathParamName+"/triggers/template",
		createTriggersDownloadHandler(svr))

	// Create Login handler
	eng.POST("/api/auth/login", createLoginHandler(svr, cookieDomain))

	// Create status handler
	eng.GET("/api/status", createStatusHandler(svr))

	// Create task handler (after upload and analyze)
	eng.POST("/api/tasks", createCreateTaskHandler(svr))

	// Create TaskInfo update handler
	eng.PATCH("/api/tasks/:"+TaskInfoUUIDPathParamName, createPatchTaskHandler(svr))

	// Get Task report CSV
	eng.GET("/api/tasks/:"+TaskInfoUUIDPathParamName+"/reports/csv",
		createCreateTaskCSVReportHandler(svr))

	// Create get supported codepages list
	eng.GET("/api/info/codepages", handleCodepagesHandler)

	// Static path
	eng.Static("/static", "./frontend/")

	return eng
}
