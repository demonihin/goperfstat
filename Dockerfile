FROM golang:1.13rc2-buster AS gobuilder
ENV GO111MODULES=on
WORKDIR /workdir
COPY . .
RUN go get -v ./...
RUN go build -o app

FROM node:12-buster AS nodebuilder
WORKDIR /workdir
COPY templates/perfmon/package*.json ./
RUN npm install
COPY templates/perfmon/ ./
RUN node_modules/@vue/cli-service/bin/vue-cli-service.js build --dest /frontend

FROM debian:buster AS production
WORKDIR /usr/src/app
COPY --from=gobuilder /workdir/app .
COPY --from=nodebuilder /frontend/ ./frontend/ 
RUN mkdir -p ./reports ./upload
EXPOSE 4000
CMD ["./app"]