package perfmon_stat

import (
	"encoding/csv"
	"io"
)

func readCSVIgnoreParseError(csvReader *csv.Reader) ([][]string, error) {
	// Read all data - ToDo - optimize
	rows := make([][]string, 0)

	for {
		row, ierr := csvReader.Read()
		if ierr != nil {

			// Skip ErrFieldCount
			if cerr, ok := ierr.(*csv.ParseError); ok {
				if cerr.Err == csv.ErrFieldCount {
					continue
				}

				if cerr.Err == csv.ErrBareQuote {
					continue
				}

				if cerr.Err == csv.ErrQuote {
					continue
				}

			}

			if ierr == io.EOF {
				break
			}

			return nil, ierr
		}

		rows = append(rows, row)
	}

	return rows, nil
}
