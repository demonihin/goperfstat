package perfmon_stat

import (
	"context"
	"encoding/csv"

	"gitlab.com/demonihin/goperfstat/web_server/types"

	"os"
	"time"

	"fmt"

	"io"

	"github.com/pkg/errors"
)

// InitNewFilesWatcherLoopContext - creates a new Loop which performs new File analyzing
// as soon as the file uploaded
func InitNewFilesWatcherLoopContext(ctx context.Context, svr FileAnalyzingTasksGetterSaver,
	errorsCh chan<- error, parallelTasksCount int) {

	// Analyze loop
	inTasks := make(chan types.FileAnalyzeTask)

	// Analyzing loop
	analyzedTasks := initAnalyzeLoopContext(ctx, inTasks, parallelTasksCount)

	// DB Updater loop
	initSaveAnalyzeResultLoopContext(ctx, analyzedTasks, svr, errorsCh)

	// Push new tasks to analyze
	go func() {

		// Init watch timer
		loopWaiter := time.NewTicker(types.FileAnalyzeLoopWatchIntervalSec * time.Second)
		defer func() {
			loopWaiter.Stop()
		}()

		for {
			select {
			case <-ctx.Done():
				close(inTasks)
				return

			case <-loopWaiter.C:
				// Get not-yet-processed files
				files, err := svr.GetFilesForAnalyzingContext(ctx,
					types.FileAnalyzeBatchSize)
				if err != nil {
					errorsCh <- errors.WithMessage(
						err, "Can not get files to analyze from DB")

					return
				}

				// Send task to analyzing loop
				for _, f := range files {
					// Copy to get rid of data race
					nf := f
					inTasks <- types.FileAnalyzeTask{
						FileInformation: &nf,
					}
				}

			}
		}
	}()
}

func initSaveAnalyzeResultLoopContext(
	ctx context.Context,
	inputTasks <-chan types.FileAnalyzeTask, svr FileAnalyzingTasksSaver, errorsCh chan<- error) {

	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			case taskRes := <-inputTasks:
				if err := svr.SaveFileAnalyzeResultContext(ctx, &taskRes); err != nil {

					errorsCh <- errors.WithMessagef(err,
						"Can not save analyzing report for file %d, %s",
						taskRes.FileInformation.FilePKID,
						taskRes.FileInformation.FileUploadName)

				} else {

					errorsCh <- errors.New(fmt.Sprintf(
						"Saved analyzing report for file %d, %s",
						taskRes.FileInformation.FilePKID,
						taskRes.FileInformation.FileUploadName))
				}
			}
		}
	}()
}

func initAnalyzeLoopContext(ctx context.Context,
	inputTasks <-chan types.FileAnalyzeTask,
	parallelTasksCount int) <-chan types.FileAnalyzeTask {

	outCh := make(chan types.FileAnalyzeTask)

	for i := 0; i < parallelTasksCount; i++ {
		go func() {
			for {
				select {
				case anTask := <-inputTasks:

					// Get current time to compute duration
					startTime := time.Now()

					anRes, err := performAnalyze(anTask.FileInformation)
					// End Time
					endTime := time.Now()
					spentTime := endTime.Sub(startTime)
					if err != nil {

						outT := types.FileAnalyzeTask{
							FileInformation:   anTask.FileInformation,
							Errors:            []string{err.Error()},
							StartTime:         &startTime,
							FinishTime:        &endTime,
							AnalyzingDuration: &spentTime,
						}

						outCh <- outT
						break
					}

					outT := types.FileAnalyzeTask{
						FileInformation:   anTask.FileInformation,
						StartTime:         &startTime,
						FinishTime:        &endTime,
						AnalyzingDuration: &spentTime,
						Errors:            nil,
						Result:            anRes,
					}
					outCh <- outT

				case <-ctx.Done():
					close(outCh)
					return
				}
			}
		}()
	}

	return outCh
}

func performAnalyze(fileInfo *types.FileInfo) (fAR *types.FileAnalyzeResult, err error) {

	// Open File
	file, err := os.Open(types.FileUploadDirectory + fileInfo.FileUploadName)
	if err != nil {
		return nil, err
	}
	defer func() {
		if ierr := file.Close(); ierr != nil {
			err = ierr
		}
	}()

	// Read and parse CSV
	csvReader := csv.NewReader(file)

	// Get Column Names
	columnNames, err := csvReader.Read()
	if err != nil {
		return nil, err
	}

	var (
		minTS, maxTS    *time.Time
		tsColInfo       *types.TimestampColumnInfo
		possibleFormats = fileInfo.FileAnalyzingResult.PossibleTimestampColumnInfos
	)

	// Try to find timestamp column formats if not yet set in FileInfo
	if possibleFormats == nil {

		var ierr error
		possibleFormats, ierr = assumeTimestampColumnInfos(csvReader, columnNames,
			types.MaxRowsReadToFindTimestampColumn)
		if ierr != nil {
			err = ierr
			return nil, err
		}
	}

	// If TimestampColumnInfo is not set - try to set it from Possible formats
	if fileInfo.FileTimestampColumnName == nil || fileInfo.FileTimestampColumnFormat == nil {

		// Format is not set in incoming FileInfo and detected only in one variant
		if len(possibleFormats) == 1 {
			// If only one format found - set it as the only possible format
			tsColInfo = &possibleFormats[0]
		}

	} else {
		tsColInfo = &types.TimestampColumnInfo{
			TimestampColumnName:   *fileInfo.FileTimestampColumnName,
			TimestampColumnFormat: *fileInfo.FileTimestampColumnFormat,
		}
	}

	// Try to find CSV file timestamps range only if there is only one TimestampColumnFormat
	if tsColInfo != nil {
		// Find Min and Max Timestamps in the file
		minTS, maxTS, err = getCSVFileTimestampsRange(file, tsColInfo)
		if err != nil {
			return nil, err
		}
	}

	// Timestamp column was found
	return &types.FileAnalyzeResult{
		ColumnNames:                  columnNames,
		PossibleTimestampColumnInfos: possibleFormats,
		MinFileTimestamp:             minTS,
		MaxFileTimestamp:             maxTS,
		TimestampColumn:              tsColInfo,
	}, nil
}

func getCSVFileTimestampsRange(file *os.File,
	tCI *types.TimestampColumnInfo) (
	minTimestamp *time.Time, maxTimestamp *time.Time, err error) {

	// Start file reading from the very beginning
	_, err = file.Seek(0, 0)
	if err != nil {
		return nil, nil, err
	}
	csvReader := csv.NewReader(file)

	// Read CSV and skip parsing errors (I.E. empty last line in file)
	rows, err := readCSVIgnoreParseError(csvReader)
	if err != nil {
		return nil, nil, errors.WithStack(err)
	}

	// Get Headers
	headersRow := rows[0]

	// Find Timestamp column index
	var (
		timestampColumnInd int
		foundTSCol         bool
	)
	for i, col := range headersRow {

		if col == tCI.TimestampColumnName {
			timestampColumnInd = i
			foundTSCol = true
			break
		}
	}
	if !foundTSCol {
		return nil, nil, types.ErrCanNotAutoFindTimestapFormat
	}

	// Get the earliest date
	firstRow := rows[1]
	minTimestampV, err := tCI.ParseTime(firstRow[timestampColumnInd])
	if err != nil {
		return nil, nil, err
	}

	// Get the latest date
	lastRow := rows[len(rows)-1]
	maxTimestampV, err := tCI.ParseTime(lastRow[timestampColumnInd])
	if err != nil {
		return nil, nil, err
	}

	return &minTimestampV, &maxTimestampV, nil
}

func assumeTimestampColumnInfos(rd *csv.Reader,
	columnNames []string, maxRowsToReadCount int) ([]types.TimestampColumnInfo, error) {

	var (
		tsColInfoRes              []types.TimestampColumnInfo
		tsColInfoPossibleVariants = make(map[types.TimestampColumnInfo]bool, 1)
	)

	for ind := 0; ind < maxRowsToReadCount; ind++ {

		row, err := rd.Read()
		if err != nil {
			if err == io.EOF {
				break
			} else {
				return nil, err
			}
		}

		// Find Timestamp Columns in current row
		tsCols, err := types.FindPossibleTimestampColumnFormats(columnNames, row)
		if err != nil {
			return nil, err
		}

		// Populate result with allowed Timestamp formats
		for i := range tsCols {
			tsColInfoPossibleVariants[tsCols[i]] = true
		}
	}

	// Make result
	tsColInfoRes = make([]types.TimestampColumnInfo, 0, len(tsColInfoPossibleVariants))
	for k := range tsColInfoPossibleVariants {
		tsColInfoRes = append(tsColInfoRes, k)
	}

	if len(tsColInfoRes) == 0 {
		return nil, types.ErrCanNotAutoFindTimestapFormat
	}

	return tsColInfoRes, nil
}

// type NonEmptyLineReader struct {
// 	r *bufio.Reader
// }

// func NewNonEmptyLineReader(r io.Reader) *NonEmptyLineReader {
// 	return &NonEmptyLineReader{
// 		r: bufio.NewReader(r),
// 	}
// }

// func (nel *NonEmptyLineReader) Read(b []byte) (int, error) {

// 	if len(b) == 0 {
// 		return 0, errors.New("Error reading to zero-length buffer")
// 	}

// 	// Read and omit zero-length lines
// 	for {
// 		line, err := nel.r.ReadSlice('\n')
// 		if err != nil {
// 			if errors.Cause(err) == io.EOF {
// 				return 0, io.EOF
// 			}

// 			return 0, errors.WithStack(err)
// 		}

// 		if len(line) != 0 {

// 			return copy(), nil
// 		}
// 	}

// }
