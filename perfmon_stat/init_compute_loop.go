package perfmon_stat

import (
	"context"
	"encoding/csv"
	"encoding/json"
	"io"
	"os"
	"sort"
	"strconv"
	"sync"
	"time"

	"gitlab.com/demonihin/goperfstat/web_server/types"

	"fmt"

	"github.com/pkg/errors"
)

// InitTasksProcessLoopContext - Initilizes loop which waits for new Tasks
func InitTasksProcessLoopContext(ctx context.Context,
	svr FileStatsComputationTasksGetterSaver,
	errCh chan<- error, parallelColumnsCount int) {

	outTasks := make(chan types.TaskInfo)

	// Start DB updater loop
	initSaveTaskStatsComputationResultLoopContext(ctx, outTasks, svr, errCh)

	// Start Task processer loop
	initTaskComputationLoopContext(ctx, outTasks, svr, errCh, parallelColumnsCount)
}

func initTaskComputationLoopContext(ctx context.Context, outTasks chan<- types.TaskInfo,
	svr FileStatsComputationTasksGetter, errCh chan<- error,
	parallelColumnsCount int) {

	go func() {

		tm := time.NewTicker(time.Second * types.FileStatsComputeLoopWatchIntervalSec)
		defer func() {
			tm.Stop()
		}()

		for {
			select {
			case <-ctx.Done():
				close(outTasks)
				return
			case <-tm.C:

				// Get data
				tasks, err := svr.GetTasksForStatsComputationContext(ctx,
					types.FileStatsComputeBatchSize)
				if err != nil {
					errCh <- err
					break
				}

				// Skip other steps
				if len(tasks) == 0 {
					break
				}

				// Process every Task
				for _, curTask := range tasks {

					// ToDo - make DB requests parallel?

					// StartTime
					startTimestamp := time.Now()

					// Get Timestamp Column
					tsColInfo, err := svr.GetTimestampColumnInfoForFileContext(
						ctx, curTask.FilePKID)
					if err != nil {
						errCh <- err

						// Result
						curTask.TaskProcessed = true
						curTask.TaskProcessingErrors = append(
							curTask.TaskProcessingErrors, err.Error())

						endTimestamp := time.Now()
						duration := endTimestamp.Sub(startTimestamp)

						curTask.TaskProcessingSpentTime = &duration
						curTask.TaskProcessingStartTime = &startTimestamp
						curTask.TaskProcessingEndTime = &endTimestamp

						outTasks <- curTask
						break
					}

					// Get values from File
					populatedCurTask, err := populateTaskColumnValues(&curTask, tsColInfo)
					if err != nil {

						// Result
						curTask.TaskProcessed = true
						curTask.TaskProcessingErrors = append(
							curTask.TaskProcessingErrors, err.Error())

						endTimestamp := time.Now()
						duration := endTimestamp.Sub(startTimestamp)

						curTask.TaskProcessingSpentTime = &duration
						curTask.TaskProcessingStartTime = &startTimestamp
						curTask.TaskProcessingEndTime = &endTimestamp

						outTasks <- curTask
						break
					}

					// Compute stats
					taskResult := computeStatisticsForTaskContext(ctx,
						populatedCurTask, parallelColumnsCount, errCh)
					outTasks <- *taskResult

				}
			}
		}

	}()
}

/*initSaveTaskStatsComputationResultLoopContext - Creates loop which updates
with statistics results
*/
func initSaveTaskStatsComputationResultLoopContext(ctx context.Context,
	inputTaskResults <-chan types.TaskInfo, svr FileStatsComputationTasksSaver,
	errorsCh chan<- error) {

	go func() {

		for {

			select {
			case <-ctx.Done():
				return

			case inputTaskResults, chanOpened := <-inputTaskResults:

				if !chanOpened {
					return
				}

				if err := svr.SaveTaskComputaionResultContext(
					ctx, &inputTaskResults); err != nil {

					errorsCh <- errors.WithStack(err)
				}

			}
		}
	}()

}

// computeStatisticsForTaskContext - Performs statistics computation for one Task
func computeStatisticsForTaskContext(ctx context.Context,
	inTask *types.TaskInfo, parallelColumnsCount int, errCh chan<- error) *types.TaskInfo {

	processingStartTimestamp := time.Now()

	outTask := &types.TaskInfo{
		TaskPKID:            inTask.TaskPKID,
		FilePKID:            inTask.FilePKID,
		FileUploadName:      inTask.FileUploadName,
		TaskCreateTimestamp: inTask.TaskCreateTimestamp,
		TimeRangesToAnalyze: make([]types.TaskTimeRange, 0, len(inTask.TimeRangesToAnalyze)),
	}

	// Set TaskTimeRanges for output
	for _, t := range inTask.TimeRangesToAnalyze {
		outTask.TimeRangesToAnalyze = append(outTask.TimeRangesToAnalyze, types.TaskTimeRange{
			RangePKID:        t.RangePKID,
			TaskPKID:         t.TaskPKID,
			RangeStartTime:   t.RangeStartTime,
			RangeEndTime:     t.RangeEndTime,
			ColumnsToAnalyze: make([]types.TaskColumnToAnalyze, 0, len(t.ColumnsToAnalyze)),
		})
	}

	var (
		wgRun sync.WaitGroup
	)
	wgRun.Add(1)

	// Send Task ColumnsToAnalyze through
	tasksColumnsFlow := make(chan types.TaskColumnToAnalyze)

	// Create Stats processing loop
	outTaskColumnFlow := initComputeColumnLoopContext(ctx, tasksColumnsFlow,
		inTask.TimeRangesToAnalyze, parallelColumnsCount)

	// Gather results
	go func() {

		defer wgRun.Done()

		for {

			select {
			case <-ctx.Done():
				return
			case taskColumnRes, chanOpened := <-outTaskColumnFlow:

				if !chanOpened {
					return
				}

				// Find and set values for resulting task
				var trFound bool
				for i := range outTask.TimeRangesToAnalyze {
					if taskColumnRes.TaskTimeRangePKID == outTask.TimeRangesToAnalyze[i].RangePKID {
						trFound = true
						outTask.TimeRangesToAnalyze[i].ColumnsToAnalyze = append(
							outTask.TimeRangesToAnalyze[i].ColumnsToAnalyze, taskColumnRes)

						// If there are errors - add them to task
						// The errors here are not for machine but for a man
						// The errors are not escaped
						if len(taskColumnRes.ComputationError) != 0 {
							outTask.TaskProcessingErrors = append(
								outTask.TaskProcessingErrors,
								fmt.Sprintf("%v::::%v::::%s",
									outTask.TimeRangesToAnalyze[i].RangeStartTime,
									outTask.TimeRangesToAnalyze[i].RangeEndTime,
									taskColumnRes.FileColumnName))
						}
					}
				}
				if !trFound {
					errI := errors.Wrap(types.ErrCanNotFindTaskTimeRange,
						"computeStatisticsForTaskContext")
					outTask.TaskProcessingErrors = append(
						outTask.TaskProcessingErrors, errI.Error())
					errCh <- errI
				}

			}
		}

	}()

	// Send Tasks
	for tri := range inTask.TimeRangesToAnalyze {
		tr := &inTask.TimeRangesToAnalyze[tri]
		for tci := range tr.ColumnsToAnalyze {
			tasksColumnsFlow <- tr.ColumnsToAnalyze[tci]
		}
	}
	// Closes and leases Results gatherer
	close(tasksColumnsFlow)

	// Wait until all Tasks are finished
	wgRun.Wait()

	// Processing time
	processingEndTimestamp := time.Now()
	processingDuration := processingEndTimestamp.Sub(processingStartTimestamp)

	outTask.TaskProcessingStartTime = &processingStartTimestamp
	outTask.TaskProcessingEndTime = &processingEndTimestamp
	outTask.TaskProcessingSpentTime = &processingDuration

	outTask.TaskProcessed = true

	// Save all Task to its field
	jsB, err := json.Marshal(outTask)
	if err != nil {
		outTask.TaskProcessingErrors = append(outTask.TaskProcessingErrors,
			err.Error())
		errCh <- err
	}
	jsS := string(jsB)
	outTask.TaskProcessingInfo = &jsS

	return outTask
}

// initComputeColumnLoopContext - Performs computation of statistics
func initComputeColumnLoopContext(ctx context.Context,
	inputColumnTasks <-chan types.TaskColumnToAnalyze, timeRanges []types.TaskTimeRange,
	parallelColumnsCount int) <-chan types.TaskColumnToAnalyze {

	outputTasks := make(chan types.TaskColumnToAnalyze)
	var (
		waitEnd       sync.WaitGroup
		closeChanOnce sync.Once
	)

	// Processing tasks loop init
	waitEnd.Add(parallelColumnsCount)
	for i := 0; i < parallelColumnsCount; i++ {
		go func() {

			// Close outputTasks chan only once
			defer func() {

				waitEnd.Wait()

				closeChanOnce.Do(func() {
					// Close on end
					close(outputTasks)
				})
			}()

			defer waitEnd.Done()

			// Process tasks
			for {
				select {
				// Process one input task
				case inTasksColumn, chanOpened := <-inputColumnTasks:

					if !chanOpened {
						return
					}

					// start timestamp
					startTime := time.Now()

					tasksColumn := types.TaskColumnToAnalyze{
						TaskTimeRangePKID:    inTasksColumn.TaskTimeRangePKID,
						ProcessedValuesCount: inTasksColumn.ProcessedValuesCount,
						FileColumnName:       inTasksColumn.FileColumnName,
						MinValueThreshold:    inTasksColumn.MinValueThreshold,
						MaxValueThreshold:    inTasksColumn.MaxValueThreshold,
						ColumnPKID:           inTasksColumn.ColumnPKID,
					}

					// Validate Input
					if len(inTasksColumn.Values) == 0 {
						tasksColumn.ComputationError = append(
							tasksColumn.ComputationError,
							types.ErrZeroLengthInputSlice.Error())
						outputTasks <- tasksColumn
						break
					}

					// Make local copy of input data
					copyed := make([]float64, len(inTasksColumn.Values))
					copyNum := copy(copyed, inTasksColumn.Values)
					if copyNum != len(inTasksColumn.Values) {
						tasksColumn.ComputationError = append(
							tasksColumn.ComputationError,
							types.ErrCopyInputValues.Error())
						outputTasks <- tasksColumn
						break
					}
					// Sort
					if !sort.Float64sAreSorted(copyed) {
						sort.Float64s(copyed)
					}

					// Perform stats computation
					stats, err := computeStats(copyed,
						tasksColumn.MinValueThreshold,
						tasksColumn.MaxValueThreshold)
					if err != nil {

						// Finish time and duration
						tasksColumn.ComputationStartTime = &startTime
						finishTime := time.Now()
						tasksColumn.ComputationFinishTime = &finishTime
						duration := tasksColumn.ComputationFinishTime.Sub(startTime)
						tasksColumn.ComputationDuration = &duration

						tasksColumn.ComputationError = append(
							tasksColumn.ComputationError,
							types.ErrCopyInputValues.Error())
						outputTasks <- tasksColumn
						break
					}

					// finish time and duration
					tasksColumn.ComputationStartTime = &startTime
					finishTime := time.Now()
					tasksColumn.ComputationFinishTime = &finishTime
					duration := tasksColumn.ComputationFinishTime.Sub(startTime)
					tasksColumn.ComputationDuration = &duration

					// Processed values
					tasksColumn.ProcessedValuesCount = uint64(len(copyed))

					// Push result
					tasksColumn.Result = stats
					outputTasks <- tasksColumn

				case <-ctx.Done():
					return
				}
			}

		}()

	}

	return outputTasks
}

// populateTaskColumnValues - Loads column's values from CSV file for given Task
func populateTaskColumnValues(task *types.TaskInfo,
	timestampColInfo *types.TimestampColumnInfo) (tIR *types.TaskInfo, err error) {

	// Open CSV file
	fileRdr, err := os.Open(types.FileUploadDirectory + task.FileUploadName)
	if err != nil {
		return nil, err
	}
	defer func() {
		if ierr := fileRdr.Close(); ierr != nil {
			err = ierr
		}
	}()

	// CSV
	csvRdr := csv.NewReader(fileRdr)

	// Make Headers => column numbers mapping
	headerCNMapping := make(map[string]int)
	headerRow, err := csvRdr.Read()
	if err != nil {
		return nil, err
	}
	for cn, headerName := range headerRow {
		headerCNMapping[headerName] = cn
	}

	// Timestamp Column Number in row
	timestampColumnNumber, ok := headerCNMapping[timestampColInfo.TimestampColumnName]
	if !ok {
		return nil, errors.WithStack(types.ErrCanNotFindTimestampColumnIndex)
	}

	// Populate Columns Values
	for {

		// Load data from CSV to row
		row, err := csvRdr.Read()
		if err != nil {

			if cerr, ok := err.(*csv.ParseError); ok {
				// Skip parse error
				if cerr.Err == csv.ErrFieldCount {
					continue
				}

				return nil, err
			}

			if err == io.EOF {
				break
			}
			return nil, err
		}

		// Get Timestamp of current CSV row
		rowTimestamp, err := timestampColInfo.ParseTime(row[timestampColumnNumber])
		if err != nil {
			return nil, errors.Wrapf(err,
				"populateTaskColumnValues: Can not parse Row Timestamp for row, column number %d, content %v",
				timestampColumnNumber, row)
		}

		// Add the data to Task Column values for the row
		for timeRangeIND := range task.TimeRangesToAnalyze {

			tTimeRangeP := &task.TimeRangesToAnalyze[timeRangeIND]

			// Check if current CSV row is in TimeRangeToAnalyze
			if !timeInRange(
				maxTime(&tTimeRangeP.RangeStartTime, &types.MinTimestampInReport),
				minTime(&tTimeRangeP.RangeEndTime, &types.MaxTimetampInReport),
				&rowTimestamp) {

				continue
			}

			// Add row's value to TaskColumnToAnalyze
			for timeRangeColumnID := range tTimeRangeP.ColumnsToAnalyze {

				colPtr := tTimeRangeP.ColumnsToAnalyze[timeRangeColumnID]

				colNumber, ok := headerCNMapping[colPtr.FileColumnName]
				if !ok {
					return nil, errors.Wrapf(types.ErrColumnNotFoundInFile,
						"populateTaskColumnValues: Error finding required column '%s' in CSV file headers %v",
						colPtr.FileColumnName, headerRow)
				}

				valS := row[colNumber]
				// Sometimes CSV reports contain empty strings as values.
				// I decided to skip them
				if valS == " " || valS == "" {
					continue
				}

				val, err := strconv.ParseFloat(valS, 64)
				if err != nil {
					return nil, errors.Wrapf(err,
						"populateTaskColumnValues: Error converting '%s' to float64",
						valS)
				}

				tTimeRangeP.ColumnsToAnalyze[timeRangeColumnID].Values = append(colPtr.Values, val)
			}

		}
	}

	return task, nil
}

// timeInRange - Returns true if given timestamp is in range [start, end]
func timeInRange(start, end, timestamp *time.Time) bool {
	includingStart := start.Add(-time.Nanosecond)
	includingEnd := end.Add(time.Nanosecond)
	return timestamp.After(includingStart) && timestamp.Before(includingEnd)
}

func minTime(t1, t2 *time.Time) *time.Time {

	if t1 == nil {
		return t2
	}

	if t2 == nil {
		return t1
	}

	if t1.Before(*t2) {
		return t1
	} else {
		return t2
	}
}

func maxTime(t1, t2 *time.Time) *time.Time {

	if t1 == nil {
		return t2
	}

	if t2 == nil {
		return t1
	}

	if t1.Before(*t2) {
		return t2
	} else {
		return t1
	}
}
