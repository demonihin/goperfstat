package perfmon_stat

import (
	"context"
	"log"
)

func InitErrorsLoggerContext(ctx context.Context, ch <-chan error) {

	go func() {
		for {
			select {
			case <-ctx.Done():
				return

			case err := <-ch:
				log.Println(err)
			}
		}
	}()

}
