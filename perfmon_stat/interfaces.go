package perfmon_stat

import (
	"context"

	"gitlab.com/demonihin/goperfstat/web_server/types"
)

type FilesAnalyzingTasksGetter interface {
	GetFilesForAnalyzingContext(ctx context.Context, maxFiles int) (fi []types.FileInfo, err error)
}

type FileAnalyzingTasksSaver interface {
	SaveFileAnalyzeResultContext(ctx context.Context, fAT *types.FileAnalyzeTask) (err error)
}

type FileAnalyzingTasksGetterSaver interface {
	FileAnalyzingTasksSaver
	FilesAnalyzingTasksGetter
}

type FileStatsComputationTasksSaver interface {
	SaveTaskComputaionResultContext(ctx context.Context, ti *types.TaskInfo) (err error)
}

type FileStatsComputationTasksGetter interface {
	GetTasksForStatsComputationContext(ctx context.Context, maxTasks int) (outTasks []types.TaskInfo, err error)
	GetTimestampColumnInfoForFileContext(ctx context.Context, filePKID uint64) (tColInfo *types.TimestampColumnInfo, err error)
}

type FileStatsComputationTasksGetterSaver interface {
	FileStatsComputationTasksGetter
	FileStatsComputationTasksSaver
}
