package perfmon_stat

import (
	"log"
	"sync"

	"gitlab.com/demonihin/goperfstat/web_server/types"

	"sort"

	"github.com/pkg/errors"
	"gonum.org/v1/gonum/stat"
)

// computeStats - Computes statistics.
func computeStats(sortedVals []float64,
	minThreshold *float64,
	maxThreshold *float64) (*types.StatsColumnResult, error) {

	if len(sortedVals) == 0 {
		return nil, errors.Wrap(types.ErrZeroLengthInputSlice,
			"computeStats: Error processing Zero-Lenth input values")
	}

	if !sort.Float64sAreSorted(sortedVals) {
		return nil, types.ErrUnsortedInput
	}

	var (
		mean, variance                             float64
		min, max                                   float64
		percentiles                                types.Percentiles
		maxThresholdFraction, minThresholdFraction *float64

		wg sync.WaitGroup
	)

	// Set WaitGroup
	wg.Add(2)

	// Compute thresholds
	if minThreshold != nil {
		wg.Add(1)
	}
	if maxThreshold != nil {
		wg.Add(1)
	}

	if maxThreshold != nil {
		go func() {
			var err error
			maxThresholdFraction, err = moreMaxThresholdFraction(
				sortedVals, maxThreshold)

			if err != nil {
				log.Fatalln(err)
			}

			wg.Done()
		}()
	}
	if minThreshold != nil {
		go func() {
			var err error
			minThresholdFraction, err = lessMinThresholdFraction(
				sortedVals, maxThreshold)

			if err != nil {
				log.Fatalln(err)
			}

			wg.Done()
		}()
	}

	// Compute Mean and Variance
	go func() {
		mean, variance = stat.MeanVariance(sortedVals, nil)

		wg.Done()
	}()

	// Min and Max
	min, max, err := minMax(sortedVals)
	if err != nil {
		return nil, errors.Wrap(err, "computeStats: Error getting Min and Max values")
	}

	// Percentiles
	go func() {
		percentiles = computePercentiles(sortedVals)
		wg.Done()
	}()

	// Wait for completion
	wg.Wait()

	return &types.StatsColumnResult{
		Mean:                         mean,
		Variance:                     variance,
		Min:                          min,
		Max:                          max,
		MoreThanMaxThresholdFraction: maxThresholdFraction,
		LessThanMinThresholdFraction: minThresholdFraction,
		Percentiles:                  percentiles,
	}, nil
}

// minMax - finds minimum and maximum from values
func minMax(sortedVals []float64) (min float64, max float64, err error) {
	valsLen := len(sortedVals)
	if valsLen == 0 {
		return 0, 0, errors.Wrap(types.ErrZeroLengthInputSlice, "minMax: Error computing mix max values")
	}
	return sortedVals[0], sortedVals[len(sortedVals)-1], nil
}

// lessMinThresholdFraction - returns fraction of values which are less than minThreshold
func lessMinThresholdFraction(
	sortedVals []float64, minThreshold *float64) (*float64, error) {

	if len(sortedVals) == 0 {
		return nil, types.ErrZeroLengthInputSlice
	}

	if minThreshold == nil {
		return nil, types.ErrIncorrectThresholdValue
	}

	var (
		count  int
		thresh float64 = *minThreshold
	)

	// Use a fact thet input is sorted
	for i, v := range sortedVals {
		// Stop on first value which is greater than threshold
		if v > thresh {
			count = i
			break
		}
	}

	res := float64(count) / float64(len(sortedVals))
	return &res, nil
}

// moreMaxThresholdFraction - returns fraction of values which are more than maxThreshold
func moreMaxThresholdFraction(
	sortedVals []float64, maxThreshold *float64) (*float64, error) {

	if len(sortedVals) == 0 {
		return nil, types.ErrZeroLengthInputSlice
	}

	if maxThreshold == nil {
		return nil, types.ErrIncorrectThresholdValue
	}

	var (
		count  int
		thresh float64 = *maxThreshold
	)

	// Use a fact thet input is sorted
	for i, v := range sortedVals {

		// Stop on first value that is more than threshold
		if v > thresh {
			count = i
			break
		}
	}

	res := float64(len(sortedVals)-count) / float64(len(sortedVals))
	return &res, nil
}

func computePercentiles(vals []float64) types.Percentiles {

	var (
		wg           sync.WaitGroup
		percentile95 float64
		percentile90 float64
		percentile80 float64
	)
	wg.Add(3)

	go func() {
		percentile95 = stat.Quantile(0.95, stat.Empirical, vals, nil)
		wg.Done()
	}()

	go func() {
		percentile90 = stat.Quantile(0.9, stat.Empirical, vals, nil)
		wg.Done()
	}()

	go func() {
		percentile80 = stat.Quantile(0.8, stat.Empirical, vals, nil)
		wg.Done()
	}()

	wg.Wait()

	return types.Percentiles{
		Percentile95: percentile95,
		Percentile90: percentile90,
		Percentile80: percentile80,
	}
}
