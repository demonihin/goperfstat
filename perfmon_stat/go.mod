module gitlab.com/demonihin/goperfstat/perfmon_stat

go 1.12

require (
	github.com/pkg/errors v0.8.1
	gitlab.com/demonihin/goperfstat/web_server/types v0.0.0-00010101000000-000000000000
	golang.org/x/exp v0.0.0-20190510132918-efd6b22b2522 // indirect
	gonum.org/v1/gonum v0.0.0-20190520094443-a5f8f3a4840b
	gonum.org/v1/netlib v0.0.0-20190331212654-76723241ea4e // indirect
)

replace gitlab.com/demonihin/goperfstat/web_server/types => ../types
