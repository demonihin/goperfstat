package main

import (
	"context"

	"gitlab.com/demonihin/goperfstat/web_server/types"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"gitlab.com/demonihin/goperfstat/web_server/db_saver"
)

func createPatchFileHandler(svr *db_saver.DBSaver) gin.HandlerFunc {

	return func(c *gin.Context) {

		// Get session
		cook, err := c.Cookie(AuthCookiesName)
		if err != nil {
			AbortNoAuthCookies500(c, err)

			return
		}

		// Get requested filename
		filename := c.Param(FilenameDownloadPathParamName)
		if filename == "" {
			AbortNoFileGiven500(c)

			return
		}

		// Check if the file was uploaded by current user
		fi, err := svr.GetFileInfoForFileContext(c, filename, cook)
		if err != nil {
			Abort404Error(c, err, "File not found")

			return
		}

		// Load file operations
		var operations []types.FilePatchPayload
		if err := c.ShouldBindJSON(&operations); err != nil {
			Abort500Error(c, err, "Error parsing request")

			return
		}

		// Perform operations
		if err := performFileOperationsContext(c, svr, operations, fi); err != nil {
			Abort500Error(c, err, "Error performing file info patching")

			return
		}

		Send200JSONResponse(c, gin.H{"status": "ok"})
	}
}

func performFileOperationsContext(ctx context.Context, svr *db_saver.DBSaver,
	operations []types.FilePatchPayload, fi *types.FileInfo) error {

	for _, op := range operations {

		switch op.Key {
		case types.FileOriginalNameKey:
			if err := svr.UpdateFileNameContext(ctx, fi.FilePKID,
				op.Value); err != nil {
				return err
			}
		default:
			return errors.WithStack(types.ErrInvalidPatchOperationKey)
		}
	}

	return nil
}
