package main

import (
	"context"

	"gitlab.com/demonihin/goperfstat/web_server/types"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"

	"gitlab.com/demonihin/goperfstat/web_server/db_saver"
)

func createPatchTaskHandler(svr *db_saver.DBSaver) gin.HandlerFunc {

	return func(c *gin.Context) {
		// Get session
		cook, err := c.Cookie(AuthCookiesName)
		if err != nil {
			AbortNoAuthCookies500(c, err)

			return
		}

		// Get requested task UUID
		task_uuid := c.Param(TaskInfoUUIDPathParamName)
		if task_uuid == "" {
			AbortNoTaskUUIDGiven500(c)

			return
		}

		// Load Task operations
		var taskOperations []types.TaskPatchPayload
		if err := c.ShouldBindJSON(&taskOperations); err != nil {
			Abort500Error(c, err, "Error parsing request payload")

			return
		}

		// Check if the file was uploaded by current user
		ti, err := svr.GetTaskInfoForTaskUUIDContext(c, task_uuid, cook)
		if err != nil {
			Abort404Error(c, err, "No task found")

			return
		}

		// Perform Task modification operations
		if err := performTaskOperationsContext(c, svr,
			taskOperations, ti); err != nil {
			Abort500Error(c, err, "Error applying task patch")

			return
		}

		Send200JSONResponse(c, gin.H{"status": "ok"})
	}
}

func performTaskOperationsContext(ctx context.Context, svr *db_saver.DBSaver,
	operations []types.TaskPatchPayload, ti *types.TaskInfo) error {

	for _, tp := range operations {

		switch tp.Key {
		case types.TaskPatchTaskNameKey:
			if err := svr.UpdateTaskNameContext(ctx, ti.TaskPKID,
				tp.Value); err != nil {

				return errors.WithStack(err)
			}
		}
	}

	return nil
}
